﻿using System;
using Topshelf;

namespace TopshelfDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var rc = HostFactory.Run(x =>
                {
                    x.Service<TopshelfDemoEntrance>(s =>
                    {
                        //配置一个完全定制的服务,对Topshelf没有依赖关系。常用的方式。
                        s.ConstructUsing(name => new TopshelfDemoEntrance());
                        s.WhenStarted(tc => tc.Start(args));
                        s.WhenStopped(tc => tc.Stop());
                    });

                    //服务使用network_service内置帐户运行。身份标识，有好几种方式，如：x.RunAs("username", "password");  x.RunAsPrompt(); x.RunAsNetworkService(); 等
                    x.RunAsLocalSystem();
                    //安装服务后，服务的描述
                    x.SetDescription("Windows伺服程序,用于Windows标准服务程序的安装、启动、停止、卸载，服务的自动化维护操作");
                    //显示名称
                    x.SetDisplayName("WindowsServoService,Windows伺服程序");
                    //服务名称
                    x.SetServiceName("WindowsServoService");

                    x.OnException(ex =>
                    {
                        LogHelper.Error(ex.Message);
                    });
                });
                var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
                Environment.ExitCode = exitCode;
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message);
            }
        }
    }
}
