/*
* 命名空间: Mall.Model
*
* 功 能： ClientInfo实体类
*
* 类 名： ClientInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/10/30 11:36:42 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Mall.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 客户信息表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "mall", TableName = "CLIENT_INFO")]
    public class ClientInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public ClientInfoEntity()
        {
            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //OpenId
            this.open_id = string.Empty;

            //用户头像
            this.head_img = string.Empty;

            //支付密码
            this.pay_password = string.Empty;

            //电话号码
            this.mobile_phone = string.Empty;

            //姓名
            this.name = string.Empty;

            //性别
            this.sex = 0;

            //用户昵称
            this.nickname = string.Empty;

            //是否有效  100有效 110无效
            this.is_valid = 0;

            //积分
            this.integral = 0;

            //消费总金额
            this.monetary = 0;

            //备注
            this.remarks = string.Empty;

            //注册时间
            this.create_time = DateTime.Now;

            //是否被删除 100正常 110删除
            this.is_deleted = 0;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// OpenId
        /// </summary>
        [DBFieldInfo(ColumnName = "OPEN_ID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string open_id
        {
            get; set;
        }

        /// <summary>
        /// 用户头像
        /// </summary>
        [DBFieldInfo(ColumnName = "HEAD_IMG", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string head_img
        {
            get; set;
        }

        /// <summary>
        /// 支付密码
        /// </summary>
        [DBFieldInfo(ColumnName = "PAY_PASSWORD", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string pay_password
        {
            get; set;
        }

        /// <summary>
        /// 电话号码
        /// </summary>
        [DBFieldInfo(ColumnName = "MOBILE_PHONE", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// 姓名
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 性别 0未知 1男 2女
        /// </summary>
        [DBFieldInfo(ColumnName = "SEX", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int sex
        {
            get; set;
        }

        /// <summary>
        /// 用户昵称
        /// </summary>
        [DBFieldInfo(ColumnName = "NICKNAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string nickname
        {
            get; set;
        }


        /// <summary>
        /// 用户所在国家
        /// </summary>
        [DBFieldInfo(ColumnName = "COUNTRY", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string country
        {
            get; set;
        }

        /// <summary>
        ///  用户所在省份
        /// </summary>
        [DBFieldInfo(ColumnName = "PROVINCE", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string province
        {
            get; set;
        }


        /// <summary>
        /// 用户所在城市
        /// </summary>
        [DBFieldInfo( ColumnName = "CITY", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string city
        {
            get;set;
        }

        /// <summary>
        /// 是否有效  100有效 110无效
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_VALID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int is_valid
        {
            get; set;
        }

        /// <summary>
        /// 积分
        /// </summary>
        [DBFieldInfo(ColumnName = "INTEGRAL", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int integral
        {
            get; set;
        }

        /// <summary>
        /// 消费总金额
        /// </summary>
        [DBFieldInfo(ColumnName = "MONETARY", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public decimal monetary
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DBFieldInfo(ColumnName = "REMARKS", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 注册时间
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_TIME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public DateTime create_time
        {
            get; set;
        }

        /// <summary>
        /// 是否被删除 100正常 110删除
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_DELETED", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int is_deleted
        {
            get; set;
        }

    }
}
