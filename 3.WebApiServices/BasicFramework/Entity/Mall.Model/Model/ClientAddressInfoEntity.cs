/*
* 命名空间: Mall.Model
*
* 功 能： ClientAddressInfo实体类
*
* 类 名： ClientAddressInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/10/30 11:36:42 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Mall.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 客户收获地址表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "mall", TableName = "CLIENT_ADDRESS_INFO")]
    public class ClientAddressInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public ClientAddressInfoEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //用户唯一标识符
            this.client_id = string.Empty;

            //联系人
            this.user_name = string.Empty;

            //联系电话
            this.mobile_phone = string.Empty;

            //省
            this.province = string.Empty;

            //市
            this.city = string.Empty;

            //区县
            this.county = string.Empty;

            //具体地址
            this.address = string.Empty;

            //排序
            this.sort = 0;

            //100默认地址 110非默认地址
            this.is_default = 0;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 用户唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "CLIENT_ID", Required = false, IsPrimarykey = false)]
        public string client_id
        {
            get; set;
        }

        /// <summary>
        /// 联系人
        /// </summary>
        [DBFieldInfo(ColumnName = "USER_NAME", Required = false, IsPrimarykey = false)]
        public string user_name
        {
            get; set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        [DBFieldInfo(ColumnName = "MOBILE_PHONE", Required = false, IsPrimarykey = false)]
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// 省
        /// </summary>
        [DBFieldInfo(ColumnName = "PROVINCE", Required = false, IsPrimarykey = false)]
        public string province
        {
            get; set;
        }

        /// <summary>
        /// 市
        /// </summary>
        [DBFieldInfo(ColumnName = "CITY", Required = false, IsPrimarykey = false)]
        public string city
        {
            get; set;
        }

        /// <summary>
        /// 区县
        /// </summary>
        [DBFieldInfo(ColumnName = "COUNTY", Required = false, IsPrimarykey = false)]
        public string county
        {
            get; set;
        }

        /// <summary>
        /// 具体地址
        /// </summary>
        [DBFieldInfo(ColumnName = "ADDRESS", Required = false, IsPrimarykey = false)]
        public string address
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT", Required = false, IsPrimarykey = false)]
        public long sort
        {
            get; set;
        }

        /// <summary>
        /// 100默认地址 110非默认地址
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_DEFAULT", Required = false, IsPrimarykey = false)]
        public int is_default
        {
            get; set;
        }
    }
}
