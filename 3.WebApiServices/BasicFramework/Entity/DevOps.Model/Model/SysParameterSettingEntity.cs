/*
* 命名空间: DevOps.Model
*
* 功 能： SysParameterSetting实体类
*
* 类 名： SysParameterSettingEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/06/15 14:06:57 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using Dapper.Library;
    using System;

    /// <summary>
    /// 参数设置
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_PARAMETER_SETTING")]
    public class SysParameterSettingEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public SysParameterSettingEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("-", "");

            //参数名称
            this.name = string.Empty;

            //参数键名
            this.parameters_key = string.Empty;

            //参数键值
            this.parameters_value = string.Empty;

            //创建时间
            this.create_date = DateTime.Now;

            //备注
            this.remarks = string.Empty;

            //排序
            this.sort =0;

        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 参数名称
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 参数键名
        /// </summary>
        [DBFieldInfo(ColumnName = "PARAMETERS_KEY", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string parameters_key
        {
            get; set;
        }

        /// <summary>
        /// 参数键值
        /// </summary>
        [DBFieldInfo(ColumnName = "PARAMETERS_VALUE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string parameters_value
        {
            get; set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_DATE", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DBFieldInfo(ColumnName = "REMARKS", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public double sort
        {
            get; set;
        }
    }
}
