/*
* 命名空间: DevOps.Model
*
* 功 能： FlowFlowInfo实体类
*
* 类 名： FlowFlowInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/01/15 09:34:11 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 流程基础信息表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "FLOW_INFO")]
    public class FlowInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowInfoEntity()
        {

            //唯一标识符
            this.id = string.Empty;

            //名称
            this.name = string.Empty;

            //分类
            this.flow_category = string.Empty;

            //管理人员
            this.manager = string.Empty;
            //创建日期
            this.create_time = DateTime.Now;

            //创建人员id
            this.create_user = string.Empty;

            //创建人员名称
            this.create_user_name = string.Empty;

            //设计时JSON
            this.designer_json = string.Empty;

            //安装人员ID
            this.install_user = string.Empty;

            //安装人员名称
            this.install_user_name = string.Empty;

            //状态 100设计中 110已安装 120已卸载 130已删除
            this.status = 0;

            //备注
            this.remarks = string.Empty;

            //表单关键字
            this.form_key = string.Empty;

            //排序
            this.sort = 0;

        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 名称
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 分类
        /// </summary>
        [DBFieldInfo(ColumnName = "FLOW_CATEGORY", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string flow_category
        {
            get; set;
        }

        /// <summary>
        /// 管理人员
        /// </summary>
        [DBFieldInfo(ColumnName = "MANAGER", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string manager
        {
            get; set;
        }

        /// <summary>
        /// 管理人员名称
        /// </summary>
        [DBFieldInfo(ColumnName = "MANAGERNAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string managername
        {
            get; set;
        }


        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_TIME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public DateTime create_time
        {
            get; set;
        }

        /// <summary>
        /// 创建人员id
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_USER", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string create_user
        {
            get; set;
        }

        /// <summary>
        /// 创建人员名称
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_USER_NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string create_user_name
        {
            get; set;
        }

        /// <summary>
        /// 设计时JSON
        /// </summary>
        [DBFieldInfo(ColumnName = "DESIGNER_JSON", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string designer_json
        {
            get; set;
        }

        /// <summary>
        /// 安装时间
        /// </summary>
        [DBFieldInfo(ColumnName = "INSTALL_TIME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public DateTime? install_time
        {
            get; set;
        }

        /// <summary>
        /// 安装人员ID
        /// </summary>
        [DBFieldInfo(ColumnName = "INSTALL_USER", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string install_user
        {
            get; set;
        }

        /// <summary>
        /// 安装人员名称
        /// </summary>
        [DBFieldInfo(ColumnName = "INSTALL_USER_NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string install_user_name
        {
            get; set;
        }

        /// <summary>
        /// 状态 100设计中 110已安装 120已卸载 130已删除
        /// </summary>
        [DBFieldInfo(ColumnName = "STATUS", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int status
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DBFieldInfo(ColumnName = "REMARKS", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 表单关键字
        /// </summary>
        [DBFieldInfo(ColumnName = "FORM_KEY", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string form_key
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public float sort
        {
            get; set;
        }
    }
}
