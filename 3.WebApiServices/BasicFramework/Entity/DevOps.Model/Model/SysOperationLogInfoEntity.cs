/*
* 命名空间: DevOps.Model
*
* 功 能： SysOperationLogInfo实体类
*
* 类 名： SysOperationLogInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 系统操作记录表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_OPERATION_LOG_INFO")]
    public class SysOperationLogInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public SysOperationLogInfoEntity()
        {
            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("-", "");

            //所属服务名称
            this.service_name = string.Empty;

            //功能类型 100：插入操作  101：更新操作 102：删除操作
            this.func_type = 0;

            //操作用户CODE
            this.user_code = string.Empty;

            //操作用户名称
            this.user_name = string.Empty;

            //业务标题【用户管理】
            this.business_title = string.Empty;

            //IP地址
            this.ip_address = string.Empty;

            //创建日期
            this.create_date = DateTime.Now;

            //操作明细
            this.detail = string.Empty;

          
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 所属服务名称
        /// </summary>
        [DBFieldInfo(ColumnName = "SERVICE_NAME", Required = true, IsPrimarykey = false)]
        public string service_name
        {
            get; set;
        }

        /// <summary>
        /// 功能类型 100：插入操作  101：更新操作 102：删除操作
        /// </summary>
        [DBFieldInfo(ColumnName = "FUNC_TYPE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public int func_type
        {
            get; set;
        }

        /// <summary>
        /// 操作用户CODE
        /// </summary>
        [DBFieldInfo(ColumnName = "USER_CODE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string user_code
        {
            get; set;
        }

        /// <summary>
        /// 操作用户名称
        /// </summary>
        [DBFieldInfo(ColumnName = "USER_NAME", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string user_name
        {
            get; set;
        }

        /// <summary>
        /// 业务标题【用户管理】
        /// </summary>
        [DBFieldInfo(ColumnName = "BUSINESS_TITLE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string business_title
        {
            get; set;
        }

        /// <summary>
        /// IP地址
        /// </summary>
        [DBFieldInfo(ColumnName = "IP_ADDRESS", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string ip_address
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_DATE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 操作明细
        /// </summary>
        [DBFieldInfo(ColumnName = "DETAIL", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string detail
        {
            get; set;
        }
    }
}
