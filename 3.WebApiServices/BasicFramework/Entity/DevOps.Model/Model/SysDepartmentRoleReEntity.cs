/*
* 命名空间: DevOps.Model
*
* 功 能： SysDepartmentRoleRe实体类
*
* 类 名： SysDepartmentRoleReEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 部门—角色关系表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_DEPARTMENT_ROLE_RE")]
    public class SysDepartmentRoleReEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysDepartmentRoleReEntity()
     {

           //部门唯一标识符
           this.department_id = string.Empty;

           //角色唯一标识符
           this.role_id = string.Empty;

           //唯一标识符
           this.id = Guid.NewGuid().ToString().Replace("-", "");
        }

        /// <summary>
        /// 部门唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "DEPARTMENT_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string department_id
        {
            get; set;
        }

        /// <summary>
        /// 角色唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ROLE_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string role_id
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }
    }
}
