/*
* 命名空间: DevOps.Model
*
* 功 能： SysExceptionLogInfo实体类
*
* 类 名： SysExceptionLogInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 异常日志记录表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_EXCEPTION_LOG_INFO")]
    public class SysExceptionLogInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public SysExceptionLogInfoEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("-", "");

            //异常类型 100：警告  101：严重警告
            this.exception_type = 0;

            //服务名称
            this.service_name = string.Empty;

            //异常标题
            this.exception_title = string.Empty;

            //操作明细
            this.detail = string.Empty;

            //创建日期
            this.create_date = DateTime.Now;

            //状态 100-未解决，101-已解决
            this.status = 100;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 异常类型 100：警告  101：严重警告
        /// </summary>
        [DBFieldInfo(ColumnName = "EXCEPTION_TYPE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public int exception_type
        {
            get; set;
        }

        /// <summary>
        /// 服务名称
        /// </summary>
        [DBFieldInfo(ColumnName = "SERVICE_NAME", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string service_name
        {
            get; set;
        }

        /// <summary>
        /// 异常标题
        /// </summary>
        [DBFieldInfo(ColumnName = "EXCEPTION_TITLE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string exception_title
        {
            get; set;
        }

        /// <summary>
        /// 操作明细
        /// </summary>
        [DBFieldInfo(ColumnName = "DETAIL", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string detail
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_DATE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 状态 100-未解决，101-已解决
        /// </summary>
        [DBFieldInfo(ColumnName = "STATUS", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public int status
        {
            get; set;
        }

    }
}
