/*
* 命名空间: DevOps.Model
*
* 功 能： SysUserRoleRe实体类
*
* 类 名： SysUserRoleReEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 用户-角色关系表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_USER_ROLE_RE")]
    public class SysUserRoleReEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysUserRoleReEntity()
     {

           //用户唯一标识符
           this.user_id = string.Empty;

           //角色唯一标识符
           this.role_id = string.Empty;

           //唯一标识符
           this.id = Guid.NewGuid().ToString().Replace("-", "");
        }

        /// <summary>
        /// 用户唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "USER_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string user_id
        {
            get; set;
        }

        /// <summary>
        /// 角色唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ROLE_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string role_id
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }
    }
}
