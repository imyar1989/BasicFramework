/*
* 命名空间: DevOps.Model
*
* 功 能： SafeServiceInfo实体类
*
* 类 名： SafeServiceInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/05/09 10:31:19 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 官网首页轮播图信息表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SAFE_SERVICE_INFO")]
    public class SafeServiceInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public SafeServiceInfoEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("-", "");

            //服务名称【ip地址和服务名称加起来，必须唯一】
            this.name = string.Empty;

            //显示名称
            this.display_name = string.Empty;

            //寄宿方式【IIS，windows，Linux】
            this.boarding_mode = string.Empty;

            //IP地址
            this.ip = string.Empty;

            //服务器登录账号【必须加码】
            this.port = string.Empty;

            //服务器登录账号【必须加码】
            this.login_account = string.Empty;

            //服务器登录密码【必须加码】
            this.login_password = string.Empty;

            //文件地址【必须加码,iis指文件夹，服务指EXE地址】
            this.file_path = string.Empty;

            //IIS托管模式【集成、经典】
            this.hosted_mode = string.Empty;

            //框架版本【4,2,0 表示无托管】
            this.framework_version = 0;

            //状态【无状态0，安装或创建100，开始或运行200，停止300，删除或卸载400】
            this.status = 0;

            //权限码【没有这个权限码，无法操作服务】
            this.authority_code = string.Empty;

            //描述
            this.describe = string.Empty;

            //是否已经上传发布文件
            this.is_upload_file = false;

            //可执行文件名称
            this.exe_file = string.Empty;

            //伺服端口
            this.servo_port = string.Empty;

            //服务类型
            this.service_type = string.Empty;

        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 服务名称【ip地址和服务名称加起来，必须唯一】
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 显示名称
        /// </summary>
        [DBFieldInfo(ColumnName = "DISPLAY_NAME", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string display_name
        {
            get; set;
        }

        /// <summary>
        /// 寄宿方式【IIS，windows，Linux】
        /// </summary>
        [DBFieldInfo(ColumnName = "BOARDING_MODE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string boarding_mode
        {
            get; set;
        }

        /// <summary>
        /// IP地址
        /// </summary>
        [DBFieldInfo(ColumnName = "IP", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string ip
        {
            get; set;
        }

        /// <summary>
        /// 端口号
        /// </summary>
        [DBFieldInfo(ColumnName = "PORT", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string port
        {
            get; set;
        }

        /// <summary>
        /// 服务器登录账号【必须加码】
        /// </summary>
        [DBFieldInfo(ColumnName = "LOGIN_ACCOUNT", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string login_account
        {
            get; set;
        }

        /// <summary>
        /// 服务器登录密码【必须加码】
        /// </summary>
        [DBFieldInfo(ColumnName = "LOGIN_PASSWORD", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string login_password
        {
            get; set;
        }

        /// <summary>
        /// 文件地址【必须加码,iis指文件夹，服务指EXE地址】
        /// </summary>
        [DBFieldInfo(ColumnName = "FILE_PATH", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string file_path
        {
            get; set;
        }

        /// <summary>
        /// IIS托管模式【集成、经典】
        /// </summary>
        [DBFieldInfo(ColumnName = "HOSTED_MODE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string hosted_mode
        {
            get; set;
        }

        /// <summary>
        /// 框架版本【4,2,0 表示无托管】
        /// </summary>
        [DBFieldInfo(ColumnName = "FRAMEWORK_VERSION", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public int framework_version
        {
            get; set;
        }

        /// <summary>
        /// 状态【无状态0，安装或创建100，开始或运行200，停止300，删除或卸载400】
        /// </summary>
        [DBFieldInfo(ColumnName = "STATUS", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public int status
        {
            get; set;
        }

        /// <summary>
        /// 权限码【没有这个权限码，无法操作服务】
        /// </summary>
        [DBFieldInfo(ColumnName = "AUTHORITY_CODE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string authority_code
        {
            get; set;
        }

        /// <summary>
        /// 描述
        /// </summary>
        [DBFieldInfo(ColumnName = "DESCRIBE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string describe

        {
            get; set;
        }

        /// <summary>
        /// 是否已经上传发布文件
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_UPLOAD_FILE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public bool is_upload_file
        {
            get; set;
        }

        /// <summary>
        /// 可执行文件名称
        /// </summary>
        [DBFieldInfo(ColumnName = "EXE_FILE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string exe_file

        {
            get; set;
        }


        /// <summary>
        /// 伺服端口
        /// </summary>
        [DBFieldInfo(ColumnName = "SERVO_PORT", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string servo_port
        {
            get; set;
        }

        /// <summary>
        /// 服务类型
        /// </summary>
        [DBFieldInfo(ColumnName = "SERVICE_TYPE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string service_type
        {
            get; set;
        }

    }
}
