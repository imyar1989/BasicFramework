/*
* 命名空间: DevOps.Model
*
* 功 能： SysDepartmentInfo实体类
*
* 类 名： SysDepartmentInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 部门表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_DEPARTMENT_INFO")]
    public class SysDepartmentInfoEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysDepartmentInfoEntity()
     {

           //修改者日期
           this.modifier_date = DateTime.Now;

           //是否有效
           this.is_valid = true;

           //是否被逻辑删除
           this.is_deleted = false;

           //部门名称
           this.name = string.Empty;

           //领导人名称
           this.leader = string.Empty;

           //联系电话
           this.phone = string.Empty;

           //邮件地址
           this.email = string.Empty;

           //备注
           this.remarks = string.Empty;

           //修改者
           this.modifier_id = string.Empty;

           //父节点唯一标识符
           this.parent_id = string.Empty;

           //排序
           this.sort = 0;

           //创建者
           this.creator_id = string.Empty;

           //创建者姓名
           this.creator_name = string.Empty;

           //创建日期
           this.create_date = DateTime.Now;

           //修改者姓名
           this.modifier_name = string.Empty;

           //唯一标识符
           this.id = Guid.NewGuid().ToString().Replace("-","");
      }

        /// <summary>
        /// 修改者日期
        /// </summary>
        [DBFieldInfo(ColumnName = "MODIFIER_DATE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime modifier_date
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_VALID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否被逻辑删除
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_DELETED",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_deleted
        {
            get; set;
        }

        /// <summary>
        /// 部门名称
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 领导人名称
        /// </summary>
        [DBFieldInfo(ColumnName = "LEADER",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string leader
        {
            get; set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        [DBFieldInfo(ColumnName = "PHONE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string phone
        {
            get; set;
        }

        /// <summary>
        /// 邮件地址
        /// </summary>
        [DBFieldInfo(ColumnName = "EMAIL",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DBFieldInfo(ColumnName = "REMARKS",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 修改者
        /// </summary>
        [DBFieldInfo(ColumnName = "MODIFIER_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string modifier_id
        {
            get; set;
        }

        /// <summary>
        /// 父节点唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "PARENT_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 创建者
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATOR_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string creator_id
        {
            get; set;
        }

        /// <summary>
        /// 创建者姓名
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATOR_NAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string creator_name
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_DATE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 修改者姓名
        /// </summary>
        [DBFieldInfo(ColumnName = "MODIFIER_NAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string modifier_name
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }
    }
}
