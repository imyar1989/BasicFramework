/*
* 命名空间: DevOps.Model
*
* 功 能： 基础应用表实体类
*
* 类 名： SafeApplicationInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/05/11 09:16:30 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 基础应用表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SAFE_APPLICATION_INFO")]
    public class SafeApplicationInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public SafeApplicationInfoEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("-", "");

            //应用名称
            this.applicatian_name = string.Empty;

            //应用类型（100：App，200：微信，300：web，400：桌面）
            this.applicatian_type = 0;

            //应用秘钥
            this.appsecret = string.Empty;

            //应用ID
            this.unionid = string.Empty;

            //应用登录名
            this.accountid = string.Empty;

            //是否有效
            this.is_valid = true;

            //是否删除
            this.is_deleted = false;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 应用名称
        /// </summary>
        [DBFieldInfo(ColumnName = "APPLICATIAN_NAME", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string applicatian_name
        {
            get; set;
        }

        /// <summary>
        /// 应用类型（100：App，200：微信，300：web，400：桌面）
        /// </summary>
        [DBFieldInfo(ColumnName = "APPLICATIAN_TYPE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public int applicatian_type
        {
            get; set;
        }

        /// <summary>
        /// 应用秘钥
        /// </summary>
        [DBFieldInfo(ColumnName = "APPSECRET", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string appsecret
        {
            get; set;
        }

        /// <summary>
        /// 应用ID
        /// </summary>
        [DBFieldInfo(ColumnName = "UNIONID", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string unionid
        {
            get; set;
        }

        /// <summary>
        /// 应用登录名
        /// </summary>
        [DBFieldInfo(ColumnName = "ACCOUNTID", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string accountid
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_VALID", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否删除
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_DELETED", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public bool is_deleted
        {
            get; set;
        }
    }
}
