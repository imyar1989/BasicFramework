/*
* 命名空间: DevOps.Model
*
* 功 能： FlowButtonInfo实体类
*
* 类 名： FlowButtonInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/01/14 09:25:28 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using Dapper.Library;
    using System;

    /// <summary>
    /// 流程按钮
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "FLOW_BUTTON_INFO")]
    public class FlowButtonInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowButtonInfoEntity()
        {

            //唯一标识符
            this.id = string.Empty;

            //按钮脚本
            this.script = string.Empty;

            //备注说明
            this.note = string.Empty;

            //排序
            this.sort = 0;

            //按钮标题
            this.title = string.Empty;

            //按钮图标
            this.icon = string.Empty;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 按钮脚本
        /// </summary>
        [DBFieldInfo(ColumnName = "SCRIPT", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string script
        {
            get; set;
        }

        /// <summary>
        /// 备注说明
        /// </summary>
        [DBFieldInfo(ColumnName = "NOTE", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string note
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int sort
        {
            get; set;
        }

        /// <summary>
        /// 按钮标题
        /// </summary>
        [DBFieldInfo(ColumnName = "TITLE", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string title
        {
            get; set;
        }

        /// <summary>
        /// 按钮图标
        /// </summary>
        [DBFieldInfo(ColumnName = "ICON", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string icon
        {
            get; set;
        }
    }
}
