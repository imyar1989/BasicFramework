/*
* 命名空间: Official.Model
*
* 功 能： WebBasicSettings实体类
*
* 类 名： WebBasicSettingsEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/05/24 14:44:14 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Official.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 官网基础设置表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "official", TableName = "WEB_BASIC_SETTINGS")]
    public class WebBasicSettingsEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public WebBasicSettingsEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //LOG保存地址
            this.log_pic = string.Empty;

            //网站标题【在界面底部显示专业创造价值 诚信铸就未来】
            this.website_title = string.Empty;

            //友情链接【包括链接名称和链接地址，使用json数组保存】
            this.friendship_links = string.Empty;

            //二维码地址
            this.qr_code = string.Empty;

            //电话号码
            this.telephone = string.Empty;

            //邮箱
            this.email = string.Empty;

            //传真
            this.fax = string.Empty;

            //公司地址
            this.address = string.Empty;

            //公司授权内容信息【Copyright ? 2019-2020  All Rights Reserved 四川地理智绘科技】
            this.copyright_info = string.Empty;

            //备案编号
            this.record_number = string.Empty;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// LOG保存地址
        /// </summary>
        [DBFieldInfo(ColumnName = "LOG_PIC", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string log_pic
        {
            get; set;
        }

        /// <summary>
        /// 网站标题【在界面底部显示专业创造价值 诚信铸就未来】
        /// </summary>
        [DBFieldInfo(ColumnName = "WEBSITE_TITLE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string website_title
        {
            get; set;
        }

        /// <summary>
        /// 友情链接【包括链接名称和链接地址，使用json数组保存】
        /// </summary>
        [DBFieldInfo(ColumnName = "FRIENDSHIP_LINKS", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string friendship_links
        {
            get; set;
        }

        /// <summary>
        /// 二维码地址
        /// </summary>
        [DBFieldInfo(ColumnName = "QR_CODE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string qr_code
        {
            get; set;
        }

        /// <summary>
        /// 电话号码
        /// </summary>
        [DBFieldInfo(ColumnName = "TELEPHONE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string telephone
        {
            get; set;
        }

        /// <summary>
        /// 邮箱
        /// </summary>
        [DBFieldInfo(ColumnName = "EMAIL", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 传真
        /// </summary>
        [DBFieldInfo(ColumnName = "FAX", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string fax
        {
            get; set;
        }

        /// <summary>
        /// 公司地址
        /// </summary>
        [DBFieldInfo(ColumnName = "ADDRESS", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string address
        {
            get; set;
        }

        /// <summary>
        /// 公司授权内容信息【Copyright ? 2019-2020  All Rights Reserved 四川地理智绘科技】
        /// </summary>
        [DBFieldInfo(ColumnName = "COPYRIGHT_INFO", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string copyright_info
        {
            get; set;
        }

        /// <summary>
        /// 备案编号
        /// </summary>
        [DBFieldInfo(ColumnName = "RECORD_NUMBER", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string record_number
        {
            get; set;
        }
    }
}
