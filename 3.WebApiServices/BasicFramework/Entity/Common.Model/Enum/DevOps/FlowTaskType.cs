﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

/*
* 命名空间:  Common.Model
*
* 功 能：流程任务类型枚举
*
* 类 名： Flow
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/1/19 9:32:12     LW     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 流程任务类型枚举
    /// 任务类型 100常规 110指派 120委托 130转交 140退回 150抄送 160前加签 170并签 180后加签
    /// </summary>
    public enum FlowTaskType
    {

        /// <summary>
        /// 100常规
        /// </summary>
        [Description("常规")]
        General = 100,

        /// <summary>
        /// 110指派
        /// </summary>
        [Description("指派")]
        Assign = 110,

        /// <summary>
        /// 120委托
        /// </summary>
        [Description("委托")]
        Entrust = 120,

        /// <summary>
        /// 130转交
        /// </summary>
        [Description("转交")]
        DeliverTo = 130,

        /// <summary>
        /// 140退回
        /// </summary>
        [Description("退回")]
        SendBack = 140,

        /// <summary>
        /// 150抄送
        /// </summary>
        [Description("抄送")]
        CarbonCopy = 150,

        /// <summary>
        /// 160前加签
        /// </summary>
        [Description("前加签")]
        FormerSignature = 160,

        /// <summary>
        /// 170并签
        /// </summary>
        [Description("并签")]
        MergeSignature = 170,

        /// <summary>
        /// 180后加签
        /// </summary>
        [Description("后加签")]
        AfterSignature = 170,
    }
}
