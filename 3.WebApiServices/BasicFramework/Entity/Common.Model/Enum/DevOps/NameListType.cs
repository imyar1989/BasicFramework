﻿using System.ComponentModel;

/*
* 命名空间: Common.Model
*
* 功 能： 名单类型
*
* 类 名： NameListType
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/14 11:23:31 				Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 名单类型
    /// </summary>
    public enum NameListType
    {
        /// <summary>
        /// 警告类型
        /// </summary>
        [Description("警告类型")]
        Warning=100,

        /// <summary>
        /// 禁止访问
        /// </summary>
        [Description("禁止访问")]
        NoAccess = 200,
    }
}
