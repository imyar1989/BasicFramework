﻿using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 菜单级数
    /// </summary>
    public enum MenuLevel : int
    {
        /// <summary>
        /// 一级菜单，一级菜单（默认为内置项目名称）
        /// </summary>
        [Description("一级菜单")]
        First = 10,

        /// <summary>
        /// 二级菜单
        /// </summary>
        [Description("二级菜单")]
        Second = 20,

        /// <summary>
        /// 三级菜单
        /// </summary>
        [Description("三级菜单")]
        Third = 30,

        /// <summary>
        /// 四级菜单
        /// </summary>
        [Description("四级菜单")]
        Fourth = 40
    }
}
