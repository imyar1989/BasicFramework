﻿using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// GIS服务类型
    /// </summary>
    public enum GisServiceType
    {
        /// <summary>
        ///TileServer
        /// </summary>
        [Description("TileServer")]
        TileServer = 100,

        /// <summary>
        ///FeatureServer
        /// </summary>
        [Description("FeatureServer")]
        FeatureServer = 110,
    }
}
