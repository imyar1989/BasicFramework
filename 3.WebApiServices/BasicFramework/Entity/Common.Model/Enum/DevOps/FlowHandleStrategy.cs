﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 流程步骤处理策略
    /// </summary>
    public enum FlowHandleStrategy
    {
        //处理策略 100所有人必须同意  110一人同意即可 120依据人数比例
        /// <summary>
        /// 所有人必须同意
        /// </summary>
        [Description("所有人必须同意")]
        EveryoneHasAgree = 100,

        /// <summary>
        /// 一人同意即可
        /// </summary>
        [Description("一人同意即可")]
        JustOneAgree = 110,

        /// <summary>
        /// 依据人数比例
        /// </summary>
        [Description("依据人数比例")]
        Wahlbeteiligung = 120,

    }
}
