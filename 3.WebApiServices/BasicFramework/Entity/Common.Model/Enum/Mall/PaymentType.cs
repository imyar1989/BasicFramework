﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 支付方式  100'微信', 110'支付宝',120 '银联', 130'余额'
    /// </summary>
    public enum PaymentType
    {
        /// <summary>
        /// 微信
        /// </summary>
        [Description("微信")]
        WeChat = 100,

        /// <summary>
        /// 支付宝
        /// </summary>
        [Description("支付宝")]
        Alipay = 110,

        /// <summary>
        /// 银联
        /// </summary>
        [Description("银联")]
        Unionpay = 120,

        /// <summary>
        /// 余额
        /// </summary>
        [Description("余额")]
        Balance = 130,
    }
}
