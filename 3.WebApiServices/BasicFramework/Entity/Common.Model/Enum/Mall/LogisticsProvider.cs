﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 物流商类型
    /// </summary>
    public enum LogisticsProvider
    {
        /// <summary>
        /// 申通快递
        /// </summary>
        [Description("申通快递")]
        STOExpress = 100,

        /// <summary>
        /// EMS
        /// </summary>
        [Description("EMS")]
        EMSExpress = 110,

        /// <summary>
        /// 顺丰快递
        /// </summary>
        [Description("顺丰快递")]
        SFExpress = 120,

        /// <summary>
        /// 韵达快递
        /// </summary>
        [Description("韵达快递")]
        YunDaExpress = 130,

        /// <summary>
        /// 圆通快递
        /// </summary>
        [Description("圆通快递")]
        YTOExpress = 140,

        /// <summary>
        /// 中通快递
        /// </summary>
        [Description("中通快递")]
        ZTOExpress = 150,

        /// <summary>
        /// 百世快递
        /// </summary>
        [Description("百世快递")]
        HTKYExpress = 160,

        /// <summary>
        /// 天天快递
        /// </summary>
        [Description("天天快递")]
        TTKExpress = 170,

        /// <summary>
        /// 宅急送
        /// </summary>
        [Description("宅急送")]
        ZJSExpress = 180,

        /// <summary>
        /// 优速快递
        /// </summary>
        [Description("优速快递")]
        UCExpress = 190,

    }
}
