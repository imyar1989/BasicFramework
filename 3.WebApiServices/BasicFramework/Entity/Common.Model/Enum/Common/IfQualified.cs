﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 是否合格
    /// </summary>
    public enum IfQualified
    {
        /// <summary>
        ///合格
        /// </summary>
        [Description("合格")]
        Qualified = 100,

        /// <summary>
        /// 不合格
        /// </summary>
        [Description("不合格")]
        NoQualified = 200,
    }
}
