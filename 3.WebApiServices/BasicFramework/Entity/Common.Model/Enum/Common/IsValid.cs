﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 是否有效
    /// </summary>
    public enum IsValid
    {
        /// <summary>
        /// 100有效
        /// </summary>
        [Description("有效")]
        Valid = 100,

        /// <summary>
        /// 110无效
        /// </summary>
        [Description("无效")]
        Invalid = 110,
    }
}
