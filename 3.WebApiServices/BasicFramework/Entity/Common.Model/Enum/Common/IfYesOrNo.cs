﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

/*
* 命名空间:  Common.Model
*
* 功 能：是 或 否
*
* 类 名：IfYesOrNo  
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1   2021/8/5 13:31:04  LW     创建
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 是 或 否
    /// </summary>
	public enum IfYesOrNo
    {
        /// <summary>
        ///是 
        /// </summary>
        [Description("是")]
        Yes = 100,

        /// <summary>
        /// 否
        /// </summary>
        [Description("否")]
        No = 110,
    }
}
