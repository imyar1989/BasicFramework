﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// Sde空间数据库表中几个图形类型
    /// </summary>
    public enum SdeGeometricType
    {
        /// <summary>
        /// 点 
        /// </summary>
        [Description("点")]
        Point = 100,

        /// <summary>
        /// 线
        /// </summary>
        [Description("线")]
        Line = 110,

        /// <summary>
        /// 多边形
        /// </summary>
        [Description("多边形")]
        Polygon = 120,
    }
}
