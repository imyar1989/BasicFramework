﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

/*
* 命名空间:  Common.Model
*
* 功 能：性别类型
*
* 类 名：GenderType  
*
* Version   变更日期        负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1   2021/8/5 15:43:35  LW     创建
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 性别类型 性别 1男 2女 0未知
    /// </summary>
	public enum GenderType
    {

        /// <summary>
        ///0未知 
        /// </summary>
        [Description("未知")]
        Unknown = 0,

        /// <summary>
        ///1男 
        /// </summary>
        [Description("男")]
        Man = 1,

        /// <summary>
        ///2女 
        /// </summary>
        [Description("女")]
        Woman = 2,

    }
}
