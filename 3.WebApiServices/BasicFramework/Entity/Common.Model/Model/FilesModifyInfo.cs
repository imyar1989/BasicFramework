﻿using Newtonsoft.Json;

namespace Common.Model
{
    /// <summary>
    /// 文件操作实体
    /// </summary>
    public class FilesModifyInfo
    {
        ///<summary>
        /// 名称或块名称
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string name { get; set; }

        /// <summary>
        /// 服务名称文件夹路径
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ServiceName servicePath { get; set; }

        /// <summary>
        /// 文件夹路径
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string folderPath { get; set; }

        /// <summary>
        /// 临时路径
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string temp_path { get; set; }
        
        /// <summary>
        /// 大小
        /// </summary>
        public double size { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string media_type { get; set; }

        /// <summary>
        /// 格式
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string media_sub_type { get; set; }

        /// <summary>
        /// 文件二进制信息
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public byte[] fileByte { get; set; }

    }
}
