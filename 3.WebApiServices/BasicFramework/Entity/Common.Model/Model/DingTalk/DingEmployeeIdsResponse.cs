﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间:  Common.Model
*
* 功 能：员工IDs信息
*
* 类 名：DingEmployeeIdsResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/9 10:40:51       LW      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 员工IDs信息
    /// </summary>
    public class DingEmployeeIdsResponse
    {
        /// <summary>
        /// errcode
        /// </summary>
        public int errcode { get; set; }

        /// <summary>
        /// errmsg
        /// </summary>
        public string errmsg { get; set; }

        /// <summary>
        /// 调用结果
        /// </summary>
        public bool success { get; set; }

        /// <summary>
        /// 请求ID
        /// </summary>
        public string request_id { get; set; }

        /// <summary>
        /// result
        /// </summary>
        public DingEmployeePageResult result { get; set; }
    }

    /// <summary>
    /// 员工ID分页信息
    /// </summary>
    public class DingEmployeePageResult
    {
        /// <summary>
        /// 查询到的员工userid。 
        /// </summary>
        public string[] data_list { get; set; }

        /// <summary>
        /// 下一次分页调用的offset值，当返回结果里没有nextCursor时，表示分页结果。
        /// </summary>
        public int next_cursor { get; set; }

    }
}
