﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间:  Common.Model
*
* 功 能：考勤组人员信息分批查询
*
* 类 名：DingGroupMemberUserIds
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/11 9:07:19       LW      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 考勤组人员信息分批查询
    /// </summary>
    public class DingGroupMemberUserIds
    {
        /// <summary>
        /// result
        /// </summary>
        public List<string> result { get; set; }

        /// <summary>
        /// cursor
        /// </summary>
        public long cursor { get; set; }

        /// <summary>
        /// 是否还有更多
        /// </summary>
        public bool has_more { get; set; }
    }
}
