﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 服务信息
    /// </summary>
    public class ServiceInfo
    {
        /// <summary>
        /// 服务名称
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// 服务显示名称
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// 服务描述信息
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 服务地址
        /// </summary>
        public string BaseAddress { get; set; }

    }
}
