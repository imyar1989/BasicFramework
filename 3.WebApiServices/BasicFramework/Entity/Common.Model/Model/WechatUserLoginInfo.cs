﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Model
{

    /// <summary>
    /// 微信用户登录信息
    /// </summary>
    public class WechatUserLoginInfo
    {
        /// <summary>
        /// 唯一编码
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// OpenId
        /// </summary>
        public string open_id
        {
            get; set;
        }

        /// <summary>
        /// 头像信息
        /// </summary>
        public string head_img
        {
            get; set;
        }

        /// <summary>
        /// 用户昵称
        /// </summary>
        public string nickname
        {
            get; set;
        }

        /// <summary>
        /// 性别 0未知 1男 2女
        /// </summary>
        public int sex
        {
            get; set;
        }

        /// <summary>
        /// 姓名
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 用户所在国家
        /// </summary>
        public string country
        {
            get; set;
        }

        /// <summary>
        ///  用户所在省份
        /// </summary>
        public string province
        {
            get; set;
        }

        /// <summary>
        /// 用户所在城市
        /// </summary>
        public string city
        {
            get; set;
        }

        /// <summary>
        /// 电话号码
        /// </summary>
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// 支付密码
        /// </summary>
        public string pay_password
        {
            get; set;
        }

        /// <summary>
        /// 积分
        /// </summary>
        public int integral
        {
            get; set;
        }

        /// <summary>
        /// 消费总金额
        /// </summary>
        public decimal monetary
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 是否有效  100有效 110无效
        /// </summary>
        public int is_valid
        {
            get; set;
        }

        /// <summary>
        /// 授权token
        /// </summary>
        public string token { get; set; }

    }
}
