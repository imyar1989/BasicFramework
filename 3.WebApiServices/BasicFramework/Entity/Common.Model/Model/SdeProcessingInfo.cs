﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 空间数据库数据处理信息
    /// </summary>
    public class SdeProcessingInfo
    {

        /// <summary>
        /// 构造函数
        /// </summary>
        public SdeProcessingInfo() {

            geometricInfo = new List<Dictionary<string, object>>();

            fieldInfos = new Dictionary<string, object>();
        }

        /// <summary>
        /// 操作类型 Insert Modify  Remove
        /// </summary>
        public int operationType { get; set; }

        /// <summary>
        /// 几何图形类型 Point Line Polygon 
        /// </summary>
        public int geometricType { get; set; }

        /// <summary>
        /// 几何图形坐标点信息
        /// </summary>
        public List<Dictionary<string,object>> geometricInfo { get; set; }

        /// <summary>
        /// 要素类名称
        /// </summary>
        public string strFeatureClassName { get; set; }

        /// <summary>
        /// 参数信息
        /// </summary>
        public Dictionary<string, object> fieldInfos { get; set; }

        /// <summary>
        /// where 条件
        /// </summary>
        public string strWhere { get; set; }

    }
}
