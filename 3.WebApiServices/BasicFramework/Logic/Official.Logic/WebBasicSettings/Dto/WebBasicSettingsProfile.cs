﻿using AutoMapper;
using Official.Model;

namespace Official.Logic
{
    /// <summary>
    /// 管网基础设置功能-实体映射帮助类
    /// </summary>
    public class WebBasicSettingsProfile: Profile
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public WebBasicSettingsProfile() {

            CreateMap<WebBasicSettingsRequest, WebBasicSettingsEntity>();

        }
    }
}
