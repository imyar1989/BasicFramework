﻿using Common.Model;
using Official.Model;
using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  Official.Logic
*
* 功 能：管网基础设置功能接口
*
* 类 名：IWebBasicSettingsService  
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1   2021/5/24 14:29:42  LW     创建
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
namespace Official.Logic
{
    /// <summary>
    /// 管网基础设置功能接口
    /// </summary>
	public interface IWebBasicSettingsService
    {
        /// <summary>
        /// 获取官网基础设置
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<WebBasicSettingsEntity> LoadWebBasicInfo();
        /// <summary>
        /// 修改官网基础设置
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ModifyWebBasic(WebBasicSettingsRequest inputInfo);
    }
}
