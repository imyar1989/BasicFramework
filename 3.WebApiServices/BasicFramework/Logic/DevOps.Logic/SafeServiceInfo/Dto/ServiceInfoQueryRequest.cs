﻿/*
* 命名空间: DevOps.Logic
*
* 功 能： 获取服务请求条件实体
*
* 类 名： ServiceInfoQueryRequest
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/5/9 14:26:57 	Harvey    创建
* ─────────────────────────────────────────────────
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Logic
{
    /// <summary>
    /// 获取服务请求条件实体
    /// </summary>
    public class ServiceInfoQueryRequest
    {
        /// <summary>
        /// IP地址
        /// </summary>
        public string ip { get; set; }

        /// <summary>
        /// 占用端口
        /// </summary>
        public string port { get; set; }

        /// <summary>
        /// 服务名称
        /// </summary>
        public string service_name { get; set; }
    }
}
