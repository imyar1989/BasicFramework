﻿using Common.Library;
using Common.Model;
using Container.Library;
using Dapper.Library;
using DevOps.Model;
using Serialize.Library;
using System;
using System.Collections.Generic;

/*
* 命名空间:  DevOps.Logic
*
* 功 能：接口服务相关逻辑
*
* 类 名： SysServerApiInfoImpl
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/12/30 15:04:12               Harvey      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 接口服务相关逻辑
    /// </summary>
    public class SysServerApiInfoImpl : OperationLogicImpl, ISysServerApiInfoService
    {
        #region 查询

        /// <summary>
        /// 根据关键字 分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<SysServerApiInfoResponse>> LoadPageList(ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<SysServerApiInfoResponse>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<SysServerApiInfoEntity>();
                if (inputInfo.parameters.IsNotNullOrEmpty())
                {
                    result.Where(a => a.server_name.Contains(inputInfo.parameters));
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                var listInfo = result.PageList(inputInfo.page, inputInfo.limit);

                if (listInfo.Items.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo.Items.MapToList<SysServerApiInfoResponse>();
                    resultInfo.Count = listInfo.Total;
                }
                else
                {
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }

        #endregion

        #region 更新

        /// <summary>
        /// 更新
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<int> Save(SysServerApiInfoSaveRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = 0;
                if (inputInfo.id.IsNotNullOrEmpty())
                {
                    #region Modify

                    //判断是否 有键值重复
                    var infoCount = con.QuerySet<SysServerApiInfoEntity>().Where(p => p.id != inputInfo.id && p.server_name.Equals(inputInfo.server_name)).Count();
                    if (infoCount == 0)
                    {
                        var modifyInfo = con.QuerySet<SysServerApiInfoEntity>().Where(p => p.id == inputInfo.id).Get();
                        if (modifyInfo != null)
                        {
                            modifyInfo.server_name = inputInfo.server_name;
                            modifyInfo.swagger_url = inputInfo.swagger_url;
                            modifyInfo.remarks = inputInfo.remarks;
                            result = con.CommandSet<SysServerApiInfoEntity>().Update(modifyInfo);

                            if (result > 0)
                            {
                                resultInfo.Code = ActionCodes.Success;
                                resultInfo.Msg = "修改成功！";
                                AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.SysServerApiInfo, $"修改接口服务信息，修改信息：{JsonHelper.ToJson(inputInfo)}");
                            }
                            else
                            {
                                resultInfo.Code = ActionCodes.InvalidOperation;
                                resultInfo.Msg = "修改失败！";
                            }
                        }
                        else
                        {
                            resultInfo.Code = ActionCodes.InvalidOperation;
                            resultInfo.Msg = "对应Id的信息不存在！";
                        }
                    }
                    else
                    {
                        resultInfo.Code = ActionCodes.InvalidOperation;
                        resultInfo.Msg = "接口服务已存在！";
                    }
                    #endregion
                }
                else
                {
                    #region Add
                    var addInfo = inputInfo.MapTo<SysServerApiInfoEntity>();
                    addInfo.id = GuidHelper.GetGuid();
                    addInfo.create_time = DateTime.Now;
                    
                    var infoCount = con.QuerySet<SysServerApiInfoEntity>().Where(p => p.server_name.Equals(inputInfo.server_name)).Count();
                    if (infoCount == 0)
                    {
                        result = con.CommandSet<SysServerApiInfoEntity>().Insert(addInfo);

                        if (result > 0)
                        {

                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "添加成功！";
                            AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.SysServerApiInfo, $"添加了一个接口服务，新增信息：{JsonHelper.ToJson(inputInfo)}");
                        }
                        else
                        {
                            resultInfo.Code = ActionCodes.InvalidOperation;
                            resultInfo.Msg = "添加失败！";
                        }
                    }
                    else
                    {
                        resultInfo.Code = ActionCodes.InvalidOperation;
                        resultInfo.Msg = "接口服务已存在！";
                    }

                    #endregion
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 删除接口服务
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                //删除
                var result = con.CommandSet<SysServerApiInfoEntity>().Where(p => p.id.In(ids.ToArray())).Delete();
                if (result > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "删除成功！";
                    AddOperationLog(OperationLogType.RemoveOperation, BusinessTitleType.SysServerApiInfo, $"删除接口服务成功，物理删除信息：{JsonHelper.ToJson(ids)}");
                }
                else
                {
                    resultInfo.Msg = "删除失败！";
                }
            }
            return resultInfo;
        }

        #endregion
    }
}
