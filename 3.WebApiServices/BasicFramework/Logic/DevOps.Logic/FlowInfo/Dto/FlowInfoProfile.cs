﻿using AutoMapper;
using Common.Library;
using Common.Model;
using DevOps.Model;
using System;

/*
* 命名空间:  DevOps.Logic
*
* 功 能：XXX
*
* 类 名： FlowFlowInfoProfile
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/1/15 9:41:11               Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 流程基础信息映射类
    /// </summary>
    public class FlowInfoProfile : Profile
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowInfoProfile() {

            CreateMap<FlowInfoRequest, FlowInfoEntity>();

            CreateMap<FlowInfoEntity, FlowInfoResponse>()
            .ForMember(p => p.statusname, opt => opt.MapFrom(x => (Convert.ToInt32(x.status)).GetEnumDescriptionByValue(typeof(FlowType))));
            CreateMap<FlowInfoEntity, FlowDetailInfoResponse>();


            //流程步骤映射
            CreateMap<FlowStepInfoEntity, FlowStepInfoOperation>();


        }
    }
}
