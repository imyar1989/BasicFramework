﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 获取当前节点和后续节点信息
    /// </summary>
    public class FlowStepCurrentInfo
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowStepCurrentInfo() {

            currentStep = new FlowStepInfoOperation();

            nextSteps = new List<FlowStepInfoOperation>();
        }

        /// <summary>
        /// 当前节点
        /// </summary>
        public FlowStepInfoOperation currentStep { get; set; }

        /// <summary>
        /// 后续节点
        /// </summary>
        public List<FlowStepInfoOperation> nextSteps { get; set; }
    }
}
