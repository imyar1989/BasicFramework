﻿using DevOps.Model;
using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  DevOps.Logic
*
* 功 能：流程基础信息返回信息
*
* 类 名： FlowInfoResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/1/15 9:41:32               Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 流程基础信息返回信息
    /// </summary>
    public class FlowInfoResponse: FlowInfoEntity
    {

        /// <summary>
        /// 分类名称
        /// </summary>
        public string flow_categoryname
        {
            get; set;
        }
       
        /// <summary>
        /// 状态 100设计中 110已安装 120已卸载 130已删除
        /// </summary>
        public string statusname
        {
            get; set;
        }

    }
}
