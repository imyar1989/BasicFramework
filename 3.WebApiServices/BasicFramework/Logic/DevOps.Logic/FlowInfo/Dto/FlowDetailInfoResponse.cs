﻿using DevOps.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 流程详细信息
    /// </summary>
    public class FlowDetailInfoResponse
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowDetailInfoResponse() {

            nodeList = new List<FlowStepInfoEntity>();
            lineList = new List<FlowLineInfoEntity>();

        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 名称
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 分类
        /// </summary>
        public string flow_category
        {
            get; set;
        }

        /// <summary>
        /// 管理人员
        /// </summary>
        public string manager
        {
            get; set;
        }

        /// <summary>
        /// 管理人员名称
        /// </summary>
        public string managername
        {
            get; set;
        }


        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime create_time
        {
            get; set;
        }

        /// <summary>
        /// 创建人员id
        /// </summary>
        public string create_user
        {
            get; set;
        }

        /// <summary>
        /// 创建人员名称
        /// </summary>
        public string create_user_name
        {
            get; set;
        }

        /// <summary>
        /// 设计时JSON
        /// </summary>
        public string designer_json
        {
            get; set;
        }


        /// <summary>
        /// 安装时间
        /// </summary>
        public DateTime install_time
        {
            get; set;
        }

        /// <summary>
        /// 安装人员ID
        /// </summary>
        public string install_user
        {
            get; set;
        }

        /// <summary>
        /// 安装人员名称
        /// </summary>
        public string install_user_name
        {
            get; set;
        }

        /// <summary>
        /// 状态 100设计中 110已安装 120已卸载 130已删除
        /// </summary>
        public int status
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 表单关键字
        /// </summary>
        public string form_key
        {
            get; set;
        }

        /// <summary>
        /// 流程步骤节点信息
        /// </summary>
        public List<FlowStepInfoEntity> nodeList
        {
            get; set;
        }

        /// <summary>
        /// 流程连线信息
        /// </summary>
        public List<FlowLineInfoEntity> lineList
        {
            get; set;
        }
    }
}
