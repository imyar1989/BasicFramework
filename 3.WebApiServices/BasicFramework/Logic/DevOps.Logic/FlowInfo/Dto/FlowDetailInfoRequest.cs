﻿using DevOps.Model;
using System;
using System.Collections.Generic;

namespace DevOps.Logic
{
    /// <summary>
    /// 流程详细信息
    /// </summary>
    public class FlowDetailInfoRequest
    {

        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 名称
        /// </summary>
        public string name
        {
            get; set;
        }


        /// <summary>
        /// 流程步骤节点信息
        /// </summary>
        public List<FlowStepInfoEntity> nodeList
        {
            get; set;
        }

        /// <summary>
        /// 流程连线信息
        /// </summary>
        public List<FlowLineInfoEntity> lineList
        {
            get; set;
        }
    }
}
