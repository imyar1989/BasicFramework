﻿using AutoMapper;
using Common.Library;
using Common.Model;
using DevOps.Model;
using System;

namespace DevOps.Logic
{
    /// <summary>
    /// 权限相关映射
    /// </summary>
    public class OperationLogProfile : Profile
    {
        /// <summary>
        /// 权限数据实体与传输实体映射
        /// </summary>
        public OperationLogProfile()
        {
            CreateMap<SysOperationLogInfoEntity, OperationLogInfoResponse>()
                .ForMember(p => p.func_typename, opt => opt.MapFrom(x => (Convert.ToInt32(x.func_type)).GetEnumDescriptionByValue(typeof(OperationLogType))));
        }
    }
}
