﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 日志返回类
    /// </summary>
    public class OperationLogInfoResponse
    {/// <summary>
     /// 唯一标识符
     /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 所属服务名称
        /// </summary>
        public string service_name
        {
            get; set;
        }


        /// <summary>
        /// 功能类型 100：插入操作  101：更新操作 102：删除操作
        /// </summary>
        public int func_type
        {
            get; set;
        }
        /// <summary>
        /// 功能类型 100：插入操作  101：更新操作 102：删除操作
        /// </summary>
        public string func_typename
        {
            get; set;
        }
        
        /// <summary>
        /// 业务标题【用户管理】
        /// </summary>
        public string business_title
        {
            get; set;
        }
        /// <summary>
        /// 操作用户code
        /// </summary>
        public string user_code
        {
            get; set;
        }
        /// <summary>
        /// 操作用户名称
        /// </summary>
        public string user_name
        {
            get; set;
        }

        /// <summary>
        /// IP地址
        /// </summary>
        public string ip_address
        {
            get; set;
        }

        /// <summary>
        /// 操作明细
        /// </summary>
        public string detail
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime create_date
        {
            get; set;
        }
    }
}
