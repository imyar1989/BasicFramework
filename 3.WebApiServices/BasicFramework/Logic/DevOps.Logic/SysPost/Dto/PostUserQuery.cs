﻿using Validate.Library;

namespace DevOps.Logic
{

    /// <summary>
    /// 岗位用户查询请求实体
    /// </summary>
    public class PostUserQuery
    {
        /// <summary>
        ///岗位唯一标识符【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "岗位唯一标识符")]
        public string postid
        {
            get; set;
        }

    }
}
