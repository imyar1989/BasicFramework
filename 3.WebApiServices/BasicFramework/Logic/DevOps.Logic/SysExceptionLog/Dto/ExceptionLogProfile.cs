﻿using AutoMapper;
using Common.Library;
using Common.Model;
using DevOps.Model;
using System;

namespace DevOps.Logic
{
    /// <summary>
    /// 权限相关映射
    /// </summary>
    public class ExceptionLogProfile : Profile
    {
        /// <summary>
        /// 权限数据实体与传输实体映射
        /// </summary>
        public ExceptionLogProfile()
        {

            CreateMap<SysExceptionLogInfoEntity, ExceptionLogInfoResponse>()
                 .ForMember(p => p.exception_typename, opt => opt.MapFrom(x => (Convert.ToInt32(x.exception_type)).GetEnumDescriptionByValue(typeof(ExceptionLogType))))
                  .ForMember(p => p.statusname, opt => opt.MapFrom(x => (Convert.ToInt32(x.status)).GetEnumDescriptionByValue(typeof(ExceptionLogStatus)))); 


        }
    }
}
