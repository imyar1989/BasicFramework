﻿using Common.Model;
using DevOps.Model;
using Redis.Library;
using System;
using System.Collections.Generic;
/*
* 命名空间: DevOps.Logic
*
* 功 能： 异常日志Redis操作类
*
* 类 名： ExceptionLogServiceRedis
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/10 11:13:35 	Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 异常日志Redis操作类
    /// </summary>
    public class ExceptionLogServiceRedis
    {

        /// <summary>
        /// 获取所有异常日志信息
        /// </summary>
        /// <param name="successAction">成功执行</param>
        /// <param name="failAction">失败执行</param>
        /// <returns></returns>
        public static void GetAllExceptionLog(Action<List<SysExceptionLogInfoEntity>> successAction, Action failAction = null)
        {
            var resultInfo = RedisClient.GetRedisDb(RedisLogInfo.RedisLogDbLog).HashGetAll<SysExceptionLogInfoEntity>(RedisLogInfo.RedisExceptionHashId);
            if (resultInfo!=null&& resultInfo.Count>0)
            {
                successAction?.Invoke(resultInfo);
            }
            else
            {
                failAction?.Invoke();
            }
        }

        /// <summary>
        /// 根据Key获取异常日志信息
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="successAction">成功执行</param>
        /// <param name="failAction">失败执行</param>
        /// <returns></returns>
        public static void GetExceptionLog(string key,Action<SysExceptionLogInfoEntity> successAction, Action failAction = null)
        {
            var resultInfo = RedisClient.GetRedisDb(RedisLogInfo.RedisLogDbLog).HashGet<SysExceptionLogInfoEntity>(RedisLogInfo.RedisExceptionHashId, key);
            if (resultInfo != null)
            {
                successAction?.Invoke(resultInfo);
            }
            else
            {
                failAction?.Invoke();
            }
        }

        /// <summary>
        /// 修改异常日志
        /// </summary>
        /// <param name="key"></param>
        /// <param name="exceptionLogInfo"></param>
        public static bool ModifyExceptionLog(string key ,SysExceptionLogInfoEntity exceptionLogInfo)
        {
            var resultInfo = RedisClient.GetRedisDb(RedisLogInfo.RedisLogDbLog).HashUpdate<SysExceptionLogInfoEntity>(RedisLogInfo.RedisExceptionHashId,key,
                (query) => {
                    query = exceptionLogInfo;
                    return query;
            });
            return resultInfo;
        }

        /// <summary>
        /// 批量删除异常日志
        /// </summary>
        /// <param name="keys"></param>
        public static void RemoveExceptionLog(List<string>  keys)
        {
             RedisClient.GetRedisDb(RedisLogInfo.RedisLogDbLog).HashDeletes(RedisLogInfo.RedisExceptionHashId, keys);
        }
    }
}
