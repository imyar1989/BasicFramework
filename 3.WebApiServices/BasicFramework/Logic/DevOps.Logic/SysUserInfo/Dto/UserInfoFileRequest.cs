﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace DevOps.Logic
{
    /// <summary>
    /// 用户文件请求信息
    /// </summary>
    public class UserInfoFileRequest
    {
        /// <summary>
        /// 用户名【登录名】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "用户名【登录名】")]
        public string name
        {
            get; set;
        }


        /// <summary>
        /// 用户手机号码
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "用户手机号码")]
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// 性别
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "性别")]
        public string gender
        {
            get; set;
        }



        /// <summary>
        /// 用户邮箱
        /// </summary>
        public string email
        {
            get; set;
        }
        /// <summary>
        /// 角色
        /// </summary>
        public string role
        {
            get; set;
        }
        /// <summary>
        /// 角色id
        /// </summary>
        public string roleid
        {
            get; set;
        }

        /// <summary>
        /// 部门
        /// </summary>
        public string depart
        {
            get; set;
        }
        /// <summary>
        /// 部门id
        /// </summary>
        public string departid
        {
            get; set;
        }

        /// <summary>
        /// 岗位
        /// </summary>
        public string post
        {
            get; set;
        }
        /// <summary>
        /// 岗位id
        /// </summary>
        public string postid
        {
            get; set;
        }
        /// <summary>
        /// 是否有效
        /// </summary>
        public bool is_valid
        {
            get; set;
        }
        /// <summary>
        /// 是否有效
        /// </summary>
        public string ifis_valid
        {
            get; set;
        }
    }
}
