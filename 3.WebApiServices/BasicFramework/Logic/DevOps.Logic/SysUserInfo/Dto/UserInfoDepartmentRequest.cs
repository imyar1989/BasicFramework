﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 配置部门
    /// </summary>
    public class UserInfoDepartmentRequest
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public string userId { get; set; }
        /// <summary>
        /// 部门ID
        /// </summary>
        public string departmentId { get; set; }
    }
}
