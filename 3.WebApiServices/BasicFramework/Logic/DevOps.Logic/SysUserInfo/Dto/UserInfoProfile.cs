﻿using AutoMapper;
using Common.Model;
using DevOps.Model;

namespace DevOps.Logic
{
    /// <summary>
    /// 权限相关映射
    /// </summary>
    public class UserInfoProfile : Profile
    {
        /// <summary>
        /// 权限数据实体与传输实体映射
        /// </summary>
        public UserInfoProfile()
        {
            CreateMap<UserInfoAddRequest,SysUserInfoEntity>();
            CreateMap<UserInfoRequest, SysUserInfoEntity>();

            CreateMap<SysUserInfoEntity, UserInfoResponse>();

            CreateMap<UserInfoModifyRequest, SysUserInfoEntity>();

            CreateMap<View_SysUserEntity, UserLoginInfo>()
                  .ForMember(p => p.token, opt => opt.Ignore());

            CreateMap<View_SysUserEntity, UserInfoResponse>();

            //用户数据返回类[id改成了数字，key是以前的ID]
            CreateMap<View_SysUserEntity, UserTreeInfoResponse>()
               .ForMember(p => p.id, opt => opt.MapFrom(x => 0))
               .ForMember(p => p.key, opt => opt.MapFrom(x => x.id));

            CreateMap<UserInfoModifyOneselfRequest, SysUserInfoEntity>();
            
        }
    }
}
