﻿using Common.Library;
using Common.Model;
using Redis.Library;
using System;

namespace DevOps.Logic
{
    /// <summary>
    /// 管理员Redis操作逻辑类
    /// </summary>
    internal static class SysUserServiceRedis
    {
        #region 验证码

        /// <summary>
        /// 保存登录验证码
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">值</param>
        /// <param name="successAction">成功执行</param>
        /// <param name="failAction">失败执行</param>
        /// <returns></returns>
        public static void SaveValidateCode(string key, string value, Action successAction, Action failAction = null)
        {
            bool resultInfo = RedisClient.GetRedisDb(RedisAuthorityInfo.RedisDbWeb)
                .StringSet(RedisAuthorityInfo.RedisValidateCode + key, value, RedisAuthorityInfo.TimeOut);
            if (resultInfo)
            {
                successAction?.Invoke();
            }
            else
            {
                failAction?.Invoke();
            }
        }

        /// <summary>
        /// 获取登录验证码
        /// </summary>
        /// <param name="key"></param>
        /// <param name="successAction"></param>
        /// <param name="failAction"></param>
        public static void GetValidateCode(string key, Action<string> successAction, Action failAction = null)
        {
            string ValidateCode = RedisClient.GetRedisDb(RedisAuthorityInfo.RedisDbWeb)
                .StringGet(RedisAuthorityInfo.RedisValidateCode + key);

            //验证码验证
            if (ValidateCode.IsNotNullOrEmpty())
            {
                successAction?.Invoke(ValidateCode);
            }
            else
            {
                failAction?.Invoke();
            }
        }

        #endregion

        #region 用户登录信息

        /// <summary>
        /// 保存用户登录信息在Redis中【用户登录】
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="successAction"></param>
        /// <param name="failAction"></param>
        public static void SaveUserInfo(string key, UserLoginInfo value, Action successAction = null, Action failAction = null)
        {
            bool resultInfo = RedisClient.GetRedisDb(RedisAuthorityInfo.RedisDbWeb)
                .StringSet(RedisAuthorityInfo.RedisSessionCode + key, value, RedisAuthorityInfo.SessionTimeOut);

            bool userResultInfo = RedisClient.GetRedisDb(RedisAuthorityInfo.RedisDbWeb)
                     .StringSet(RedisAuthorityInfo.RedisSessionCode + value.id, value);

            if (resultInfo && userResultInfo)
            {
                successAction?.Invoke();
            }
            else
            {
                failAction?.Invoke();
            }
        }


        /// <summary>
        /// 保存用户登录信息在Redis中[用于修改]
        /// </summary>
        /// <param name="value"></param>
        /// <param name="successAction"></param>
        /// <param name="failAction"></param>
        public static void SaveUserInfo(UserLoginInfo value, Action successAction = null, Action failAction = null)
        {
            bool userResultInfo = RedisClient.GetRedisDb(RedisAuthorityInfo.RedisDbWeb)
                     .StringSet(RedisAuthorityInfo.RedisSessionCode + value.id, value);

            if (userResultInfo)
            {
                successAction?.Invoke();
            }
            else
            {
                failAction?.Invoke();
            }
        }


        /// <summary>
        /// 获取用户登录信息在Redis中
        /// </summary>
        /// <param name="key"></param>
        /// <param name="successAction"></param>
        /// <param name="failAction"></param>
        public static void GetUserInfo(string key, Action<string> successAction, Action failAction = null)
        {
            string resultInfo = RedisClient.GetRedisDb(RedisAuthorityInfo.RedisDbWeb).StringGet(RedisAuthorityInfo.RedisSessionCode + key);

            if (resultInfo.IsNotNullOrEmpty())
            {
                successAction?.Invoke(resultInfo);
            }
            else
            {
                failAction?.Invoke();
            }
        }
        #endregion
    }
}
