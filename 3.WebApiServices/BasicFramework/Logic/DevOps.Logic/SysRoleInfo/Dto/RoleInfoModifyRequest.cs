﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace DevOps.Logic
{
    /// <summary>
    /// 修改角色数据类
    /// </summary>
    public class RoleInfoModifyRequest
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 32, Description = "角色id")]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 角色名称
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "角色名称")]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        [Validate(ValidateType.NotEmpty, Regex = ValidateRegex.Number, Description = "排序字段")]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 是否启用
        /// </summary>
        [Validate(ValidateType.NotEmpty, Description = "是否启用")]
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 角色描述
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 100, Description = "角色描述")]
        public string describe
        {
            get; set;
        }

    }
}
