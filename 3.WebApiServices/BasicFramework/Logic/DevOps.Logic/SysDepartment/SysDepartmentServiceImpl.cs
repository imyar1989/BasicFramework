﻿using Common.Library;
using System.Collections.Generic;
using Common.Model;
using Container.Library;
using System.Linq;
using Dapper.Library;
using Serialize.Library;
using System;
using DevOps.Model;

/*
* 命名空间: DevOps.Logic
*
* 功 能： 部门管理逻辑
*
* 类 名： SysDepartmentServiceImpl
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 部门管理逻辑
    /// </summary>
    public class SysDepartmentServiceImpl : OperationLogicImpl, ISysDepartmentService
    {

        #region 部门基础信息管理模块

        #region 信息查询
        /// <summary>
        /// 获取所有部门下拉列表
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<DepartmentResponse>> LoadDepartmentAllList()
        {
            var resultInfo = new ResultJsonInfo<List<DepartmentResponse>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<SysDepartmentInfoEntity>().Where(a => a.is_deleted == false&& a.is_valid==true).ToList();

                resultInfo.Code = ActionCodes.Success;
                resultInfo.Data = result.MapToList<DepartmentResponse>();

            }
            return resultInfo;
        }
        /// <summary>
        /// 根据关键字【部门名称，领导人名称，联系电话】获取所有的未删除部门信息
        /// </summary>
        /// <param name="inputInfo">关键字【部门名称，领导人名称，联系电话】</param>
        /// <returns></returns>
        public ResultJsonInfo<List<DepartmentResponse>> LoadPageList(ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<DepartmentResponse>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<SysDepartmentInfoEntity>();

                if (inputInfo.parameters.IsNotNullOrEmpty())
                {
                    result.Where(p => p.name.Contains(inputInfo.parameters) || p.leader.Contains(inputInfo.parameters) || p.phone.Contains(inputInfo.parameters));
                }
                var queryInfo = result.Where(p => p.is_deleted == false).OrderBy(p => p.sort).PageList(inputInfo.page, inputInfo.limit);

                if (queryInfo.Items.Count > 0)
                {
                    var menusList = queryInfo.Items.MapToList<DepartmentResponse>();

                    //这里获取所有，查询是否有兄弟节点
                    var functionListInfoAll = result.Where(p => p.is_deleted == false)
                         .OrderBy(p => p.sort).ToList();
                    foreach (var item in menusList)
                    {
                        //NET算术运算溢出问题
                        item.have_elder = functionListInfoAll.Exists(p => p.sort < item.sort && p.id != item.id && p.parent_id == item.parent_id);
                        item.have_younger = functionListInfoAll.Exists(p => p.sort > item.sort && p.id != item.id && p.parent_id == item.parent_id);
                    }
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = menusList;
                    resultInfo.Count = queryInfo.Total;
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Msg = "无对应值存在！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 根据关键字【部门名称，领导人名称，联系电话】获取所有的未删除部门树状信息
        /// </summary>
        /// <param name="keyWords"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<DepartmentTreeInfo>> LoadTreeList(string keyWords) {

            var resultInfo = new ResultJsonInfo<List<DepartmentTreeInfo>>();

            List<DepartmentResponse> listInfo = new List<DepartmentResponse>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<SysDepartmentInfoEntity>();
                //所有部门
                var departmentLists = new List<SysDepartmentInfoEntity>();
                //这里获取所有，查询是否有兄弟节点
                var listInfoAll = result.Where(p => p.is_deleted == false)
                     .OrderBy(p => p.sort).ToList();

                if (keyWords.IsNotNullOrEmpty())
                {
                    result.Where(p => p.name.Contains(keyWords) || p.leader.Contains(keyWords) || p.phone.Contains(keyWords));
                   
                }
                //直接查询出的信息
                var queryInfo = result.Where(p => p.is_deleted == false).OrderBy(p => p.sort).ToList();

                var queryInfoRes = queryInfo.MapToList<DepartmentResponse>();
                foreach (var item in queryInfoRes)
                {
                    //NET算术运算溢出问题
                    item.have_elder = listInfoAll.Exists(p => p.sort < item.sort && p.id != item.id && p.parent_id == item.parent_id);
                    item.have_younger = listInfoAll.Exists(p => p.sort > item.sort && p.id != item.id && p.parent_id == item.parent_id);
                }

                //所有部门
                var listInfoAllRes = listInfoAll.MapToList<DepartmentResponse>();


                if (keyWords.IsNotNullOrEmpty())
                {
                    if (queryInfoRes.Count > 0)
                    {
                        for (int i = 0; i < queryInfoRes.Count; i++)
                        {
                            DrillAnyCategory(listInfoAllRes, queryInfoRes[i].id, ref listInfo);
                            DownCategory(listInfoAllRes, queryInfoRes[0].id, ref listInfo);
                        }
                        queryInfoRes = listInfo;
                    }
                }
                
                if (queryInfoRes.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = GetDepartmentChildTreeInfo(queryInfoRes, "");
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Msg = "无对应值存在！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 向上查询父级数据
        /// </summary>
        /// <param name="list"></param>
        /// <param name="id"></param>
        /// <param name="newAssetsCategory"></param>
        private void DrillAnyCategory(List<DepartmentResponse> list, string id, ref List<DepartmentResponse> newAssetsCategory)
        {

            var info = list.Where(a => a.id == id).ToList();
            if (info.Count > 0)
            {
                newAssetsCategory.Add(info[0]);
                if (info[0].parent_id != "" && info[0].parent_id != null)
                {
                    DrillAnyCategory(list, info[0].parent_id, ref newAssetsCategory);
                }
            }
        }

        /// <summary>
        /// 向下查询子级数据
        /// </summary>
        /// <param name="list"></param>
        /// <param name="id"></param>
        /// <param name="newAssetsCategory"></param>
        private void DownCategory(List<DepartmentResponse> list, string id, ref List<DepartmentResponse> newAssetsCategory)
        {

            var info = list.Where(a => a.id == id).ToList();
            if (info.Count > 0)
            {
                var xia = list.Where(a => a.parent_id == info[0].id).ToList();
                for (int i = 0; i < xia.Count; i++)
                {
                    newAssetsCategory.Add(xia[i]);
                    DownCategory(list, xia[i].id, ref newAssetsCategory);
                }
            }
        }

        /// <summary>
        /// 根据部门id获取用户情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<DepartmentUserResponse>> LoadUserListByDeparId(DepartmentUserQuery queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<DepartmentUserResponse>>();

            var userList = new List<DepartmentUserResponse>();

            List<SysUserInfoEntity> listInfo = null;
            List<SysUserDepartmentReEntity> sysDeparMenu = null;

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {

                con.Transaction(tran =>
                {

                    listInfo = tran.QuerySet<SysUserInfoEntity>()
                              .Where(p => p.is_deleted == false && p.is_valid == true)
                              .OrderBy(p => p.sort)
                              .ToList();
                    
                    sysDeparMenu = tran.QuerySet<SysUserDepartmentReEntity>()
                        .Join<SysUserDepartmentReEntity, SysUserInfoEntity>((a, b) => a.user_id == b.id)
                        .From<SysUserDepartmentReEntity, SysUserInfoEntity>()
                        .OrderBy<SysUserInfoEntity>(p => p.sort)
                        .Where((a, b) => b.is_valid == true && a.department_id == queryInfo.departmentid)
                        .ToList((a, b) => new SysUserDepartmentReEntity
                        {
                            id = a.id,
                            department_id = a.department_id,
                            user_id = a.user_id
                        });

                    userList = listInfo.MapToList<DepartmentUserResponse>();

                    foreach (var item in userList)
                    {
                        if (sysDeparMenu.Exists(p => p.user_id == item.id))
                        {
                            item.lay_is_checked = true;
                        }
                    }
                });
            }

            if (listInfo.Count > 0)
            {
                resultInfo.Code = ActionCodes.Success;
                resultInfo.Data = userList;
                resultInfo.Count = userList.Count;
                resultInfo.Msg = "获取成功！";
            }
            else
            {
                resultInfo.Msg = "无对应信息！";
            }
            return resultInfo;
        }

        /// <summary>
        /// 根据部门id获取领导情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<DepartmentUserResponse>> LoadLeaderListByDeparId(DepartmentUserQuery queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<DepartmentUserResponse>>();

            var userList = new List<DepartmentUserResponse>();

            List<SysUserInfoEntity> listInfo = null;
            List<SysLeaderDepartmentReEntity> sysDepar = null;

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {

                con.Transaction(tran =>
                {

                    var result = tran.QuerySet<SysUserInfoEntity>()
                              .Where(p => p.is_deleted == false && p.is_valid == true)
                             ;

                    if (queryInfo.keyword.IsNotNullOrEmpty())
                    {
                        result.Where(a => a.realname.Contains(queryInfo.keyword) || a.name.Contains(queryInfo.keyword));
                    }
                    listInfo = result.OrderBy(a => a.sort).ToList();
                    sysDepar = tran.QuerySet<SysLeaderDepartmentReEntity>()
                        .Join<SysLeaderDepartmentReEntity, SysUserInfoEntity>((a, b) => a.user_id == b.id)
                        .From<SysLeaderDepartmentReEntity, SysUserInfoEntity>()
                        .OrderBy<SysUserInfoEntity>(p => p.sort)
                        .Where((a, b) => b.is_valid == true && a.department_id == queryInfo.departmentid)
                        .ToList((a, b) => new SysLeaderDepartmentReEntity
                        {
                            id = a.id,
                            department_id = a.department_id,
                            user_id = a.user_id
                        });

                    userList = listInfo.MapToList<DepartmentUserResponse>();

                    foreach (var item in userList)
                    {
                        if (sysDepar.Exists(p => p.user_id == item.id))
                        {
                            item.lay_is_checked = true;
                        }
                    }
                });
            }

            if (listInfo.Count > 0)
            {
                resultInfo.Code = ActionCodes.Success;
                resultInfo.Data = userList;
                resultInfo.Count = userList.Count;
                resultInfo.Msg = "获取成功！";
            }
            else
            {
                resultInfo.Msg = "无对应信息！";
            }
            return resultInfo;
        }
        
        #endregion

        #region 更新操作

        /// <summary>
        /// 新增节点信息
        /// </summary>
        /// <param name="addInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Addnode(DepartmentChildAddRequest addInfo)
        {

            var resultInfo = new ResultJsonInfo<int>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                if (addInfo.parent_id.IsNotNullOrEmpty())
                {
                    con.Transaction(tran =>
                    {
                        var result = 0;
                        var user = GetLoginUserInfo();

                        var sysDepartmentResult = tran.QuerySet<SysDepartmentInfoEntity>().ToList();

                        var sysDepartment = sysDepartmentResult.Find(p => p.id == addInfo.parent_id);
                        if (sysDepartment != null)
                        {
                            //查询出排序在该节点之后的所有节点
                            var sysDepartmentInfos = sysDepartmentResult.FindAll(p => p.sort > sysDepartment.sort);
                            foreach (var item in sysDepartmentInfos)
                            {
                                item.sort = item.sort + 1;
                                tran.CommandSet<SysDepartmentInfoEntity>().Update(item);
                            }

                            //插入数据
                            var departmentInfoEntity = addInfo.MapTo<SysDepartmentInfoEntity>();

                            departmentInfoEntity.modifier_date = DateTime.Now;
                            departmentInfoEntity.modifier_name = user.realname;
                            departmentInfoEntity.modifier_id = user.id;
                            departmentInfoEntity.create_date = DateTime.Now;
                            departmentInfoEntity.creator_name = user.realname;
                            departmentInfoEntity.creator_id = user.id;
                            departmentInfoEntity.sort = sysDepartment.sort + 1;

                            result = tran.CommandSet<SysDepartmentInfoEntity>().Insert(departmentInfoEntity);

                            if (result > 0)
                            {
                                resultInfo.Code = ActionCodes.Success;
                                resultInfo.Data = result;
                                resultInfo.Msg = "新增子节点信息成功！";
                                AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.DepartmentManage, $"新增子节点信息:{JsonHelper.ToJson(sysDepartment)}");
                            }
                            else
                            {
                                resultInfo.Msg = "操作失败！";
                            }
                        }
                        else
                        {
                            resultInfo.Msg = "无对应父节点信息！";
                        }
                    });
                }
                else
                {
                    var result = 0;
                    var sysDepartment = addInfo.MapTo<SysDepartmentInfoEntity>();
                    var user = GetLoginUserInfo();

                    //获取所有部门信息
                    var maxSort = con.QuerySet<SysDepartmentInfoEntity>().Max(p => p.sort);
                    sysDepartment.id = GuidHelper.GetGuid();
                    sysDepartment.sort = maxSort + 1;
                    sysDepartment.creator_id = user.id;
                    sysDepartment.creator_name = user.realname;
                    sysDepartment.modifier_id = user.id;
                    sysDepartment.modifier_name = user.realname;

                    result = con.CommandSet<SysDepartmentInfoEntity>().Insert(sysDepartment);
                    if (result > 0)
                    {
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Data = result;
                        resultInfo.Msg = "新增节点信息成功！";
                        AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.DepartmentManage, $"新增节点信息:{JsonHelper.ToJson(sysDepartment)}");
                    }
                    else
                    {
                        resultInfo.Msg = "操作失败！";
                    }
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 修改节点信息
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Modify(DepartmentModifyRequest modifyInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = 0;

                var sysDepartmentInfo = con.QuerySet<SysDepartmentInfoEntity>().Where(p => p.id == modifyInfo.id).Get();

                if (sysDepartmentInfo != null)
                {
                    //获取登录用户信息
                    var user = GetLoginUserInfo();

                    sysDepartmentInfo.name = modifyInfo.name;
                    sysDepartmentInfo.leader = modifyInfo.leader;
                    sysDepartmentInfo.phone = modifyInfo.phone;
                    sysDepartmentInfo.email = modifyInfo.email;
                    sysDepartmentInfo.remarks = modifyInfo.remarks;
                    sysDepartmentInfo.is_valid = modifyInfo.is_valid;
                    sysDepartmentInfo.modifier_id = user.id;
                    sysDepartmentInfo.modifier_name = user.realname;

                    result = con.CommandSet<SysDepartmentInfoEntity>().Update(sysDepartmentInfo);

                    if (result > 0)
                    {
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Data = result;
                        resultInfo.Msg = "修改信息成功！";

                        AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.DepartmentManage, $"新增根节点信息:{JsonHelper.ToJson(sysDepartmentInfo)}");
                    }
                    else
                    {
                        resultInfo.Msg = "操作失败！";
                    }
                }
                else
                {
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 禁用/启用部门
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ForbidOrEnable(string id)
        {
            var resultInfo = new ResultJsonInfo<int>();

            if (!string.IsNullOrEmpty(id))
            {
                using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
                {
                    var result = 0;
                    var sysDepartmentInfolist = con.QuerySet<SysDepartmentInfoEntity>().Where(p => p.is_deleted == false).ToList();

                    var list = GetSons(sysDepartmentInfolist, id);
                    var sysDepartmentInfo = con.QuerySet<SysDepartmentInfoEntity>().Where(p => p.id == id).Get(); ;


                    if (list.Count > 0 && sysDepartmentInfo != null)
                    {   //获取登录用户信息
                        var user = GetLoginUserInfo();
                        if (sysDepartmentInfo.is_valid)
                        {
                            foreach (SysDepartmentInfoEntity item in list)
                            {
                                item.is_valid = item.is_valid == true ? false : false;
                                item.modifier_id = user.id;
                                item.modifier_name = user.realname;

                                result += con.CommandSet<SysDepartmentInfoEntity>().Update(item);
                            }
                        }
                        else
                        {
                            sysDepartmentInfo.is_valid = !sysDepartmentInfo.is_valid;
                            sysDepartmentInfo.modifier_id = user.id;
                            sysDepartmentInfo.modifier_name = user.realname;

                            result += con.CommandSet<SysDepartmentInfoEntity>().Update(sysDepartmentInfo);

                        }
                        if (result > 0)
                        {
                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Data = result;
                            resultInfo.Msg = "禁用/启用部门成功！";

                            AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.DepartmentManage, $"禁用/启用部门:{JsonHelper.ToJson(sysDepartmentInfo)}");
                        }
                        else
                        {
                            resultInfo.Msg = "操作失败！";
                        }
                    }
                    else
                    {
                        resultInfo.Msg = "无对应信息！";
                    }

                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 修改对应部门下对应的用户
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ModifyDeparUserInfo(DepartmentUserModifyRequest department)
        {
            var resultInfo = new ResultJsonInfo<int>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                con.Transaction(tran =>
                {
                    var userCodes = department.idList;

                    #region 获取当前选中节点和选中节点的子节点
                    var allInfo = tran.QuerySet<SysUserInfoEntity>().Where(p => p.is_deleted == false && p.is_valid == true)
                                                                .OrderBy(p => p.sort)
                                                                .ToList();
                    var deoarInfo = allInfo.FindAll(p => userCodes.Contains(p.id));

                    var listInfo = new List<SysUserInfoEntity>();
                    if (deoarInfo.Count > 0)
                    {
                        listInfo.AddRange(deoarInfo);
                        
                    }
                    userCodes = listInfo.Select(t => t.id).Distinct().ToList();

                    #endregion

                    //获取该部门原有的用户
                    var deparUserListInfo = tran.QuerySet<SysUserDepartmentReEntity>()
                                    .Where(p => p.department_id.Equals(department.department_id))
                                    .ToList();

                    //1、获取需要删除的，删除掉。
                    var needRemoveDeparUserListInfo = deparUserListInfo.FindAll(p => !userCodes.Contains(p.user_id))
                                    .Select(p => p.user_id)
                                    .ToArray();

                    //删除掉不用的用户
                    tran.CommandSet<SysUserDepartmentReEntity>()
                                    .Where(p => p.user_id.In(needRemoveDeparUserListInfo) && p.department_id.Equals(department.department_id))
                                    .Delete();

                    //2、获取需要增加的，增加上。
                    var needAddDeparUserListInfo = userCodes.ToList()
                                    .FindAll(p => !deparUserListInfo.Select(roleMenu => roleMenu.user_id).Contains(p));

                    SysUserDepartmentReEntity deparUserItem = null;
                    foreach (var item in needAddDeparUserListInfo)
                    {
                        deparUserItem = new SysUserDepartmentReEntity();
                        deparUserItem.id = GuidHelper.GetGuid();
                        deparUserItem.department_id = department.department_id;
                        deparUserItem.user_id = item;
                        tran.CommandSet<SysUserDepartmentReEntity>().Insert(deparUserItem);
                    }
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = 1;
                    resultInfo.Msg = "操作成功！";
                });
            }
            return resultInfo;
        }

        /// <summary>
        /// 修改对应部门下对应的领导
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ModifyDeparLeaderInfo(DepartmentUserModifyRequest department)
        {
            var resultInfo = new ResultJsonInfo<int>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                con.Transaction(tran =>
                {
                    var userCodes = department.idList;

                    #region 获取当前选中节点和选中节点的子节点
                    var allInfo = tran.QuerySet<SysUserInfoEntity>().Where(p => p.is_deleted == false && p.is_valid == true)
                                                                .OrderBy(p => p.sort)
                                                                .ToList();
                    var deoarInfo = allInfo.FindAll(p => userCodes.Contains(p.id));

                    var listInfo = new List<SysUserInfoEntity>();
                    if (deoarInfo.Count > 0)
                    {
                        listInfo.AddRange(deoarInfo);

                    }
                    userCodes = listInfo.Select(t => t.id).Distinct().ToList();

                    #endregion

                    //获取该部门原有的领导
                    var deparUserListInfo = tran.QuerySet<SysLeaderDepartmentReEntity>()
                                    .Where(p => p.department_id.Equals(department.department_id))
                                    .ToList();

                    //1、获取需要删除的，删除掉。
                    var needRemoveDeparUserListInfo = deparUserListInfo.FindAll(p => !userCodes.Contains(p.user_id))
                                    .Select(p => p.user_id)
                                    .ToArray();

                    //删除掉不用的用户
                    tran.CommandSet<SysLeaderDepartmentReEntity>()
                                    .Where(p => p.user_id.In(needRemoveDeparUserListInfo) && p.department_id.Equals(department.department_id))
                                    .Delete();

                    //2、获取需要增加的，增加上。
                    var needAddDeparUserListInfo = userCodes.ToList()
                                    .FindAll(p => !deparUserListInfo.Select(roleMenu => roleMenu.user_id).Contains(p));

                    SysLeaderDepartmentReEntity deparUserItem = null;
                    foreach (var item in needAddDeparUserListInfo)
                    {
                        deparUserItem = new SysLeaderDepartmentReEntity();
                        deparUserItem.id = GuidHelper.GetGuid();
                        deparUserItem.department_id = department.department_id;
                        deparUserItem.user_id = item;
                        tran.CommandSet<SysLeaderDepartmentReEntity>().Insert(deparUserItem);
                    }
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = 1;
                    resultInfo.Msg = "操作成功！";
                });
            }
            return resultInfo;
        }

        #region 获取子节点信息

        private List<SysDepartmentInfoEntity> GetSons(List<SysDepartmentInfoEntity> list, string parent_id)
        {
            var query = list.Where(p => p.id == parent_id).ToList();
            var list2 = query.Concat(GetSonList(list, parent_id)).ToList();
            return list2;
        }

        private IEnumerable<SysDepartmentInfoEntity> GetSonList(List<SysDepartmentInfoEntity> list, string parent_id)
        {
            var query = list.Where(p => p.parent_id == parent_id).ToList();
            return query.ToList().Concat(query.ToList().SelectMany(t => GetSonList(list, t.id)));
        }
        #endregion

        #endregion

        #region 移动顺序操作
        /// <summary>
        /// 移动顺序操作
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Move(DepartmentMoveRequest inputInfo)
        {
            var result = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                con.Transaction(tran =>
                {
                    //（1）所有部门
                    var adminDepartments = tran.QuerySet<SysDepartmentInfoEntity>().OrderBy(p => p.sort).ToList();
                    //（2）当前部门
                    SysDepartmentInfoEntity currAdminDepartment = adminDepartments.Find(p => p.id == inputInfo.id);
                    //（3）当前功能兄弟节点
                    List<SysDepartmentInfoEntity> brothersAdminDepartment = adminDepartments.FindAll(p => p.parent_id == currAdminDepartment.parent_id);

                    SysDepartmentInfoEntity broAdminDepartment = null;
                    foreach (SysDepartmentInfoEntity item in brothersAdminDepartment)
                    {
                        if (inputInfo.Flag == 1)//上移
                        {
                            if (item.sort < currAdminDepartment.sort)
                            {
                                broAdminDepartment = item;
                            }
                            if (item.sort > currAdminDepartment.sort)
                            {
                                break;
                            } //找到上一个兄弟节点
                        }
                        else //下移
                        {
                            if (item.sort > currAdminDepartment.sort) { broAdminDepartment = item; break; } //找到下一个兄弟节点
                        }
                    }

                    List<SysDepartmentInfoEntity> currFuns = new List<SysDepartmentInfoEntity>();
                    currFuns.Add(currAdminDepartment);
                    GetMoveNode(adminDepartments, currAdminDepartment.id, currFuns);

                    List<SysDepartmentInfoEntity> moveFuns = new List<SysDepartmentInfoEntity>();
                    moveFuns.Add(broAdminDepartment);
                    GetMoveNode(adminDepartments, broAdminDepartment.id, moveFuns);

                    //上移
                    if (inputInfo.Flag == 1)
                    {
                        foreach (SysDepartmentInfoEntity item in currFuns)
                        {
                            item.sort = short.Parse((item.sort - short.Parse(moveFuns.Count.ToString())).ToString());
                            tran.CommandSet<SysDepartmentInfoEntity>().Update(item);
                        }

                        foreach (SysDepartmentInfoEntity item in moveFuns)
                        {
                            item.sort = short.Parse((item.sort + short.Parse(currFuns.Count.ToString())).ToString());
                            tran.CommandSet<SysDepartmentInfoEntity>().Update(item);
                        }
                    }
                    else
                    {
                        foreach (SysDepartmentInfoEntity item in currFuns)
                        {
                            item.sort = short.Parse((item.sort + short.Parse(moveFuns.Count.ToString())).ToString());
                            tran.CommandSet<SysDepartmentInfoEntity>().Update(item);
                        }
                        foreach (SysDepartmentInfoEntity item in moveFuns)
                        {
                            item.sort = short.Parse((item.sort - short.Parse(currFuns.Count.ToString())).ToString());
                            tran.CommandSet<SysDepartmentInfoEntity>().Update(item);
                        }
                    }
                });
                result.Code = ActionCodes.Success;
                result.Data = 1;

                AddOperationLog(OperationLogType.ModifyOperation,BusinessTitleType.DepartmentManage, $"移动顺序操作:{JsonHelper.ToJson(inputInfo)}");
            }
            return result;
        }

        /// <summary>
        /// 上下一定子功能（递归）
        /// </summary>
        /// <param name="adminDepartments"></param>
        /// <param name="funId"></param>
        /// <param name="move"></param>
        private void GetMoveNode(List<SysDepartmentInfoEntity> adminDepartments, string funId, List<SysDepartmentInfoEntity> move)
        {
            List<SysDepartmentInfoEntity> borgs = adminDepartments.FindAll(r => r.parent_id == funId);
            foreach (SysDepartmentInfoEntity item in borgs)
            {
                if (item.parent_id == funId)
                {
                    move.Add(item);
                    List<SysDepartmentInfoEntity> tempFuns = adminDepartments.FindAll(r => r.parent_id == item.id);
                    if (tempFuns != null && tempFuns.Count > 0)
                    {
                        GetMoveNode(adminDepartments, item.id, move);
                    }
                }
            }
        }
        #endregion

        #region 删除信息
        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="removeInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Remove(List<string> removeInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = 0;
                if (removeInfo.Count >= 0)
                {
                    con.Transaction(tran =>
                    {
                        var listInfo = new List<SysDepartmentInfoEntity>();
                        var allInfo = tran.QuerySet<SysDepartmentInfoEntity>().Where(p => p.is_deleted == false).ToList();

                        var rootInfo = allInfo.FindAll(p => removeInfo.Contains(p.id));
                        if (rootInfo.Count > 0)
                        {
                            listInfo.AddRange(rootInfo);
                            listInfo.AddRange(allInfo.GetChildList(rootInfo));
                        }
                        var listCode = listInfo.Select(p => p.id).ToList();

                        //判断用户-部门关系表中是否存在 用户【部门是否被用户使用】，如果存在，就不能被删除
                        int existInfo = tran.QuerySet<SysUserDepartmentReEntity>().Where(p => p.department_id.In(listCode.ToArray())).Count();
                        if (existInfo == 0)
                        {
                            result = tran.CommandSet<SysDepartmentInfoEntity>().Where(p => p.id.In(listCode.ToArray()))
                                                                           .Update(p => new SysDepartmentInfoEntity
                                                                           {
                                                                               is_deleted = true
                                                                           });
                            tran.CommandSet<SysDepartmentRoleReEntity>().Where(p => p.department_id.In(listCode.ToArray())).Delete();

                            if (result > 0)
                            {
                                resultInfo.Code = ActionCodes.Success;
                                resultInfo.Data = result;
                                resultInfo.Msg = "操作成功！";

                                AddOperationLog(OperationLogType.RemoveOperation, BusinessTitleType.DepartmentManage, $"删除信息:{JsonHelper.ToJson(removeInfo)}");
                            }
                            else
                            {
                                resultInfo.Msg = "操作失败！";
                            }
                        }
                        else
                        {
                            resultInfo.Msg = "选中部门已被已被用户使用，无法删除！";
                        }
                    });
                }
                else
                {
                    resultInfo.Msg = "id为空，操作失败！";
                }
            }
            return resultInfo;
        }
        #endregion

        #endregion

        #region 公用操作

        /// <summary>
        /// 获取所有启用组织机构树状信息
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<TreeInfo>> LoadAllTreeList()
        {
            var resultInfo = new ResultJsonInfo<List<TreeInfo>>();
            List<TreeInfo> listInfo = new List<TreeInfo>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var departments = con.QuerySet<SysDepartmentInfoEntity>()
                            .Where(p => p.is_deleted == false).OrderBy(p => p.sort)
                            .ToList();

                if (departments.Count > 0)
                {
                    TreeInfo departmentInfo = new TreeInfo()
                    {
                        id = "",
                        title = "公司组织信息",
                        field = "公司组织信息",
                        spread = "true",
                        children = departments.GetChildTreeInfo("")
                    };
                    listInfo.Add(departmentInfo);
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo;
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Msg = "获取组织机构树状失败！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 获取所有部门Select树状数据
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<SelectListInfo>> LoadDepartmentTreeList() {

            var resultInfo = new ResultJsonInfo<List<SelectListInfo>>();

            List<SelectListInfo> listInfo = new List<SelectListInfo>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var departments = con.QuerySet<SysDepartmentInfoEntity>().Where(p => p.is_deleted == false).OrderBy(p=>p.sort).ToList();

                if (departments.Count > 0)
                {
                    var department = departments.FindAll(p => p.parent_id == "");
                    foreach (var item in department)
                    {
                        SelectListInfo departmentInfo = new SelectListInfo()
                        {
                            value = item.id,
                            name = item.name,
                            children = departments.GetSelectChildInfo<SysDepartmentInfoEntity>(item.id)
                        };
                        listInfo.Add(departmentInfo);
                    }

                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo;
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Msg = "获取所有部门失败！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 获取指定用户机构树状信息
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<DepartmentUserTreeInfo>> LoadDepartmentUserTreeList(UserDepartmentQuery inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<DepartmentUserTreeInfo>>();
            List<DepartmentUserTreeInfo> listInfo = new List<DepartmentUserTreeInfo>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var departmentList = con.QuerySet<SysDepartmentInfoEntity>()
                            .Where(p => p.is_deleted == false).OrderBy(p => p.sort)
                            .ToList();
                var departmentInfos = departmentList.MapToList<DepartmentTreeResponse>();

                var userInfoList = con.QuerySet<View_SysUserEntity>().OrderBy(a => a.sort).ToList();

                var userInfos = new List<UserTreeInfoResponse>();
                // 获取类型 100所有 110给与的
                if (inputInfo.type == 100)
                {
                    userInfos = userInfoList.MapToList<UserTreeInfoResponse>();
                }
                else
                {
                    var employeeIdArr = inputInfo.employeeIds.ToArray();
                    userInfos = userInfoList.FindAll(p => employeeIdArr.Contains(p.id)).MapToList<UserTreeInfoResponse>();
                }
                int index = 1;
                foreach (var item in departmentInfos)
                {
                    item.id = ++index;
                }
                foreach (var item in userInfos)
                {
                    item.id = ++index;
                }
                if (departmentInfos.Count > 0)
                {
                    List<int> usedIdList = new List<int>();
                    var departmentInfo = new DepartmentUserTreeInfo()
                    {
                        id = 1,
                        label = "公司组织信息",
                        pid = 0,
                        type = 100,
                        key = "",
                        children = GetDepartmentUserTree(departmentInfos, "", userInfos, usedIdList)
                    };
                    listInfo.Add(departmentInfo);
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo;
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Msg = "获取组织机构树状失败！";
                }
            }
            return resultInfo;
        }
        #endregion

    }
}
