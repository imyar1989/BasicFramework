﻿using DevOps.Logic;
using AutoMapper;
using DevOps.Model;

namespace DevOps.Logic
{
    /// <summary>
    /// 菜单功能相关实体映射类
    /// </summary>
    public class FunctionCfgProfile: Profile
    {
        /// <summary>
        /// 数据实体与传输实体映射
        /// </summary>
        public FunctionCfgProfile()
        {

            CreateMap<FunctionCfgAddRequest, SysFunctionCfgEntity>();

            CreateMap<FunctionCfgModifyRequest, SysFunctionCfgEntity>();

            CreateMap<SysFunctionCfgEntity, FunctionCfgInfoResponse>();

            CreateMap<SysFunctionCfgEntity, FunctionCfgRoleMenuResponse>()
                  .ForMember(p => p.lay_is_checked, opt => opt.Ignore());

        }
    }
}
