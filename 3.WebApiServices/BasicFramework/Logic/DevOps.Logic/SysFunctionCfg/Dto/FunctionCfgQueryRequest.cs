﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 功能请求查询条件传输实体
    /// </summary>
    public class FunctionCfgQueryRequest
    {

        /// <summary>
        /// 菜单名称【可空】
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 应用ID
        /// </summary>
        public string application_id
        {
            get; set;
        }
    }
}
