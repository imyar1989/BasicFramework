﻿using Common.Library;
using Common.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 应用管理逻辑类接口
    /// </summary>
    public interface ISafeApplicationInfoService
    {
        #region 应用基础管理

        #region 查询
        /// <summary>
        /// 根据关键字 分页获取应用列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<SafeApplicationInfoResponse>> LoadSysApplicationList(ParametersInfo<string> inputInfo);


        /// <summary>
        /// 获取服务列表下拉框信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadSelectList();

        /// <summary>
        /// 获取应用列表Select树状数据
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<SelectListInfo>> LoadTreeList();


        /// <summary>
        /// 获取应用类型Select列表信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadApplicationTypeList();



        #endregion

        #region 修改
        /// <summary>
        /// 修改一个应用
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> ModifySysApplication(SafeApplicationInfoModifyRequest inputInfo);

        /// <summary>
        /// 删除应用
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> DeleteSysApplication(List<string> applicationId);

        /// <summary>
        /// 禁用/启用应用
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> ForbiddenSysApplication(string applicationId);
        #endregion

        #region 添加
        /// <summary>
        /// 新增一个应用
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> AddSysApplication(SafeApplicationInfoAddRequest addRequest);

        #endregion

        #endregion

    }
}
