﻿using DevOps.Model;
using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  DevOps.Logic
*
* 功 能：流程任务返回信息
*
* 类 名： FlowTaskInfoResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/1/22 17:06:39               Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 流程任务返回信息
    /// </summary>
    public class FlowTaskInfoResponse: FlowTaskInfoEntity
    {

        /// <summary>
        /// 任务类型名称
        /// </summary>
        public string task_type_name
        {
            get; set;
        }

        /// <summary>
        /// 任务状态名称
        /// </summary>
        public string status_name
        {
            get; set;
        }

        /// <summary>
        /// 处理类型名称
        /// </summary>
        public string execute_type_name
        {
            get; set;
        }
    }
}
