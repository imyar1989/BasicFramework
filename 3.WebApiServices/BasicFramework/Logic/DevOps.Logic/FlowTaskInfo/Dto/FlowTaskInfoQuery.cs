﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 流程任务查询请求实体
    /// </summary>
    public class FlowTaskInfoQuery
    {
        /// <summary>
        /// 是否是超级管理员
        /// </summary>
        public bool is_super_user { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public string userid { get; set; }
        /// <summary>
        /// 工作KEY
        /// </summary>
        public string wordkey { get; set; }
    }
}
