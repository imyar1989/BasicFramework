﻿using AutoMapper;
using Common.Library;
using Common.Model;
using DevOps.Model;
using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  DevOps.Logic.FlowTaskInfo.Dto
*
* 功 能：XXX
*
* 类 名： FlowTaskInfoProfile
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/1/22 17:06:24               Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 流程任务实体映射
    /// </summary>
    public class FlowTaskInfoProfile : Profile
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowTaskInfoProfile()
        {

            CreateMap<FlowTaskInfoEntity, FlowTaskInfoResponse>()
                .ForMember(p => p.task_type_name, opt => opt.MapFrom(x => Convert.ToInt32(x.task_type).GetEnumDescriptionByValue(typeof(FlowTaskType))))
                .ForMember(p => p.status_name, opt => opt.MapFrom(x => Convert.ToInt32(x.status).GetEnumDescriptionByValue(typeof(FlowTaskStatus))))
                 .ForMember(p => p.execute_type_name, opt => opt.MapFrom(x => Convert.ToInt32(x.execute_type).GetEnumDescriptionByValue(typeof(FlowTaskExecuteType))));
        }
    }
}
