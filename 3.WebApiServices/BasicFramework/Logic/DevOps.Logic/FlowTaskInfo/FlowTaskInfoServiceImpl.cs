﻿using Common.Library;
using Common.Model;
using Container.Library;
using Dapper.Library;
using DevOps.Model;
using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  DevOps.Logic
*
* 功 能：流程任务逻辑类
*
* 类 名： FlowTaskInfoServiceImpl
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/1/22 17:07:53   Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 流程任务逻辑类
    /// </summary>
    public class FlowTaskInfoServiceImpl : OperationLogicImpl, IFlowTaskInfoService
    {

        /// <summary>
        /// 分页查询已办事项
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<FlowTaskInfoResponse>> LoadHaveDonePageList(ParametersInfo<string> inputInfo)
        {

            var resultInfo = new ResultJsonInfo<List<FlowTaskInfoResponse>>();
            //获取用户登录信息
            var user = GetLoginUserInfo();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<FlowTaskInfoEntity>().Where(a =>a.status.Equals((int)FlowTaskStatus.Completed));

                //如果是超级管理员，显示所有的已办理事项
                if (!user.is_super_user)
                {
                    result.Where(a => a.receive_id.Equals(user.id));
                }

                if (inputInfo.parameters.IsNotNullOrEmpty())
                {
                    result.Where(a => a.flow_name.Contains(inputInfo.parameters) || a.sender_name.Contains(inputInfo.parameters) || a.comments.Contains(inputInfo.parameters));
                }

                #region 排序
                if (inputInfo.field.IsNullOrEmpty())
                {
                    inputInfo.field = "sort";
                }
                if (inputInfo.order.IsNullOrEmpty())
                {
                    inputInfo.order = "desc";
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                #endregion

                var listInfo = result.PageList(inputInfo.page, inputInfo.limit);
                if (listInfo.Items.Count > 0)
                {

                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo.Items.MapToList<FlowTaskInfoResponse>();
                    resultInfo.Count = listInfo.Total;
                }
                else
                {
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 分页查询待办事项
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<FlowTaskInfoResponse>> LoadBacklogPageList(ParametersInfo<string> inputInfo)
        {

            var resultInfo = new ResultJsonInfo<List<FlowTaskInfoResponse>>();
            //获取用户登录信息
            var user = GetLoginUserInfo();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {

                int isCompleted = (int)FlowTaskStatus.Completed;
                int isInvalid = (int)FlowTaskStatus.Invalid;
                
                var result = con.QuerySet<FlowTaskInfoEntity>().Where(a => a.status!= isCompleted && a.status != isInvalid);
                //如果是超级管理员，显示所有的已办理事项
                if (!user.is_super_user)
                {
                    result.Where(a => a.receive_id.Equals(user.id));
                }
                if (inputInfo.parameters.IsNotNullOrEmpty())
                {
                    result.Where(a => a.flow_name.Contains(inputInfo.parameters) || a.sender_name.Contains(inputInfo.parameters) || a.comments.Contains(inputInfo.parameters));
                }

                #region 排序
                if (inputInfo.field.IsNullOrEmpty())
                {
                    inputInfo.field = "sort";
                }
                if (inputInfo.order.IsNullOrEmpty())
                {
                    inputInfo.order = "desc";
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                #endregion

                var listInfo = result.PageList(inputInfo.page, inputInfo.limit);
                if (listInfo.Items.Count > 0)
                {

                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo.Items.MapToList<FlowTaskInfoResponse>();
                    resultInfo.Count = listInfo.Total;
                }
                else
                {
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }
        /// <summary>
        /// 分页查询待办事项(首页)
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<FlowTaskInfoResponse>> LoadBackPageList(ParametersInfo<FlowTaskInfoQuery> inputInfo)
        {

            var resultInfo = new ResultJsonInfo<List<FlowTaskInfoResponse>>();
            //获取用户登录信息
            var user = GetLoginUserInfo();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {

                int isCompleted = (int)FlowTaskStatus.Completed;
                int isInvalid = (int)FlowTaskStatus.Invalid;

                var result = con.QuerySet<FlowTaskInfoEntity>().Where(a => a.status != isCompleted && a.status != isInvalid);
                //如果是超级管理员，显示所有的已办理事项
                if (!inputInfo.parameters.is_super_user)
                {
                    result.Where(a => a.receive_id.Equals(inputInfo.parameters.userid));
                }

                #region 排序
                if (inputInfo.field.IsNullOrEmpty())
                {
                    inputInfo.field = "sort";
                }
                if (inputInfo.order.IsNullOrEmpty())
                {
                    inputInfo.order = "desc";
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                #endregion

                var listInfo = result.PageList(inputInfo.page, inputInfo.limit);
                if (listInfo.Items.Count > 0)
                {

                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo.Items.MapToList<FlowTaskInfoResponse>();
                    resultInfo.Count = listInfo.Total;
                }
                else
                {
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 根据流程任务中的主业务表ID获取流程任务信息
        /// </summary>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<FlowTaskInfoResponse>> LoadFlowTasksByInstance(string instanceId) {

            var resultInfo = new ResultJsonInfo<List<FlowTaskInfoResponse>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                int isInvalid = (int)FlowTaskStatus.Invalid;
                var listInfo = con.QuerySet<FlowTaskInfoEntity>().Where(a => a.instance_id.Equals(instanceId) && a.status != isInvalid)
                    .OrderBy(p => p.receive_time)
                    .OrderBy(p => p.sort)
                    .ToList();

                if (listInfo.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo.MapToList<FlowTaskInfoResponse>();
                }
                else
                {
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }
    }
}
