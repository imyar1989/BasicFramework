﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 流程任务逻辑类接口
    /// </summary>
    public interface IFlowTaskInfoService
    {

        /// <summary>
        /// 分页查询已办事项
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<FlowTaskInfoResponse>> LoadHaveDonePageList(ParametersInfo<string> inputInfo);

        /// <summary>
        /// 分页查询待办事项
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<FlowTaskInfoResponse>> LoadBacklogPageList(ParametersInfo<string> inputInfo);
        /// <summary>
        /// 分页查询待办事项(首页)
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<FlowTaskInfoResponse>> LoadBackPageList(ParametersInfo<FlowTaskInfoQuery> inputInfo);

        /// <summary>
        /// 根据流程任务中的主业务表ID获取流程任务信息
        /// </summary>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        ResultJsonInfo<List<FlowTaskInfoResponse>> LoadFlowTasksByInstance(string instanceId);
    }
}
