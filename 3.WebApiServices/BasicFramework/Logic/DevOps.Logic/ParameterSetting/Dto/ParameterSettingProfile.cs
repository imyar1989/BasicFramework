﻿using AutoMapper;
using DevOps.Model;

namespace DevOps.Logic
{
    /// <summary>
    /// 实体映射
    /// </summary>
    public class ParameterSettingProfile : Profile
    {
        /// <summary>
        /// 数据实体与传输实体映射
        /// </summary>
        public ParameterSettingProfile()
        {
            CreateMap<ParameterSettingOptRequest, SysParameterSettingEntity>();
        }
    }
}
