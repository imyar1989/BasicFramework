﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace DevOps.Logic
{
    /// <summary>
    /// 参数设置 添加或修改传输实体
    /// </summary>
    public class ParameterSettingOptRequest
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 参数名称
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "参数名称")]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 参数键名
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "参数键名")]
        public string parameters_key
        {
            get; set;
        }

        /// <summary>
        /// 参数键值
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 100, Description = "参数键值")]
        public string parameters_value
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        public float sort
        {
            get; set;
        }
    }
}
