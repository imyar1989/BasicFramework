﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间: DevOps.Logic
*
* 功 能： 按时间分组的接口请求记录
*
* 类 名： ApiMonitorLogGroup
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/14 10:43:57 				Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 按时间分组的接口请求记录
    /// </summary>
    public class ApiMonitorLogGroup
    {

        /// <summary>
        /// key
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 时间
        /// </summary>
        public DateTime statistics_time
        {
            get; set;
        }

        /// <summary>
        /// 请求次数
        /// </summary>
        public int frequency
        {
            get; set;
        }
    }
}
