﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 访问记录查询类
    /// </summary>
   public  class ApiMonitorLogQuery
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string sKeyWords
        {
            get; set;
        }
        /// <summary>
        ///开始时间
        /// </summary>
        public DateTime? exception_time
        {
            get; set;
        }
    }
}
