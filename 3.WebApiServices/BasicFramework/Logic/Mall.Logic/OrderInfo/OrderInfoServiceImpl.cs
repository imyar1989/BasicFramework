using Common.Library;
using Common.Model;
using Container.Library;
using Dapper.Library;
using Mall.Model;
using Serialize.Library;
using System;
using System.Collections.Generic;

namespace Mall.Logic
{

    /// <summary>
    /// OrderInfo操作逻辑类
    /// </summary>
    public class OrderInfoServiceImpl : OperationLogicImpl, IOrderInfoService
    {
        #region 查询

        /// <summary>
        ///根据关键字分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<OrderInfoResponse>> LoadPageList(ParametersInfo<OrderInfoQuery> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<OrderInfoResponse>>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                int isDeleted = (int)IsDeleted.Deleted;
                var result = con.QuerySet<OrderInfoEntity>().Where(a => a.is_deleted != isDeleted);
                if (inputInfo.parameters.keywords.IsNotNullOrEmpty())
                {
                    result.Where(p => p.client_name.Contains(inputInfo.parameters.keywords)
                        || p.order_no.Contains(inputInfo.parameters.keywords)
                        || p.trade_no.Contains(inputInfo.parameters.keywords)
                        || p.client_name.Contains(inputInfo.parameters.keywords)
                        || p.receiver.Contains(inputInfo.parameters.keywords)
                        || p.receiver_phone.Contains(inputInfo.parameters.keywords)
                        );
                }
                //支付方式【0表示所有】
                if (inputInfo.parameters.pay_type > 0)
                {
                    result.Where(p => p.pay_type.Equals(inputInfo.parameters.pay_type));
                }
                //订单状态【0表示所有】
                if (inputInfo.parameters.status > 0)
                {
                    result.Where(p => p.status.Equals(inputInfo.parameters.status));
                }
                //开始时间
                if (inputInfo.parameters.startTime != null)
                {
                    DateTime beginTime =Convert.ToDateTime(inputInfo.parameters.startTime) ;
                    result.Where(p => p.book_time >= beginTime);
                }
                //截止时间
                if (inputInfo.parameters.endTime != null)
                {
                    DateTime endtime = Convert.ToDateTime(inputInfo.parameters.endTime);
                    result.Where(p => p.book_time <= endtime);
                }

                #region 排序
                if (inputInfo.field.IsNullOrEmpty())
                {
                    inputInfo.field = " book_time";
                }
                if (inputInfo.order.IsNullOrEmpty())
                {
                    inputInfo.order = " desc";
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                #endregion

                //eg: var queryInfo = result.PageList(inputInfo.page, inputInfo.limit, p => new GoodsInfoEntity { id = p.id }); 
                var queryInfo = result.PageList(inputInfo.page, inputInfo.limit);
                if (queryInfo.Items.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = queryInfo.Items.MapToList<OrderInfoResponse>();
                    resultInfo.Count = queryInfo.Total;
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Msg = "无对应值存在！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 获取支付方式类型
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<EnumToSelectItem>> LoadPayTypeList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();
            resultInfo.Data = EnumToSelectItem.GetEnumDescriptionList(typeof(PaymentType), true);
            resultInfo.Code = ActionCodes.Success;
            resultInfo.Msg = "获取成功！";
            return resultInfo;
        }

        /// <summary>
        /// 获取订单状态类型
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<EnumToSelectItem>> LoadOrderStatusList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();
            resultInfo.Data = EnumToSelectItem.GetEnumDescriptionList(typeof(OrderStatus), true);
            resultInfo.Code = ActionCodes.Success;
            resultInfo.Msg = "获取成功！";
            return resultInfo;
        }

        /// <summary>
        /// 获取物流商类型
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<EnumToSelectItem>> LoadLogisticsProviderList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();
            resultInfo.Data = EnumToSelectItem.GetEnumDescriptionList(typeof(LogisticsProvider), true);
            resultInfo.Code = ActionCodes.Success;
            resultInfo.Msg = "获取成功！";
            return resultInfo;
        }

        #endregion

        #region 更新

        /// <summary>
        /// 更新订单信息[只修改]
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ModifyInfo(OrderInfoModifyRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = 0;
                if (inputInfo.id.IsNotNullOrEmpty())
                {
                    #region Modify
                    var modifyInfo = con.QuerySet<OrderInfoEntity>().Where(p => p.id == inputInfo.id).Get();
                    if (modifyInfo != null)
                    {
                        modifyInfo.pay_price = inputInfo.pay_price; //实际支付 pay_price
                        modifyInfo.receiver = inputInfo.receiver;//收货人 receiver
                        modifyInfo.receiver_phone = inputInfo.receiver_phone; //收货电话 receiver_phone
                        modifyInfo.status = inputInfo.status;//订单状态 status
                        modifyInfo.receive_province = inputInfo.receive_province;//区域 receive_province   receive_city receive_region
                        modifyInfo.receive_city = inputInfo.receive_city;
                        modifyInfo.receive_region = inputInfo.receive_region;
                        modifyInfo.receiver_address = inputInfo.receiver_address;//具体地址 receiver_address
                        modifyInfo.logistics_type = inputInfo.logistics_type; //物流名称 logistics_type
                        modifyInfo.logistics_num = inputInfo.logistics_num; //物流单号 logistics_num
                        modifyInfo.logistics_price = inputInfo.logistics_price;//物流费用 logistics_price
                        modifyInfo.send_time = inputInfo.send_time; //发货时间 send_time
                        modifyInfo.send_remarks = inputInfo.send_remarks; //发货备注 

                        result = con.CommandSet<OrderInfoEntity>().Update(modifyInfo);
                        if (result > 0)
                        {
                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "修改成功！";
                            AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.OrderInfoManage, $"修改订单信息，修改信息：{ JsonHelper.ToJson(inputInfo)}");
                        }
                        else
                        {
                            resultInfo.Code = ActionCodes.InvalidOperation;
                            resultInfo.Msg = "修改失败！";
                        }
                    }
                    else
                    {
                        resultInfo.Code = ActionCodes.InvalidOperation;
                        resultInfo.Msg = "对应Id的信息不存在！";
                    }
                    #endregion
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 订单发货操作
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> DeliverInfo(OrderInfoDeliverRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = 0;
                if (inputInfo.id.IsNotNullOrEmpty())
                {
                    #region Modify
                    var modifyInfo = con.QuerySet<OrderInfoEntity>().Where(p => p.id == inputInfo.id).Get();
                    if (modifyInfo != null)
                    {
                        modifyInfo.status = (int)OrderStatus.Delivered;//订单状态 status
                        modifyInfo.logistics_type = inputInfo.logistics_type; //物流名称 logistics_type
                        modifyInfo.logistics_num = inputInfo.logistics_num; //物流单号 logistics_num
                        modifyInfo.logistics_price = inputInfo.logistics_price;//物流费用 logistics_price
                        modifyInfo.send_time = inputInfo.send_time; //发货时间 send_time
                        modifyInfo.send_remarks = inputInfo.send_remarks; //发货备注 

                        result = con.CommandSet<OrderInfoEntity>().Update(modifyInfo);
                        if (result > 0)
                        {
                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "发货成功！";
                            AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.OrderInfoManage, $"订单发货操作，发货信息：{ JsonHelper.ToJson(inputInfo)}");
                        }
                        else
                        {
                            resultInfo.Code = ActionCodes.InvalidOperation;
                            resultInfo.Msg = "发货失败！";
                        }
                    }
                    else
                    {
                        resultInfo.Code = ActionCodes.InvalidOperation;
                        resultInfo.Msg = "对应Id的信息不存在！";
                    }
                    #endregion
                }
            }
            return resultInfo;
        }
        
        #endregion

        #region 删除

        /// <summary>
        /// 删除订单信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                //删除
                con.Transaction(tran =>{

                    var info = tran.QuerySet<OrderInfoEntity>().Where(p => p.id.In(ids.ToArray())).ToList();
                    info.ForEach(a => a.is_deleted = (int)IsDeleted.Deleted);
                    var result = tran.CommandSet<OrderInfoEntity>().Update(info);
                    //var result = con.CommandSet<OrderInfoEntity>().Where(p => p.id.In(ids.ToArray())).Delete();
                    if (result > 0)
                    {
                        //con.CommandSet<XXXX>().Where(p => p.xxid.In(ids.ToArray())).Delete();
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "删除成功！";
                        AddOperationLog(OperationLogType.RemoveOperation, BusinessTitleType.OrderInfoManage, $"删除订单信息成功，物理删除信息：{ JsonHelper.ToJson(ids)}");
                    }
                    else
                    {
                        resultInfo.Msg = "删除失败！";
                    }
                });
            }
            return resultInfo;
        }
        #endregion
    }
}
