using Common.Library;
using Common.Model;
using System.Collections.Generic;

namespace Mall.Logic
{

    /// <summary>
    /// OrderInfo操作逻辑类接口
    /// </summary>
    public interface IOrderInfoService
    {
       

       #region 查询

       /// <summary>
       ///根据关键字分页获取列表
       /// </summary>
       /// <param name="inputInfo"></param>
       /// <returns></returns>
        ResultJsonInfo<List<OrderInfoResponse>> LoadPageList(ParametersInfo<OrderInfoQuery> inputInfo);

        /// <summary>
        /// 获取支付方式类型
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadPayTypeList();

        /// <summary>
        /// 获取订单状态类型
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadOrderStatusList();

        /// <summary>
        /// 获取物流商类型
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadLogisticsProviderList();

        #endregion

        #region 更新

        /// <summary>
        /// 更新订单信息
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ModifyInfo(OrderInfoModifyRequest inputInfo);

        /// <summary>
        /// 订单发货操作
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> DeliverInfo(OrderInfoDeliverRequest inputInfo);

        #endregion

        #region 删除

        /// <summary>
        /// 删除订单信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> ids);

        #endregion
    }
}
