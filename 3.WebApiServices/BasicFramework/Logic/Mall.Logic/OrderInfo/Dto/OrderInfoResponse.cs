﻿using Mall.Model;
using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  Mall.Logic
*
* 功 能：订单返回信息传输实体
*
* 类 名：OrderInfoResponse  
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1   2021/8/6 11:13:53  LW     创建
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
namespace Mall.Logic
{
    /// <summary>
    /// 订单返回信息传输实体
    /// </summary>
	public class OrderInfoResponse: OrderInfoEntity
    {

        /// <summary>
        /// 订单状态名称  100:待付款 110:已付款/待发货 120:已发货 130:已完成（待评价） 140:已完成（已经评价） 150:已取消 160:售后申请中
        /// </summary>
        public string status_name
        {
            get; set;
        }

        /// <summary>
        /// 支付方式 100'微信', 110'支付宝',120 '银联', 130'余额'
        /// </summary>
        public string pay_type_name
        {
            get; set;
        }

        /// <summary>
        /// 物流配送名称 100 
        /// </summary>
        public string logistics_name
        {
            get; set;
        }
        

    }
}
