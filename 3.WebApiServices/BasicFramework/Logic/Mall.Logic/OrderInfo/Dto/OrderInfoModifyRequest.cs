﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

/*
* 命名空间:  Mall.Logic.OrderInfo.Dto
*
* 功 能：订单编辑传输实体
*
* 类 名：OrderInfoModifyRequest  
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1   2021/8/6 11:15:51  LW     创建
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
namespace Mall.Logic
{
    /// <summary>
    /// 订单编辑传输实体
    /// </summary>
	public class OrderInfoModifyRequest
    {  
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 订单实际支付价格
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, Regex = ValidateRegex.Decimal, MaxLength = 10, Description = "订单实际支付价格")]
        public decimal pay_price
        {
            get; set;
        }

        /// <summary>
        /// 收货人
        /// </summary>
        public string receiver
        {
            get; set;
        }

        /// <summary>
        /// 收货人电话
        /// </summary>
        public string receiver_phone
        {
            get; set;
        }

        /// <summary>
        /// 收货地区 - 省
        /// </summary>
        public string receive_province
        {
            get; set;
        }

        /// <summary>
        /// 收货地区 - 市
        /// </summary>
        public string receive_city
        {
            get; set;
        }

        /// <summary>
        /// 收货地区- 区县
        /// </summary>
        public string receive_region
        {
            get; set;
        }

        /// <summary>
        /// 收货详细地址
        /// </summary>
        public string receiver_address
        {
            get; set;
        }

        /// <summary>
        /// 物流配送名称[类型]
        /// </summary>
        public int logistics_type
        {
            get; set;
        }

        /// <summary>
        /// 物流单号
        /// </summary>
        public string logistics_num
        {
            get; set;
        }

        /// <summary>
        /// 发货时间
        /// </summary>
        public DateTime send_time
        {
            get; set;
        }
        
        /// <summary>
        /// 订单物流占用价格
        /// </summary>
        public decimal logistics_price
        {
            get; set;
        }

        /// <summary>
        /// 发货备注
        /// </summary>
        public string send_remarks
        {
            get; set;
        }

        /// <summary>
        /// 订单状态  100:待付款 110:已付款/待发货 120:已发货 130:已完成（待评价） 140:已完成（已经评价） 150:已取消 160:售后申请中
        /// </summary>
        public int status
        {
            get; set;
        }
    }
}
