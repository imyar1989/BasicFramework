﻿using Mall.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mall.Logic
{
    /// <summary>
    /// 商品保存信息
    /// </summary>
    public class GoodsInfoModifyRequest
    {

        /// <summary>
        /// 构造函数
        /// </summary>
        public GoodsInfoModifyRequest()
        {
            baseInfo = new GoodsInfoEntity();
            fileInfo = new List<GoodsPictureEntity>();
        }

        /// <summary>
        /// 商品基础信息
        /// </summary>
        public GoodsInfoEntity baseInfo
        {
            get; set;
        }

        /// <summary>
        /// 商品附件信息
        /// </summary>
        public List<GoodsPictureEntity> fileInfo
        {
            get; set;
        }
    }
}
