﻿namespace Mall.Logic
{
    /// <summary>
    /// 商品信息查询条件实体
    /// </summary>
    public class GoodsInfoQuery
    {

        /// <summary>
        ///商品分类
        /// </summary>
        public string category_id
        {
            get; set;
        }
        /// <summary>
        /// 关键字
        /// </summary>
        public string keywords
        {
            get; set;
        }
    }
}
