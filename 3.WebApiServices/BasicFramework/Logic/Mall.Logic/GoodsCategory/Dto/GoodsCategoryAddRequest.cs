﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mall.Logic
{
    /// <summary>
    /// 节点操作
    /// </summary>
    public class GoodsCategoryAddRequest
    {
        /// <summary>
        /// 父级节点编码
        /// </summary>
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 类名
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 分类图片
        /// </summary>
        public string img_url
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }
    }
}
