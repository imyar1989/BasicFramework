﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace Mall.Logic
{
    /// <summary>
    /// 移动端登陆操作实体
    /// </summary>
    public class ClientLoginRequest
    {

        /// <summary>
        /// OpenId
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 30, Description = "OpenId")]
        public string open_id
        {
            get; set;
        }

        /// <summary>
        /// 头像信息
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 255, Description = "头像信息")]
        public string head_img
        {
            get; set;
        }

        /// <summary>
        /// 用户昵称
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 30, Description = "用户昵称")]
        public string nickname
        {
            get; set;
        }

        /// <summary>
        /// 性别 0未知 1男 2女
        /// </summary>
        public int sex
        {
            get; set;
        }

        /// <summary>
        /// 用户所在国家
        /// </summary>
        public string country
        {
            get;   set;
        }

        /// <summary>
        ///  用户所在省份
        /// </summary>
        public string province
        {
            get; set;
        }

        /// <summary>
        /// 用户所在城市
        /// </summary>
        public string city
        {
            get;  set;
        }

    }
}
