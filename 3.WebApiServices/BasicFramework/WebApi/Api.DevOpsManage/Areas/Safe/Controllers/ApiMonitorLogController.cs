﻿using System;
using System.Collections.Generic;
using Api.DevOpsManage.App_Start;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using DevOps.Logic;
using Validate.Library;

namespace Api.Manage.Areas.Safe.Controllers
{
    /// <summary>
    /// 接口访问监控日志
    /// </summary>
    [ApiExplorerSettings(GroupName = "Safe")]
    [Route("api/Safe/[controller]")]
    [ApiController]
    public class ApiMonitorLogController : BaseApiController
    {

        private readonly IApiMonitorLogService apiMonitorLogService = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ApiMonitorLogController()
        {
            apiMonitorLogService = UnityCIContainer.Instance.GetService<IApiMonitorLogService>();
        }

        #region 接口访问监控日志明细操作
        /// <summary>
        /// 根据访问监控日志条件分页获取列表 关键字【控制器名称】【方法名】  开始时间
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<ApiMonitorLogResponse>> LoadList([FromBody]ParametersInfo<ApiMonitorLogQuery> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<ApiMonitorLogResponse>>();

            TryCatch(() =>
            {
                inputInfo.Validate();
                resultInfo = apiMonitorLogService.LoadApiMonitorLogList(inputInfo);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取监控日志条件分页列表失败");
            }, $"系统错误，接口访问监控日志-获取监控日志条件分页列表失败");
            return resultInfo;
        }
        /// <summary>
        /// 批量删除监控日志
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Remove([FromBody]ApiMonitorLogRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                inputInfo.Validate();
                resultInfo = apiMonitorLogService.Remove(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "删除监控日志失败");

            }, $"系统错误，接口访问监控日志-删除监控日志失败");

            return resultInfo;
        }


        #endregion

        #region 接口访问记录统计信息


        /// <summary>
        /// 获取接口实时访问统计信息
        /// </summary>
        /// <param name="timed"></param>
        /// <returns></returns>
        [HttpGet("LoadRequestInfo/{timed}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true, false)]
        public ResultJsonInfo<Echarts2DInfo<string, string>> LoadRequestInfo(DateTime timed)
        {
            var resultInfo = new ResultJsonInfo<Echarts2DInfo<string, string>>();

            TryCatch(() =>
            {
                resultInfo = apiMonitorLogService.loadRequestInfo(timed);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取接口实时访问统计信息失败");
            }, $"系统错误，接口访问监控日志-获取接口实时访问统计信息失败");
            return resultInfo;
        }


        /// <summary>
        /// 获取某个时间段内的接口实时访问统计信息
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpGet("LoadRequestInfoByTime/{beginTime}/{endTime}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true, false)]
        public ResultJsonInfo<Echarts2DInfo<string, string>> LoadRequestInfoByTime(DateTime beginTime, DateTime endTime)
        {
            var resultInfo = new ResultJsonInfo<Echarts2DInfo<string, string>>();

            TryCatch(() =>
            {
                resultInfo = apiMonitorLogService.loadRequestInfoByTime(beginTime, endTime);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取某个时间段内的接口实时访问统计信息失败");

            }, $"系统错误，接口访问监控日志-获取某个时间段内的接口实时访问统计信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 获取高频访问接口信息
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpGet("LoadHighFrequencyInterface/{beginTime}/{endTime}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true, false)]
        public ResultJsonInfo<Echarts2DInfo<Dictionary<string, object>, string>> LoadHighFrequencyInterface(DateTime beginTime, DateTime endTime)
        {
            var resultInfo = new ResultJsonInfo<Echarts2DInfo<Dictionary<string, object>, string>>();

            TryCatch(() =>
            {
                resultInfo = apiMonitorLogService.LoadHighFrequencyInterface(beginTime, endTime);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取高频访问接口信息失败");

            }, $"系统错误，接口访问监控日志-获取高频访问接口信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 获取接口预警信息
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadInterfaceWarning")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true, false)]
        public ResultJsonInfo<List<ApiMonitorLogPathIpGroup>> LoadInterfaceWarning([FromBody]ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<ApiMonitorLogPathIpGroup>> ();

            TryCatch(() =>
            {
                resultInfo = apiMonitorLogService.LoadInterfaceWarning(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取接口预警信息失败");

            }, $"系统错误，接口访问监控日志-获取接口预警信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 获取黑名单信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadBlacklist")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true, false)]
        public ResultJsonInfo<List<ApiMonitorLogNameList>> LoadBlacklist([FromBody]ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<ApiMonitorLogNameList>>();

            TryCatch(() =>
            {
                resultInfo = apiMonitorLogService.LoadBlacklist(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取黑名单信息失败");

            }, $"系统错误，接口访问监控日志-获取黑名单信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 加入黑名单
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        [HttpGet("AddBalckList/{ip}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true, false)]
        public ResultJsonInfo<bool> AddBalckList(string ip) {

            var resultInfo = new ResultJsonInfo<bool>();
            TryCatch(() =>
            {
                resultInfo = apiMonitorLogService.AddBalckList(ip);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "加入黑名单失败");

            }, $"系统错误，接口访问监控日志-加入黑名单失败");

            return resultInfo;
        }

        /// <summary>
        /// 删除黑名单
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        [HttpGet("RemoveBalckList/{ip}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true, false)]
        public ResultJsonInfo<bool> RemoveBalckList(string ip)
        {

            var resultInfo = new ResultJsonInfo<bool>();
            TryCatch(() =>
            {
                resultInfo = apiMonitorLogService.RemoveBalckList(ip);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "删除黑名单失败");

            }, $"系统错误，接口访问监控日志-删除黑名单失败");

            return resultInfo;
        }

        /// <summary>
        /// 获取接口预警和黑名单个数信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadApiMonitorCount")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true, false)]
        public ResultJsonInfo<ApiMonitorLogCount> LoadApiMonitorCount()
        {
            var resultInfo = new ResultJsonInfo<ApiMonitorLogCount>();

            TryCatch(() =>
            {
                resultInfo = apiMonitorLogService.LoadApiMonitorCount();

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取接口预警和黑名单个数信息失败");

            }, $"系统错误，接口访问监控日志-获取接口预警和黑名单个数信息失败");
            return resultInfo;
        }
        #endregion

    }
}