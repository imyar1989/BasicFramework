﻿using Api.DevOpsManage.App_Start;
using DevOps.Logic;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Validate.Library;
/*
* 命名空间: Api.DevOpsManage.Areas.Authority.Controllers
*
* 功 能： 岗位接口控制器
*
* 类 名： PostController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Api.DevOpsManage.Areas.Authority.Controllers
{
    /// <summary>
    /// 部门接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Authority")]
    [Route("api/Authority/[controller]")]
    [ApiController]
    public class DepartmentController : BaseApiController
    {
        ISysDepartmentService departmentService = null;
        /// <summary>
        /// 部门接口控制器构造函数
        /// </summary>
        public DepartmentController()
        {
            //部门管理逻辑注入
            departmentService = UnityCIContainer.Instance.GetService<ISysDepartmentService>();
        }

        #region 部门基础信息管理模块

        #region 信息查询

        /// <summary>
        /// 根据关键字【部门名称，领导人名称，联系电话】获取所有的未删除部门信息
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、无对应值存在，返回 1205
        /// </remarks>
        /// <param name="inputInfo">关键字【部门名称，领导人名称，联系电话】</param>
        /// <returns></returns>
        [HttpPost("LoadPageList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<List<DepartmentResponse>> LoadPageList([FromBody]ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<DepartmentResponse>>();

            TryCatch(() => {

                resultInfo = departmentService.LoadPageList(inputInfo);

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "根据关键字获取所有的未删除菜单信息失败");

            }, $"系统错误，部门管理-根据关键字获取所有的未删除菜单信息失败");

            return resultInfo;
        }


        /// <summary>
        /// 根据关键字【部门名称，领导人名称，联系电话】获取所有的未删除部门树状信息
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、无对应值存在，返回 1205
        /// </remarks>
        /// <param name="inputInfo">inputInfo.parameters关键字【部门名称，领导人名称，联系电话】</param>
        /// <returns></returns>
        [HttpPost("LoadTreeList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<List<DepartmentTreeInfo>> LoadTreeList([FromBody]ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<DepartmentTreeInfo>>();

            TryCatch(() => {

                resultInfo = departmentService.LoadTreeList(inputInfo.parameters);

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "根据关键字获取所有的未删除部门信息失败");

            }, $"系统错误，部门管理-根据关键字获取所有的未删除部门信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 根据部门ID，获取所有可操作用户情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadUserListByDeparId")]
        [ClientApiFilter(DeviceType.PCBack,true)]
        [Authorize]
        public ResultJsonInfo<List<DepartmentUserResponse>> LoadUserListByDeparId([FromBody]DepartmentUserQuery queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<DepartmentUserResponse>>();

            TryCatch(() =>
            {
                resultInfo = departmentService.LoadUserListByDeparId(queryInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "根据部门ID，获取所有可操作用户情况信息失败");

            }, $"系统错误，部门管理- 根据部门ID，获取所有可操作用户情况信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 根据部门ID，获取所有可操作领导情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadLeaderListByDeparId")]
        [ClientApiFilter(DeviceType.PCBack,true)]
        [Authorize]
        public ResultJsonInfo<List<DepartmentUserResponse>> LoadLeaderListByDeparId([FromBody]DepartmentUserQuery queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<DepartmentUserResponse>>();

            TryCatch(() =>
            {
                resultInfo = departmentService.LoadLeaderListByDeparId(queryInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "根据部门ID，获取所有可操作领导情况失败");

            }, $"系统错误，部门管理- 根据部门ID，获取所有可操作领导情况信息失败");

            return resultInfo;
        }

        #endregion

        #region 更新操作

        /// <summary>
        /// 新增节点信息
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、操作失败、无对应父节点信息，返回 1205
        /// </remarks>
        /// <param name="addInfo">子节点信息</param>
        /// <returns></returns>
        [HttpPost("Addnode")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Addnode([FromBody]DepartmentChildAddRequest addInfo )
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                addInfo.Validate();
                resultInfo = departmentService.Addnode(addInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "新增根节点信息失败");

            }, $"系统错误，部门管理-新增根节点信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 修改节点信息
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、无对应值存在、操作失败，返回 1205
        /// </remarks>
        /// <returns></returns>
        [HttpPost("ModifyInfo")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> ModifyInfo([FromBody]DepartmentModifyRequest modifyInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                modifyInfo.Validate();
                resultInfo = departmentService.Modify(modifyInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "修改节点信息信息失败");

            }, $"系统错误，部门管理-修改节点信息信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 禁用/启用部门
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、操作失败、无对应信息，返回 1205，无效操作
        /// </remarks>
        /// <returns></returns>
        [HttpGet("ForbidOrEnable/{id}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> ForbidOrEnable(string id)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = departmentService.ForbidOrEnable(id);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "禁用/启用部门失败");

            }, $"系统错误，禁用/启用部门失败");
            return resultInfo;
        }

        /// <summary>
        /// 修改对应部门的用户
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        [HttpPost("ModifyDeparUserInfo")]
        [ClientApiFilter(DeviceType.PCBack,true)]
        [Authorize]
        public ResultJsonInfo<int> ModifyDeparUserInfo([FromBody]DepartmentUserModifyRequest department)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = departmentService.ModifyDeparUserInfo(department);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "修改对应部门的用户信息失败");

            }, $"系统错误，部门管理-修改对应部门的用户信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 修改对应部门的领导
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        [HttpPost("ModifyDeparLeaderInfo")]
        [ClientApiFilter(DeviceType.PCBack,true)]
        [Authorize]
        public ResultJsonInfo<int> ModifyDeparLeaderInfo([FromBody]DepartmentUserModifyRequest department)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = departmentService.ModifyDeparLeaderInfo(department);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "修改对应部门的领导失败");

            }, $"系统错误，部门管理-修改对应部门的领导失败");

            return resultInfo;
        }
        #endregion

        #region 删除信息

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="removeInfo"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Remove(List<string> removeInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = departmentService.Remove(removeInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "删除信息操作失败");

            }, $"系统错误，部门管理-删除信息操作失败");

            return resultInfo;
        }

        #endregion

        #region 移动顺序操作
        /// <summary>
        /// 移动顺序操作（1：上移/2：下移）
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// </remarks>
        /// <returns></returns>
        [HttpPost("Move")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Move([FromBody]DepartmentMoveRequest moveInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                moveInfo.Validate();
                resultInfo = departmentService.Move(moveInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "移动顺序操作失败");

            }, $"系统错误，部门管理-移动顺序操作失败");

            return resultInfo;
        }
        #endregion

        #endregion

        #region 权限相关模块

        /// <summary>
        /// 获取所有启用组织机构树状信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadAllTreeList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<TreeInfo>> LoadAllTreeList()
        {
            var resultInfo = new ResultJsonInfo<List<TreeInfo>>();

            TryCatch(() => {

                resultInfo = departmentService.LoadAllTreeList();

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "获取所有启用组织机构树状信息失败");

            }, $"系统错误，部门管理-获取所有启用组织机构树状信息失败");

            return resultInfo;
        }


        /// <summary>
        /// 获取所有部门Select树状数据
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadAllSelectList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<SelectListInfo>> LoadAllSelectList()
        {
            var resultInfo = new ResultJsonInfo<List<SelectListInfo>>();
            TryCatch(() =>
            {
                resultInfo = departmentService.LoadDepartmentTreeList();
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取所有部门Select树状数据失败！");

            }, $"系统错误，获取所有部门Select树状数据失败！");
            return resultInfo;
        }
        #endregion
    }
}