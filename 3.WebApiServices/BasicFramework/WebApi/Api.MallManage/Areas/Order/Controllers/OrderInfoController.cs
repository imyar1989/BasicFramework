﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.MallManage.App_Start;
using Common.Library;
using Common.Model;
using Container.Library;
using Mall.Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Validate.Library;

/*
* 命名空间: Api.MallManage.Areas.Client.Controllers
*
* 功 能： 订单操作接口控制器
*
* 类 名： InfoController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/11/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Api.MallManage.Areas.Order.Controllers
{
    /// <summary>
    /// 订单操作接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Order")]
    [Route("api/Order/[controller]")]
    [ApiController]
    public class OrderInfoController : BaseApiController
    {

        private readonly IOrderInfoService orderinfoService = null;
        
        /// <summary>
        /// 构造函数
        /// </summary>
        public OrderInfoController()
        {
            orderinfoService = UnityCIContainer.Instance.GetService<IOrderInfoService>();
        }

        #region 查询

        /// <summary>
        ///根据关键字分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadPageList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<List<OrderInfoResponse>> LoadPageList([FromBody]ParametersInfo<OrderInfoQuery> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<OrderInfoResponse>>();
            TryCatch(() =>
            {
                resultInfo = orderinfoService.LoadPageList(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取分页列表失败");

            }, $"系统错误，订单管理-获取分页列表失败");

            return resultInfo;
        }

        #endregion

        #region 更新
        /// <summary>
        /// 更新订单信息[只修改]
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("ModifyInfo")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> ModifyInfo([FromBody]OrderInfoModifyRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                inputInfo.Validate();
                resultInfo = orderinfoService.ModifyInfo(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "更新订单信息失败");

            }, $"系统错误，订单管理-更新订单信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 订单发货操作
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("DeliverInfo")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> DeliverInfo([FromBody] OrderInfoDeliverRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                inputInfo.Validate();
                resultInfo = orderinfoService.DeliverInfo(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "订单发货操作失败");

            }, $"系统错误，订单管理-订单发货操作失败");
            return resultInfo;
        }
        
        #endregion

        #region 删除
        /// <summary>
        /// 删除订单信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                resultInfo = orderinfoService.Remove(ids);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "物理删除订单失败");

            }, $"系统错误，订单管理-物理订单消息失败");
            return resultInfo;
        }
        #endregion

        #region 公用功能

        /// <summary>
        /// 获取支付方式类型
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        [HttpGet("LoadPayTypeList")]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadPayTypeList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            TryCatch(() =>
            {
                resultInfo = orderinfoService.LoadPayTypeList();

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取支付方式类型失败");

            }, $"系统错误，订单管理-获取支付方式类型失败");


            return resultInfo;
        }

        /// <summary>
        /// 获取订单状态类型
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        [HttpGet("LoadOrderStatusList")]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadOrderStatusList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            TryCatch(() =>
            {
                resultInfo = orderinfoService.LoadOrderStatusList();

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取订单状态类型失败");

            }, $"系统错误，订单管理-获取订单状态类型失败");


            return resultInfo;
        }

        /// <summary>
        /// 获取物流商类型
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        [HttpGet("LoadLogisticsProviderList")]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadLogisticsProviderList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            TryCatch(() =>
            {
                resultInfo = orderinfoService.LoadLogisticsProviderList();

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取物流商类型失败");

            }, $"系统错误，订单管理-获取物流商类型失败");

            return resultInfo;
        }
        #endregion
    }
}