﻿/*
* 命名空间: Authority.Logic
*
* 功 能： 菜单树结构实体类
*
* 类 名： MenuInfo
*
* Version   变更日期            负责人     变更内容
* ────────────────────────────
* V1.0.1    2019/08/17 19:48:31 Harvey     创建
*
* Copyright (c) 2019 Harvey Corporation. All rights reserved.
*/
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Library
{
    /// <summary>
    /// 树结构信息
    /// </summary>
    public class TreeInfo
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 节点字段名
        /// </summary>
        public string field { get; set; }

        /// <summary>
        /// 点击节点弹出新窗口对应的 url。需开启 isJump 参数
        /// </summary>
        public string href { get; set; }

        /// <summary>
        /// 节点是否初始展开，默认 false
        /// </summary>
        public string spread { get; set; }

        /// <summary>
        /// 节点是否初始为选中状态（如果开启复选框的话），默认 false
        /// </summary>
        [JsonProperty(PropertyName = "checked")]
        public bool selected { get; set; }

        /// <summary>
        ///节点是否为禁用状态。默认 false
        /// </summary>
        public bool disabled { get; set; }

        /// <summary>
        ///类型100是部门  110是人员
        /// </summary>
        public int type { get; set; }
        /// <summary>
        /// 子节点
        /// </summary>
        public List<TreeInfo> children { get; set; } = new List<TreeInfo>();
    }
}
