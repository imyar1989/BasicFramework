﻿/***************************************************************************************************
  	创建时间:	2018-09-10
	文件名字:	SpatialEngineFactory.cs
	作    者:	周业
	版    本:	V1.000.000
	说    明:	空间数据引擎操作的基础工厂类，用于实例化空间数据引擎辅助类。
	历史记录：	
	<作  者>		<修改时间>		  <版  本>		       <目  的>	
****************************************************************************************************/

using System;
using BoatLeaf.Import.FileHelper;
using System.Xml.Linq;
using System.Xml.XPath;

namespace SpatialEngine.Library
{
    public class SpatialEngineFactory
    {

        /// <summary>
        /// 配置文件地址
        /// </summary>
        private static string strXmlFilePath = string.Empty;

        private static string strSystemPath = "Config\\system.xml";


        /// <summary>
        /// 实例化空间数据引擎辅助类
        /// </summary>
        /// <returns></returns>
        public static SpatialArcGISEngineBase CreateSpatialEngineBase()
        {
            try
            {

                SpatialArcGISEngineBase oSpatialEngineBase = null;
                var rootDic = AppDomain.CurrentDomain.BaseDirectory;
                strXmlFilePath = string.Format("{0}{1}", rootDic, strSystemPath);

                XElement oXElement = XElement.Load(strXmlFilePath);
                switch (oXElement.XPathSelectElement(@"/spatialengineconfig/spatialenginetype").Value)
                {
                    case "arcgisfororacle":
                        oSpatialEngineBase = new ArcGISEngineForOracle();
                        break;
                    case "arcgisformdb":
                        break;
                    case "arcgisforgdb":
                        break;
                    case "arcgisforpostgresql":
                        oSpatialEngineBase = new ArcGISEngineForPostgresql();
                        break;
                    case "arcgisforsqlserver":
                        break;
                    case "arcgisformysql":
                        break;
                }
                return oSpatialEngineBase;
            }
            catch (Exception ex)
            {
                TxtHelper.WriteLog(ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// 实例化空间数据引擎辅助类
        /// </summary>
        /// <param name="strXmlFilePath">文件路径</param>
        /// <param name="strXmlNodePath">节点路径</param>
        /// <returns></returns>
        public static SpatialArcGISEngineBase CreateSpatialEngineBase(string strXmlFilePath, string strXmlNodePath)
        {
            try
            {
                SpatialArcGISEngineBase oSpatialEngineBase = null;
                if (strXmlNodePath.Trim() != "")
                {
                    if (strXmlFilePath.Trim() == "")
                    {
                        strXmlFilePath = string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory, strSystemPath);
                    }
                }
                else
                {
                    return null;
                }
                XElement oXElement = XElement.Load(strXmlFilePath);
                switch (oXElement.XPathSelectElement(@"/spatialengineconfig/spatialenginetype").Value)
                {
                    case "arcgisfororacle":
                        oSpatialEngineBase = new ArcGISEngineForOracle(strXmlFilePath, strXmlNodePath);
                        break;
                    case "arcgisformdb":
                        break;
                    case "arcgisforgdb":
                        break;
                    case "arcgisforpostgresql":
                        oSpatialEngineBase = new ArcGISEngineForPostgresql();
                        break;
                    case "arcgisforsqlserver":
                        break;
                    case "arcgisformysql":
                        break;
                }
                return oSpatialEngineBase;
            }
            catch (Exception ex)
            {
                TxtHelper.WriteLog(ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// 实例化空间数据引擎辅助类
        /// </summary>
        /// <param name="strDatabaseType">数据库类型</param>
        /// <param name="strXmlFilePath">文件路径</param>
        /// <param name="strXmlNodePath">节点路径</param>
        /// <returns></returns>
        public static SpatialArcGISEngineBase CreateSpatialEngineBase(string strDatabaseType, string strXmlFilePath, string strXmlNodePath)
        {
            try
            {
                SpatialArcGISEngineBase oSpatialEngineBase = null;
                if (strXmlNodePath.Trim() != "")
                {
                    if (strXmlFilePath.Trim() == "")
                    {
                        strXmlFilePath = string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory, strSystemPath);
                    }
                }
                else
                {
                    return null;
                }
                switch (strDatabaseType.Trim())
                {
                    case "arcgisfororacle":
                        oSpatialEngineBase = new ArcGISEngineForOracle(strXmlFilePath, strXmlNodePath);
                        break;
                    case "arcgisformdb":
                        break;
                    case "arcgisforgdb":
                        break;
                    case "arcgisforpostgresql":
                        oSpatialEngineBase = new ArcGISEngineForPostgresql();
                        break;
                    case "arcgisforsqlserver":
                        break;
                    case "arcgisformysql":
                        break;
                }
                return oSpatialEngineBase;
            }
            catch (Exception ex)
            {
                TxtHelper.WriteLog(ex.ToString());
                throw;
            }
        }

    }
}
