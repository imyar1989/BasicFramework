﻿using System.Collections.Generic;
using Newtonsoft.Json;
namespace RabbitMQ.Library
{
    /// <summary>
    /// 路由规则信息
    /// </summary>
    public class RoutingRuleInfo
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 路由规则
        /// </summary>
        public string RoutingKey { get; set; }

        /// <summary>
        /// 源
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// 目标
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// 方法参数
        /// </summary>
        public Dictionary<string, object> Arguments { set; get; }

        /// <summary>
        /// 类型  ExchageToExchage ExchageToQueue
        /// </summary>
        public RouteRuleTypes Type { get; set; }
    }
}

