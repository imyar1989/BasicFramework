﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RabbitMQ.Library
{
    /// <summary>
    /// 队列配置信息
    /// </summary>
    public class QueuesInfo
    {
        /// <summary>
        /// 交换机名称
        /// </summary>
        public string Queue { get; set; }

        /// <summary>
        /// 是否持久化
        /// 队列的声明默认是存放到内存中的，如果rabbitmq重启会丢失，如果想重启之后还存在就要使队列持久化，
        /// 保存到Erlang自带的Mnesia数据库中，当rabbitmq重启之后会读取该数据库
        /// </summary>
        public bool Durable { get; set; }

        /// <summary>
        /// 是否排外的：有两个作用
        /// 一：当连接关闭时connection.close()该队列是否会自动删除；
        /// 二：该队列是否是私有的private，如果不是排外的，可以使用两个消费者都访问同一个队列，没有任何问题，
        ///    如果是排外的，会对当前队列加锁，其他通道channel是不能访问的，如果强制访问会报异常：
        ///    com.rabbitmq.client.ShutdownSignalException: channel error; protocol method: #method
        ///    (reply-code=405, reply-text=RESOURCE_LOCKED - cannot obtain exclusive access to locked queue 'queue_name' in vhost '/', class-id=50, method-id=20)
        ///    一般等于true的话用于一个队列只能有一个消费者来消费的场景
        /// </summary>
        public bool Exclusive { get; set; }

        /// <summary>
        /// 是否自动删除；如果是true，队列将在至少一个消费者连接之后删除自己，
        /// 然后所有消费者都断开连接。
        /// </summary>
        public bool AutoDelete { get; set; }

        /// <summary>
        /// 方法参数；队列中的消息什么时候会自动被删除。
        /// </summary>
        public Dictionary<string, object> Arguments { set; get; }
    }
}
