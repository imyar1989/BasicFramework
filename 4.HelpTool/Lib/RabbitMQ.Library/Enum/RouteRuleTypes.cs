﻿using System.ComponentModel;

namespace RabbitMQ.Library
{
    /// <summary>
    /// 路由规则类型
    /// </summary>
    public enum RouteRuleTypes
    {
        /// <summary>
        /// 交换机到交换机
        /// </summary>
        [Description("ExchageToExchage")]
        ExchageToExchage =0,

        /// <summary>
        /// 交换机到队列
        /// </summary>
        [Description("ExchageToQueue")]
        ExchageToQueue =1
    }
}
