﻿using System.ComponentModel;

namespace RabbitMQ.Library
{
    /// <summary>
    /// 数据类型
    /// </summary>
    public enum DataType
    {
        /// <summary>
        /// json格式
        /// </summary>
        [Description("json")]
        JsonType = 0,

        /// <summary>
        ///XML格式
        /// </summary>
        [Description("xml")]
        XmlType = 1

    }
}
