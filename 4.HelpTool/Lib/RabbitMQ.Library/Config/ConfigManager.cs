﻿using System;
using System.IO;

namespace RabbitMQ.Library
{
    /// <summary>
    /// 创建工厂
    /// </summary>
    internal class ConfigManager
    {

        /// <summary>
        /// 配置信息
        /// </summary>
        private static RabbitConfig _connectConfig;

        /// <summary>
        /// 数据库配置信息
        /// </summary>
        internal static RabbitConfig config => _connectConfig;

        /// <summary>
        /// 静态代码块，程序启动时初始化
        /// </summary>
        static ConfigManager()
        {
            try
            {
                string rootDic = string.Empty;
                //相对路径
                //rootDic = Directory.GetCurrentDirectory();
                //rootDic = AppDomain.CurrentDomain.BaseDirectory;
                rootDic = AppContext.BaseDirectory;
                //获取当前程序bin目录路径(还未在Web环境下测试过)
                using (StreamReader sr = new StreamReader(Path.Combine(rootDic, "Config/rabbitmq.config.json")))
                {
                    _connectConfig = sr.ReadToEnd().JsonToObject<RabbitConfig>();
                }
            }
            catch (Exception ex)
            {
                _connectConfig.Success = false;
                _connectConfig.ErrorMessage = ex.Message;
            }
        }
    }
}
