﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MongoDB.Library
{
    /// <summary>
    /// 该类主要保存了mongodb的一些配置信息
    /// </summary>
    public class MongoDBConfig
    {
        /// <summary>
        /// 是否加密
        /// </summary>
        public bool IsEncryption { get; set; }

        /// <summary>
        /// 连接字符串
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// 数据库
        /// </summary>
        public string DataBase { get; set; }

        /// <summary>
        /// 是否需要安全授权验证
        /// </summary>
        public bool IsAuthority { get; set; }

        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        public string Ex { get; set; }
    }
}
