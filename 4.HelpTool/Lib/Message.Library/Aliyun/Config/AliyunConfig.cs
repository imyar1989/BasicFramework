﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Message.Library.Aliyun
{
    /// <summary>
    /// 相关配置的DOM
    /// </summary>
    public class AliyunConfig
    {
        /// <summary>
        /// 服务器节点：基本不变。
        /// </summary>
        public string Endpoint { get; set; }

        /// <summary>
        /// 阿里云RAM/accessKey信息：accessKeyId
        /// </summary>
        public string AccessKeyId { get; set; }

        /// <summary>
        /// 阿里云RAM/accessKey信息：accessKeySecret
        /// </summary>
        public string AccessKeySecret { get; set; }

        /// <summary>
        ///短信签名
        /// </summary>
        public string SignName { get; set; }

        /// <summary>
        ///短信模板
        /// </summary>
        public Dictionary<string, string> TemplateCode { get; set; }

        /// <summary>
        /// 是否加密
        /// </summary>
        public bool IsEncryption { get; set; }
    }
}
