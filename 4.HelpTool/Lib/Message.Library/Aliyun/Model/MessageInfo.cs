﻿namespace Message.Library.Aliyun
{
    /// <summary>
    /// 消息验证发送实体信息
    /// </summary>
    public class MessageInfo<T>
    {
        /// <summary>
        /// 目标手机号码
        /// </summary>
        public string Mobile { set; get; }

        /// <summary>
        /// 短信模板Key
        /// </summary>
        public string TemplateCodeKey { set; get; }

        /// <summary>
        /// 消息实体信息
        /// </summary>
        public T Message { set; get; }

    }
}
