﻿namespace Scheduler.Library
{
    /// <summary>
    /// 调度器的状态
    /// </summary>
    public enum SchedulerStatus
    {
        /// <summary>
        /// 已开始
        /// </summary>
        Started,

        /// <summary>
        /// 已关闭
        /// </summary>
        Shutdown,

        /// <summary>
        /// 未开始
        /// </summary>
        NotStarted
    }
}
