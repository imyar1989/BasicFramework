﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间: Scheduler.Library.Hangfire
*
* 功 能： Hangfire客户调用入口
*
* 类 名： HangfireClient
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/13 11:20:26 				罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Scheduler.Library
{
    /// <summary>
    /// Hangfire客户调用入口
    /// </summary>
    public class HangfireClient
    {
        /// <summary>
        /// redis配置信息
        /// </summary>
        private static readonly RedisConfig redisConfig = null;

        /// <summary>
        /// 初始化
        /// </summary>
        static HangfireClient()
        {
            //组合RedisConfig
            redisConfig = ConfigManager.config;
        }

        /// <summary>
        /// 根据redis使用类型来生成相应的连接字符串
        /// </summary>
        /// <param name="redisConfig"></param>
        /// <returns></returns>
        public static string GenerateConnectionString()
        {
            var configStr = string.Empty;

            if (!string.IsNullOrWhiteSpace(redisConfig.Slaves))
                configStr = string.Format("{0},{1},defaultDatabase={2}", redisConfig.Masters, redisConfig.Slaves, redisConfig.DefaultDatabase);
            else
                configStr = string.Format("{0},defaultDatabase={1}", redisConfig.Masters, redisConfig.DefaultDatabase);


            if (!string.IsNullOrWhiteSpace(redisConfig.Password))
                configStr += ",password=" + redisConfig.Password;


            configStr += string.Format(",allowAdmin={0},connectRetry={1},connectTimeout={2},keepAlive={3},syncTimeout={4},abortConnect=false", redisConfig.AllowAdmin, redisConfig.ConnectRetry, redisConfig.ConnectTimeout, redisConfig.KeepAlive, redisConfig.CommandTimeout);

            if (!string.IsNullOrWhiteSpace(redisConfig.Extention))
            {
                configStr += "," + redisConfig.Extention;
            }
            return configStr;
        }
    }
}
