﻿using AutoMapper;
using AutoMapper.Configuration;
using System;
using System.Collections.Generic;

namespace Container.Library
{
    public class MappingContainer
    {

        /// <summary>
        /// Type集合
        /// </summary>
        private static List<Type> typeList = new List<Type>();

        static MappingContainer()
        {
            List<RegisterType> registerTypeList = RegisterMapping.GetTypesList();
            typeList.Clear();
            foreach (var item in registerTypeList)
            {
                typeList.Add(ReflectionHelper.GetType(item.InterfaceOrNamespace, item.ImplementOrProfile));
            }
        }

        /// <summary>
        /// Mappings初始化
        /// </summary>
        public static void RegisterMappings()
        {
            var cfg = new MapperConfigurationExpression();
            foreach (var typeInfo in typeList)
            {
                cfg.AddProfile(typeInfo);
            }
            Mapper.Initialize(cfg);
        }


        /// <summary>
        /// 获取需要映射的Profile
        /// </summary>
        /// <returns></returns>
        public static Type[] GetProfileTypes()
        {
            return typeList.ToArray();
        }
    }
}
