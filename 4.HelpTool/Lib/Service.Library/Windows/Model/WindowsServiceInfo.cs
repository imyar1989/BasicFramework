﻿/*
* 命名空间: Service.Library
*
* 功 能： Windows服务的信息实体
*
* 类 名： WindowsServiceInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/5/18 13:27:20 	罗维    创建
* ─────────────────────────────────────────────────
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/

namespace Service.Library
{
    /// <summary>
    /// Windows服务的信息实体
    /// </summary>
    public class WindowsServiceInfo
    {
        /// <summary>
        /// 是否是WebApi
        /// </summary>
        public bool isWebApi { get; set; }

        /// <summary>
        /// IP
        /// </summary>
        public string IP { get; set; }

        /// <summary>
        /// 端口
        /// </summary>
        public string Port { get; set; }

        /// <summary>
        /// 站点http://IP:Port
        /// </summary>
        public string DomainPort { get; set; }

        /// <summary>
        /// 服务名称【名称与exe执行文件一样】
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// 物理路径
        /// </summary>
        public string PhysicalPath { get; set; }

        /// <summary>
        /// 可执行文件名称
        /// </summary>
        public string ExeFile { get; set; }

    }
}
