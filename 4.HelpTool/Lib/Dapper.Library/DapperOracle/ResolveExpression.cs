﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Dapper.Library.DapperOracle
{
    internal class ResolveExpression:IResolveExpression
    {
        public ResolveExpression(SqlProvider provider):base(provider)
        {

        }
        public override string ResolveSelect(int? topNum)
        {
			//添加需要连接的导航表
			var masterEntity = EntityCache.QueryEntity(abstractSet.TableType, provider);
			var navigationList = masterEntity.Navigations;
			if (navigationList.Any())
			{
				provider.JoinList.AddRange(ExpressionExtension.Clone(navigationList));
			}

			var selectFormat = " SELECT {0} ";
            var selectSql = "";
            if (provider.Context.Set.SelectExpression == null)
            {
                var propertyBuilder = GetTableField(masterEntity);
                selectSql = string.Format(selectFormat, propertyBuilder);
            }
            else
            {
                var selectExp = new SelectExpression(provider.Context.Set.SelectExpression, "", provider);
                selectSql = string.Format(selectFormat, selectExp.SqlCmd);
                provider.Params.AddDynamicParams(selectExp.Param);
            }
            return selectSql;
        }

        public override string ResolveSum(LambdaExpression selector)
        {
            if (selector == null)
                throw new ArgumentException("selector");
            var selectSql = "";

            switch (selector.NodeType)
            {
                case ExpressionType.Lambda:
                case ExpressionType.MemberAccess:
                    {
                        EntityObject entityObject = EntityCache.QueryEntity(selector.Parameters[0].Type, provider);
                        var memberName = selector.Body.GetCorrectPropertyName();
                        selectSql = $@" SELECT NVL(SUM({entityObject.GetAsName(providerOption)}
                                 {providerOption.CombineFieldName(entityObject.FieldPairs[memberName])}),0)  ";
                    }
                    break;
                case ExpressionType.MemberInit:
                    throw new DapperExtensionException("不支持该表达式类型");
            }

            return selectSql;
        }
        public override string ResolveMax(LambdaExpression selector)
        {
            if (selector == null)
                throw new ArgumentException("selector");
            var selectSql = "";

            switch (selector.NodeType)
            {
                case ExpressionType.Lambda:
                case ExpressionType.MemberAccess:
                    {
                        EntityObject entityObject = EntityCache.QueryEntity(selector.Parameters[0].Type, provider);
                        var memberName = selector.Body.GetCorrectPropertyName();
                        selectSql = $" SELECT Max({entityObject.AsName}.{providerOption.CombineFieldName(entityObject.FieldPairs[memberName])})  ";
                    }
                    break;
                case ExpressionType.MemberInit:
                    throw new DapperExtensionException("不支持该表达式类型");
            }

            return selectSql;
        }

        public override string ResolveMin(LambdaExpression selector)
        {
            if (selector == null)
                throw new ArgumentException("selector");
            var selectSql = "";

            switch (selector.NodeType)
            {
                case ExpressionType.Lambda:
                case ExpressionType.MemberAccess:
                    {
                        EntityObject entityObject = EntityCache.QueryEntity(selector.Parameters[0].Type, provider);
                        var memberName = selector.Body.GetCorrectPropertyName();
                        selectSql = $" SELECT Min({entityObject.AsName}.{providerOption.CombineFieldName(entityObject.FieldPairs[memberName])})  ";
                    }
                    break;
                case ExpressionType.MemberInit:
                    throw new DapperExtensionException("不支持该表达式类型");
            }

            return selectSql;
        }

        public override string ResolveWithNoLock(bool nolock)
        {
            return "";
        }
    }
}
