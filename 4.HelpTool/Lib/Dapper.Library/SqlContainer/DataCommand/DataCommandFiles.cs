﻿
using System.Xml.Serialization;

namespace Dapper.Library.SqlContainer
{
	/// <summary>
    /// SQL配置文件集合
    /// </summary>
	[XmlRoot("dataCommandFiles", Namespace = "http://www.Framework.com/Framework/DataAccess")]
	public class DataCommandFiles
	{  
		/// <summary>
	    /// SQL命令的集合
	    /// </summary>
		[XmlElement("dataCommandFile")]
		public DataCommandFileInstance[] InstanceList { get; set; }
	}
}
