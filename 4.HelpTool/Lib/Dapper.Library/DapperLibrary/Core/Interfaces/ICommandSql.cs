﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dapper.Library
{
    /// <summary>
    /// Command SQL操作
    /// </summary>
    public interface ICommandSql<T>
    {
        /// <summary>
        /// 使用SQL执行查询语句
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        CommandSql<T> BySql(string predicate, object parameter);
    }
}
