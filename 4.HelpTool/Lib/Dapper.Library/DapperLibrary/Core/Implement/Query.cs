﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;


namespace Dapper.Library
{
    /// <summary>
    /// 查询
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Query<T> : AbstractSet, IQuery<T>
    {
        public readonly IDbConnection DbCon;
        public readonly IDbTransaction DbTransaction;

        protected DataBaseContext<T> SetContext { get; set; }

        /// <summary>
        /// 回收信息
        /// </summary>
        private void Recovery()
        {
            SqlProvider.Params = new DynamicParameters();
            SqlProvider.JoinList = new List<JoinAssTable>();
            SqlProvider.AsTableNameDic = new Dictionary<Type, string>();
        }


        protected Query(IDbConnection conn, SqlProvider sqlProvider)
        {
            SqlProvider = sqlProvider;
            DbCon = conn;
        }

        protected Query(IDbConnection conn, SqlProvider sqlProvider, IDbTransaction dbTransaction)
        {
            SqlProvider = sqlProvider;
            DbCon = conn;
            DbTransaction = dbTransaction;
        }

        public T Get()
        {

            T result;
            SqlProvider.FormatToSingle<T>();

            // 如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(T).GetInterface("IDictionary");
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                            Select(
                                m => ((IDictionary<string, object>)m)
                                    .ToDictionary(pi => pi.Key, pi => pi.Value))
                            .FirstOrDefault<IDictionary<string, object>>();

                if (ret!=null)
                {
                    // 随意抽一条取最大条数记录
                    result = (T)ret;
                }
                else
                {
                    result = default(T);
                }
            }
            else
            {
                result = DbCon.QueryFirstOrDefaults<T>(SqlProvider, DbTransaction);
            }

            Recovery();
            return result;
        }

        public TSource Get<TSource>()
        {
            TSource result;
            SqlProvider.FormatToSingle<T>();

            // 如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(TSource).GetInterface("IDictionary");
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                            Select(
                                m => ((IDictionary<string, object>)m)
                                    .ToDictionary(pi => pi.Key, pi => pi.Value))
                            .FirstOrDefault<IDictionary<string, object>>();

                if (ret != null)
                {
                    // 随意抽一条取最大条数记录
                    result = (TSource)ret;
                }
                else
                {
                    result = default(TSource);
                }
            }
            else
            {
                result = DbCon.QueryFirstOrDefaults<TSource>(SqlProvider, DbTransaction);
            }

            Recovery();
            return result;
        }

        public TReturn Get<TReturn>(Expression<Func<T, TReturn>> select)
        {
            TReturn result;

            SqlProvider.Context.Set.SelectExpression = select;
            SqlProvider.FormatToSingle<T>();

            // 如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(T).GetInterface("IDictionary");
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                            Select(
                                m => ((IDictionary<string, object>)m)
                                    .ToDictionary(pi => pi.Key, pi => pi.Value))
                            .FirstOrDefault<IDictionary<string, object>>();

                if (ret != null)
                {
                    // 随意抽一条取最大条数记录
                    result = (TReturn)ret;
                }
                else
                {
                    result = default(TReturn);
                }
            }
            else
            {
                result = DbCon.QueryFirst_1<TReturn>(SqlProvider, DbTransaction);
            }

            Recovery();
            return result;
        }

        public TReturn Get<TReturn>(bool where, Expression<Func<T, TReturn>> trueSelect, Expression<Func<T, TReturn>> falseSelect)
        {
            TReturn result;
            if (where)
            {
                SqlProvider.Context.Set.SelectExpression = trueSelect;
            }
            else
            {
                SqlProvider.Context.Set.SelectExpression = falseSelect;
            }
            SqlProvider.FormatToSingle<T>();

            // 如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(T).GetInterface("IDictionary");
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                            Select(
                                m => ((IDictionary<string, object>)m)
                                    .ToDictionary(pi => pi.Key, pi => pi.Value))
                            .FirstOrDefault<IDictionary<string, object>>();

                if (ret != null)
                {
                    // 随意抽一条取最大条数记录
                    result = (TReturn)ret;
                }
                else
                {
                    result = default(TReturn);
                }
            }
            else
            {
                result = DbCon.QueryFirst_1<TReturn>(SqlProvider, DbTransaction);
            }

            Recovery();

            return result;
        }

        public async Task<T> GetAsync()
        {
            T result;
            SqlProvider.FormatToSingle<T>();

            result = await DbCon.QueryFirstOrDefaultAsyncs<T>(SqlProvider, DbTransaction);

            Recovery();

            return result;
        }

        public IEnumerable<T> ToIEnumerable()
        {
            IEnumerable<T> result;
            SqlProvider.FormatToList<T>();


            var interfaceType = typeof(T).GetInterface("IDictionary");
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                            Select(m => ((IDictionary<string, object>)m).ToDictionary(pi => pi.Key, pi => pi.Value)).
                            ToList<IDictionary<string, object>>();

                if (ret.Count > 0)
                {
                    // 随意抽一条取最大条数记录
                    result = ret.Select(m => (T)m);
                }
                else
                {
                    result = new List<T>();
                }
            }
            else
            {
                result = DbCon.Querys<T>(SqlProvider, DbTransaction);
            }

            Recovery();

            return result;
        }

        public IEnumerable<TSource> ToIEnumerable<TSource>()
        {
            IEnumerable<TSource> result;
            SqlProvider.FormatToList<T>();


            // 如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(T).GetInterface("IDictionary");
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                            Select(m => ((IDictionary<string, object>)m).ToDictionary(pi => pi.Key, pi => pi.Value)).
                            ToList<IDictionary<string, object>>();

                if (ret.Count > 0)
                {
                    // 随意抽一条取最大条数记录
                    result = ret.Select(m => (TSource)m);
                }
                else
                {
                    result = new List<TSource>();
                }
            }
            else
            {
                result = DbCon.Querys<TSource>(SqlProvider, DbTransaction);
            }


            Recovery();

            return result;
        }

        public IEnumerable<TReturn> ToIEnumerable<TReturn>(Expression<Func<T, TReturn>> select)
        {
            IEnumerable<TReturn> result;
            SqlProvider.Context.Set.SelectExpression = select;
            SqlProvider.FormatToList<T>();


            // 如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(T).GetInterface("IDictionary");
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                            Select(m => ((IDictionary<string, object>)m).ToDictionary(pi => pi.Key, pi => pi.Value)).
                            ToList<IDictionary<string, object>>();

                if (ret.Count > 0)
                {
                    // 随意抽一条取最大条数记录
                    result = ret.Select(m => (TReturn)m);
                }
                else
                {
                    result = new List<TReturn>();
                }
            }
            else
            {
                result = DbCon.Query_1<TReturn>(SqlProvider, DbTransaction);
            }
           

            Recovery();

            return result;
        }

        public IEnumerable<TReturn> ToIEnumerable<TReturn>(bool where, Expression<Func<T, TReturn>> trueSelect, Expression<Func<T, TReturn>> falseSelect)
        {

            IEnumerable<TReturn> result;
            if (where){
                SqlProvider.Context.Set.SelectExpression = trueSelect;
            }
            else{
                SqlProvider.Context.Set.SelectExpression = falseSelect;
            }
            SqlProvider.FormatToList<T>();


            // 如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(T).GetInterface("IDictionary");
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                            Select(m => ((IDictionary<string, object>)m).ToDictionary(pi => pi.Key, pi => pi.Value)).
                            ToList<IDictionary<string, object>>();

                if (ret.Count > 0)
                {
                    // 随意抽一条取最大条数记录
                    result = ret.Select(m => (TReturn)m);
                }
                else
                {
                    result = new List<TReturn>();
                }
            }
            else
            {
                result = DbCon.Query_1<TReturn>(SqlProvider, DbTransaction);
            }

            Recovery();

            return result;
        }

        public async Task<IEnumerable<T>> ToIEnumerableAsync()
        {
            IEnumerable<T> result;
            SqlProvider.FormatToList<T>();


            result= await DbCon.QueryAsyncs<T>(SqlProvider, DbTransaction);

            Recovery();
            return result;
        }

        public List<T> ToList()
        {
            List<T> result;
            SqlProvider.FormatToList<T>();


            // 如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(T).GetInterface("IDictionary");
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                            Select(m => ((IDictionary<string, object>)m).ToDictionary(pi => pi.Key, pi => pi.Value)).
                            ToList<IDictionary<string, object>>();

                if (ret.Count > 0)
                {
                    // 随意抽一条取最大条数记录
                    result = ret.Select(m => (T)m).ToList();
                }
                else
                {
                    result = new List<T>();
                }
            }
            else
            {
                result = DbCon.Querys<T>(SqlProvider, DbTransaction).ToList();
            }

            Recovery();

            return result;
        }

        public List<TSource> ToList<TSource>()
        {
            List<TSource> result;
            SqlProvider.FormatToList<T>();

            // 如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(T).GetInterface("IDictionary");
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                            Select(m => ((IDictionary<string, object>)m).ToDictionary(pi => pi.Key, pi => pi.Value)).
                            ToList<IDictionary<string, object>>();

                if (ret.Count > 0)
                {
                    // 随意抽一条取最大条数记录
                    result = ret.Select(m => (TSource)m).ToList();
                }
                else
                {
                    result = new List<TSource>();
                }
            }
            else
            {
                result = DbCon.Querys<TSource>(SqlProvider, DbTransaction).ToList();
            }

            Recovery();
            return result;
        }

        public List<TReturn> ToList<TReturn>(Expression<Func<T, TReturn>> select)
        {
            List<TReturn> result;
            SqlProvider.Context.Set.SelectExpression = select;
            SqlProvider.FormatToList<T>();

            // 如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(T).GetInterface("IDictionary");
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                            Select(m => ((IDictionary<string, object>)m).ToDictionary(pi => pi.Key, pi => pi.Value)).
                            ToList<IDictionary<string, object>>();

                if (ret.Count > 0)
                {
                    // 随意抽一条取最大条数记录
                    result = ret.Select(m => (TReturn)m).ToList();
                }
                else
                {
                    result = new List<TReturn>();
                }
            }
            else
            {
                result = DbCon.Query_1<TReturn>(SqlProvider, DbTransaction);
            }

            Recovery();
            return result;
        }

        public List<TReturn> ToList<TReturn>(bool where, Expression<Func<T, TReturn>> trueSelect, Expression<Func<T, TReturn>> falseSelect)
        {
            List<TReturn> result;
            if (where)
                SqlProvider.Context.Set.SelectExpression = trueSelect;
            else
                SqlProvider.Context.Set.SelectExpression = falseSelect;
            SqlProvider.FormatToList<T>();


            // 如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(T).GetInterface("IDictionary");
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                            Select(m => ((IDictionary<string, object>)m).ToDictionary(pi => pi.Key, pi => pi.Value)).
                            ToList<IDictionary<string, object>>();

                if (ret.Count > 0)
                {
                    // 随意抽一条取最大条数记录
                    result = ret.Select(m => (TReturn)m).ToList();
                }
                else
                {
                    result = new List<TReturn>();
                }
            }
            else
            {
                result = DbCon.Query_1<TReturn>(SqlProvider, DbTransaction);
            }

            Recovery();
            return result;
        }

        public async Task<List<T>> ToListAsync()
        {
            List<T> result;

            SqlProvider.FormatToList<T>();
            var iEnumerable = await DbCon.QueryAsyncs<T>(SqlProvider, DbTransaction);
            result= iEnumerable.ToList();

            Recovery();
            return result;
        }

        public PageList<T> PageList(int pageIndex, int pageSize)
        {
            PageList<T> result;

            //查询总行数
            SqlProvider.FormatCount();
            var pageTotal = DbCon.QuerySingles<int>(SqlProvider, DbTransaction);
            //查询数据
            SqlProvider.FormatToPageList<T>(pageIndex, pageSize);


            //如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(T).GetInterface("IDictionary");
            var itemList = new List<T>();
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                                   Select(
                                         m => ((IDictionary<string, object>)m).ToDictionary(pi => pi.Key, pi => pi.Value)).
                                   ToList<IDictionary<string, object>>();

                if (ret.Count > 0)
                {
                    itemList = ret.Select(m => (T)m).ToList();
                }
                else
                {
                    itemList = new List<T>();
                }
                result = new PageList<T>(pageIndex, pageSize, pageTotal, itemList);
            }
            else
            {
                itemList = DbCon.Query_1<T>(SqlProvider, DbTransaction);
                result = new PageList<T>(pageIndex, pageSize, pageTotal, itemList);
            }

            Recovery();
            return result;
        }

        public PageList<TSource> PageList<TSource>(int pageIndex, int pageSize)
        {
            PageList<TSource> result;

            //查询总行数
            SqlProvider.FormatCount();
            var pageTotal = DbCon.QuerySingles<int>(SqlProvider, DbTransaction);
            SqlProvider.FormatToPageList<T>(pageIndex, pageSize);


            //如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(T).GetInterface("IDictionary");
            var itemList = new List<TSource>();
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                                   Select(
                                         m => ((IDictionary<string, object>)m).ToDictionary(pi => pi.Key, pi => pi.Value)).
                                   ToList<IDictionary<string, object>>();

                if (ret.Count > 0)
                {
                    itemList = ret.Select(m => (TSource)m).ToList();
                }
                else
                {
                    itemList = new List<TSource>();
                }
                result = new PageList<TSource>(pageIndex, pageSize, pageTotal, itemList);
            }
            else
            {
                //查询数据
                itemList = DbCon.Query_1<TSource>(SqlProvider, DbTransaction);
                result = new PageList<TSource>(pageIndex, pageSize, pageTotal, itemList);
            }

            Recovery();
            return result;
        }

        public PageList<TReturn> PageList<TReturn>(int pageIndex, int pageSize, Expression<Func<T, TReturn>> select)
        {
            PageList<TReturn> result;

            //查询总行数
            SqlProvider.FormatCount();
            var pageTotal = DbCon.QuerySingles<int>(SqlProvider, DbTransaction);
            //查询数据
            SqlProvider.Context.Set.SelectExpression = select;
            SqlProvider.FormatToPageList<T>(pageIndex, pageSize);

            //如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(T).GetInterface("IDictionary");
            var itemList = new List<TReturn>();
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                                   Select(
                                         m => ((IDictionary<string, object>)m).ToDictionary(pi => pi.Key, pi => pi.Value)).
                                   ToList<IDictionary<string, object>>();

                if (ret.Count > 0)
                {
                    itemList = ret.Select(m => (TReturn)m).ToList();
                }
                else
                {
                    itemList = new List<TReturn>();
                }
                result = new PageList<TReturn>(pageIndex, pageSize, pageTotal, itemList);
            }
            else
            {
                //查询数据
                itemList = DbCon.Query_1<TReturn>(SqlProvider, DbTransaction);
                result = new PageList<TReturn>(pageIndex, pageSize, pageTotal, itemList);
            }

            Recovery();

            return result;
        }

        public PageList<TReturn> PageList<TReturn>(int pageIndex, int pageSize, bool where, Expression<Func<T, TReturn>> trueSelect, Expression<Func<T, TReturn>> falseSelect)
        {
            PageList<TReturn> result;

            //查询总行数
            SqlProvider.FormatCount();
            var pageTotal = DbCon.QuerySingles<int>(SqlProvider, DbTransaction);
            if (where){
                SqlProvider.Context.Set.SelectExpression = trueSelect;
            }
            else {
                SqlProvider.Context.Set.SelectExpression = falseSelect;
            }
            SqlProvider.FormatToPageList<T>(pageIndex, pageSize);

            //如果泛型类型是字典，分页结果里面的泛型集合就是字典集合，否则就是设定的泛型类型集合
            var interfaceType = typeof(T).GetInterface("IDictionary");
            var itemList = new List<TReturn>();
            if (typeof(T).Name.IndexOf("IDictionary") >= 0 || interfaceType != null)
            {
                //随意抽一条取最大条数记录
                var ret = DbCon.Query(SqlProvider.SqlString, SqlProvider.Params, DbTransaction).
                                   Select(
                                         m => ((IDictionary<string, object>)m).ToDictionary(pi => pi.Key, pi => pi.Value)).
                                   ToList<IDictionary<string, object>>();

                if (ret.Count > 0)
                {
                    itemList = ret.Select(m => (TReturn)m).ToList();
                }
                else
                {
                    itemList = new List<TReturn>();
                }
                result = new PageList<TReturn>(pageIndex, pageSize, pageTotal, itemList);
            }
            else
            {
                //查询数据
                itemList = DbCon.Query_1<TReturn>(SqlProvider, DbTransaction);
                result = new PageList<TReturn>(pageIndex, pageSize, pageTotal, itemList);
            }

            Recovery();

            return result;
        }

        //public DataSet ToDataSet(IDbDataAdapter dataAdapter = null)
        //{
        //    DataSet result;

        //    SqlProvider.FormatToList<T>();
        //    result= DbCon.QueryDataSets(SqlProvider, DbTransaction, dataAdapter);

        //    Recovery();

        //    return result;
        //}

        //public DataSet ToDataSet<TReturn>(Expression<Func<T, TReturn>> select, IDbDataAdapter dataAdapter = null)
        //{
        //    DataSet result;

        //    SqlProvider.Context.Set.SelectExpression = select;
        //    SqlProvider.FormatToList<T>();
        //    result= DbCon.QueryDataSets(SqlProvider, DbTransaction, dataAdapter);

        //    Recovery();
        //    return result;
        //}

        //public DataSet ToDataSet<TReturn>(bool where, Expression<Func<T, TReturn>> trueSelect, Expression<Func<T, TReturn>> falseSelect, IDbDataAdapter dataAdapter = null)
        //{
        //    DataSet result;

        //    if (where)
        //        SqlProvider.Context.Set.SelectExpression = trueSelect;
        //    else
        //        SqlProvider.Context.Set.SelectExpression = falseSelect;
        //    SqlProvider.FormatToList<T>();
        //    result= DbCon.QueryDataSets(SqlProvider, DbTransaction, dataAdapter);
        //    Recovery();
        //    return result;
        //}
    }
}
