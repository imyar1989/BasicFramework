﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Threading.Tasks;

namespace Validate.Library
{
    public class JwtIssuerOptions
    {

        /// <summary>
        /// 发行JWT的委托人
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// 声明标识了JWT的主体
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        ///声明标识JWT的目标收件人。
        /// </summary>
        public string Audience { get; set; }

        /// <summary>
        /// 声明标识JWT在处理时或之后不能被接受的到期时间
        /// </summary>
        public DateTime Expiration => IssuedAt.Add(ValidFor);

        /// <summary>
        /// 标识JWT不能被接受进行处理的时间。
        /// </summary>
        public DateTime NotBefore => DateTime.UtcNow;

        /// <summary>
        /// 索赔确定了JWT的签发时间。
        /// </summary>
        public DateTime IssuedAt => DateTime.UtcNow;

        /// <summary>
        /// 设置令牌有效的时间跨度（默认值为10分钟）
        /// </summary>
        public TimeSpan ValidFor { get; set; } = TimeSpan.FromHours(10);

        /// <summary>
        /// 默认ID
        /// </summary>
        public Func<Task<string>> JtiGenerator =>() => Task.FromResult(Guid.NewGuid().ToString());

        /// <summary>
        /// 生成令牌时要使用的签名密钥。
        /// </summary>
        public SigningCredentials SigningCredentials { get; set; }
    }
}
