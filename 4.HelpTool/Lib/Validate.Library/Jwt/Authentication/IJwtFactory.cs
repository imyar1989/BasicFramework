﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Validate.Library
{
    public interface IJwtFactory
    {
        /// <summary>
        /// 生成Token
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="refreshToken"></param>
        /// <param name="identity"></param>
        /// <returns></returns>
        Task<string> GenerateEncodedToken(string userName, string refreshToken, ClaimsIdentity identity);

        /// <summary>
        /// 生成Claims标识
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        ClaimsIdentity GenerateClaimsIdentity(Dictionary<string, object> user);
    }
}
