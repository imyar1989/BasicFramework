﻿using System;
using System.Collections.Generic;
using System.DrawingCore;
using System.DrawingCore.Imaging;
using System.IO;
using System.Text;
using ZXing;
using ZXing.Common;
using ZXing.OneD;

namespace Document.Library
{
    /// <summary>
    /// 条码、二维码操作类
    /// </summary>
    public class BarCodeHelper
    {
        /// <summary>
        /// 生成128条形码
        /// </summary>
        /// <param name="message"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="pureBarcode">是否纯粹的条形码</param>
        public static byte[] Create128BarCode(string message, int width, int height, bool pureBarcode = false)
        {
            Bitmap image = null;
            byte[] result = null;
            try
            {
                if (!string.IsNullOrWhiteSpace(message))
                {
                    Code128EncodingOptions options = new Code128EncodingOptions();
                    options.ForceCodesetB = false; //如果为true，请不要为数字切换到代码集C.
                    options.PureBarcode = pureBarcode; //是否将内容字符串显示在图片上。false 为显示 true为不显示
                    options.GS1Format = false;    //是否符合GS1
                    options.Width = width;    //图片宽度，根据内容的长度会自动增长
                    options.Height = height;  //图片高度
                    options.Margin = 3;    //填充，在图片左右填充空白 30则左右各15
                    var writer = new ZXing.ZKWeb.BarcodeWriter();
                    writer.Format = BarcodeFormat.CODE_128;
                    writer.Options = options;

                    image = writer.Write(message);
                    //输出图片流
                    result = BitmapToArray(image);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                image?.Dispose();
            }
            return result;
        }

        /// <summary>
        /// 将Bitmap  写为byte[]的方法
        /// </summary>
        /// <param name="bmp"></param>
        /// <returns></returns>
        private static byte[] BitmapToArray(Bitmap bmp)
        {
            byte[] byteArray = null;

            using (MemoryStream stream = new MemoryStream())
            {
                bmp.Save(stream, ImageFormat.Png);
                byteArray = stream.GetBuffer();
            }
            return byteArray;
        }
    }
}
