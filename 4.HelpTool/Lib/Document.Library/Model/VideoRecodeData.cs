﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Document.Library
{
    /// <summary>
    /// 格式转换中参数定义
    /// </summary>
    public class VideoRecodeData
    {
        /// <summary>
        /// 基础文件夹名称
        /// </summary>
        public string BasePath { get; set; }

        /// <summary>
        /// 源文件
        /// </summary>
        public string SourceFile { get; set; }

        /// <summary>
        /// 转换后的文件
        /// </summary>
        public string TargetFile { get; set; }

        /// <summary>
        /// 视频分辨率
        /// </summary>
        public string VideoSize { get; set; }

        /// <summary>
        ///视频 比特率
        /// </summary>
        public string BitRate_Video { get; set; }

        /// <summary>
        /// 采样率
        /// </summary>
        public string SamplingRate { get; set; }

        /// <summary>
        ///音频 比特率
        /// </summary>
        public string BitRate_Audio { get; set; }

        /// <summary>
        /// 音量
        /// </summary>
        public string Volume { get; set; }

        /// <summary>
        /// 成功后是否删除原文件
        /// </summary>
        public bool removeSourceFile { get; set; } = false;

    }
}
