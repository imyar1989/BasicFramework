﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReadPdf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 浏览悬着PDF文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBrowser_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "(*.pdf)|*.pdf";
            openFileDialog1.FileName = "";
            openFileDialog1.Title = "选择FDF文件";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                SetValue(txtPath, openFileDialog1.FileName);
            }
        }

        /// <summary>
        /// 设置值
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="msg"></param>
        private void SetValue(TextBox textBox, string msg)
        {
            BeginInvoke(new Action(() =>
            {
                textBox.Text = msg;
            }));
        }

        /// <summary>
        /// 运行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRun_Click(object sender, EventArgs e)
        {
            string pdffilename = txtPath.Text.Trim();
            ReadPFD2(pdffilename);
        }


        private string ReadPFD1(string path)
        {
            // string path = path;// @"D:\ydfile\d4bab8ff-26ff-4ddf-a602-872f6988db86_.pdf";
            string text = string.Empty;
            try
            {
                string pdffilename = path;
                StringBuilder buffer = new StringBuilder();
                //Create a pdf document.
                using (Spire.Pdf.PdfDocument doc = new Spire.Pdf.PdfDocument())
                {
                    // Load the PDF Document
                    doc.LoadFromFile(pdffilename);
                    // String for hold the extracted text

                    foreach (Spire.Pdf.PdfPageBase page in doc.Pages)
                    {
                        buffer.Append(page.ExtractText());
                    }
                    doc.Close();
                }
                //save text
                text = buffer.ToString();
                return text;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private  string ReadPFD2(string path)
        {
            // string path = path;// @"D:\ydfile\d4bab8ff-26ff-4ddf-a602-872f6988db86_.pdf";
            string text = string.Empty;
            try
            {
                string pdffilename = path;
                StringBuilder buffer = new StringBuilder();
                //Create a pdf document.
                using (Spire.Pdf.PdfDocument doc = new Spire.Pdf.PdfDocument())
                {
                    // Load the PDF Document
                    doc.LoadFromFile(pdffilename);
                    // String for hold the extracted text

                    foreach (Spire.Pdf.PdfPageBase page in doc.Pages)
                    {
                        buffer.Append(page.ExtractText());
                    }
                    doc.Close();
                }
                //save text
                text = buffer.ToString();
                return text;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }

}
