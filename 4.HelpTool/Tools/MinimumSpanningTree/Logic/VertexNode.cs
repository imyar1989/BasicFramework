﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/*
* 命名空间:  MinimumSpanningTree.Logic
*
* 功 能：Step2 构造顶点表结点的结构 VertexNode。
*
* 类 名：VertexNode
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/16 13:43:03       LW      创建
*
* Copyright (c) 2020 zhdl Corporation. All rights reserved.
*/
namespace MinimumSpanningTree.Logic
{
    /// <summary>
    /// Step2 构造顶点表结点的结构 VertexNode。
    /// </summary>
    public class VertexNode
    {
        /// <summary>
        /// 获取或设置顶点的名字
        /// </summary>
        public string VertexName { get; set; }

        /// <summary>
        /// 获取或设置顶点是否被访问
        /// </summary>
        public bool Visited { get; set; }

        /// <summary>
        /// 获取或设置顶点的第一个邻接点
        /// </summary>
        public EdgeNode FirstNode { get; set; }

        /// <summary>
        /// 初始化VertexNode类的新实例
        /// </summary>
        /// <param name="vName">顶点的名字</param>
        /// <param name="firstNode">顶点的第一个邻接点</param>
        public VertexNode(string vName, EdgeNode firstNode = null)
        {
            VertexName = vName;
            Visited = false;
            FirstNode = firstNode;
        }
    }
}
