﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/*
* 命名空间:  MinimumSpanningTree.Logic
*
* 功 能：像素点
*
* 类 名：PointInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/16 14:17:27       LW      创建
*
* Copyright (c) 2020 zhdl Corporation. All rights reserved.
*/
namespace MinimumSpanningTree.Logic
{
    /// <summary>
    /// 像素点
    /// </summary>
    public class PointInfo
    {
        /// <summary>
        ///坐标唯一标记
        /// </summary>
        public string id { get; set; }


        /// <summary>
        /// x坐标
        /// </summary>
        public double x { get; set; }

        /// <summary>
        /// 有坐标
        /// </summary>
        public double y { get; set; }
    }
}
