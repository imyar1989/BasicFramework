﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TableToEntity
{
    public partial class CreateDtoWindow : Form
    {
        public CreateDtoWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 生成逻辑文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreateCS_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(comTableType.Text))
            {
                MessageBox.Show("请选择表的类型");
                return;
            }
            if (string.IsNullOrEmpty(textNameSpace.Text))
            {
                MessageBox.Show("请选择命名空间");
                return;
            }
            if (string.IsNullOrEmpty(txtProfile.Text))
            {
                MessageBox.Show("请输入实体映射配置类");
                return;
            }
            if (string.IsNullOrEmpty(txtLogicName.Text))
            {
                MessageBox.Show("请输入逻辑类名称");
                return;
            }
            if (string.IsNullOrEmpty(txtModel.Text))
            {
                MessageBox.Show("请输入数据库实体名称");
                return;
            }
            if (string.IsNullOrEmpty(txtQuery.Text))
            {
                MessageBox.Show("请输入查询数据实体名称");
                return;
            }
            if (string.IsNullOrEmpty(txtEntity.Text))
            {
                MessageBox.Show("请输入编辑传输实体名称");
                return;
            }
            if (string.IsNullOrEmpty(txtResponse.Text))
            {
                MessageBox.Show("请输入返回传输实体名称");
                return;
            }
            DtoInfo dtoInfo = new DtoInfo();
            dtoInfo.TableType = comTableType.Text;
            dtoInfo.NameSpace = textNameSpace.Text;
            dtoInfo.Profile = txtProfile.Text;
            dtoInfo.LogicName = txtLogicName.Text;
            dtoInfo.TableModel = txtModel.Text;
            dtoInfo.QueryModel = txtQuery.Text;
            dtoInfo.ModifyModel= txtEntity.Text;
            dtoInfo.ResponseModel = txtResponse.Text;

            string logicFolder = Path.Combine(Environment.CurrentDirectory, dtoInfo.LogicName);
            if (!Directory.Exists(logicFolder))
            {
                Directory.CreateDirectory(logicFolder);
            }
            dtoInfo.LogicFolder = logicFolder;

            string dtoFolder = Path.Combine(Environment.CurrentDirectory, dtoInfo.LogicName+ "\\Dto");
            if (!Directory.Exists(dtoFolder))
            {
                Directory.CreateDirectory(dtoFolder);
            }
            dtoInfo.DtoFolder = dtoFolder;

            ///生成Profile文件
            CreateLogicFile.CreateProfile(dtoInfo);
            txtResult.AppendText("[" + dtoInfo.Profile + "Profile]已生成完毕........." + Environment.NewLine);
            //生成逻辑类文件
            if (dtoInfo.TableType == "List")
            {
                CreateLogicFile.CreateListLogic(dtoInfo);
                txtResult.AppendText("[" + dtoInfo.LogicName + "ServiceImpl.cs]已生成完毕........." + Environment.NewLine);

                //创建列表类型的逻辑类接口
                CreateLogicFile.CreateListInterface(dtoInfo);
                txtResult.AppendText("[I" + dtoInfo.LogicName + "Service.cs]已生成完毕........." + Environment.NewLine);

                //生成接口控制器
                CreateControllerFile.CreateListController(dtoInfo);
                txtResult.AppendText("[" + dtoInfo.LogicName + "Controller.cs]已生成完毕........." + Environment.NewLine);

                CreateVueFile.CreateListIndexVue(dtoInfo);
                txtResult.AppendText("["+ "index.vue]已生成完毕........." + Environment.NewLine);
            }
            else {
                //CreateLogicFile. CreateTreeLogic(dtoInfo);
                //CreateLogicFile.CreateTreeInterface(dtoInfo);
            }
        }
    }
}
