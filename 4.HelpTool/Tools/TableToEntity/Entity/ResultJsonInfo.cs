﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;

namespace TableToEntity
{
    /// <summary>
    /// 返回给客户端的信息实体
    /// </summary>
    public class ResultJsonInfo<T>
    {
        /// <summary>
        /// 返回数据
        /// </summary>
        public T Data { get; set; } = default(T);

        /// <summary>
        /// 成功/失败 
        /// </summary>
        public bool Success { get; set; } = true;

        /// <summary>
        /// 返回的操作消息
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]//空值的处理
        public string Msg { get; set; } = null;

    }
}
