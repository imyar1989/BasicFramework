﻿using MongoDBTest.Entity;
using MongoDBTest.Logic;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MongoDBTest
{
    public partial class MongoDB : Form
    {
        private delegate void SetTextBoxValue(string value);

        public MongoDB()
        {
            InitializeComponent();
        }


        /// <summary>
        /// 异步显示信息
        /// </summary>
        /// <param name="value"></param>
        private void SetMyTextBoxValue(string value)
        {
            if (this.txt_show.InvokeRequired)
            {
                SetTextBoxValue objSetTextBoxValue = new SetTextBoxValue(SetMyTextBoxValue);
                IAsyncResult result = this.txt_show.BeginInvoke(objSetTextBoxValue, new object[] { value });
                try
                {
                    objSetTextBoxValue.EndInvoke(result);
                }
                catch
                {
                }
            }
            else
            {
                this.txt_show.Text += value + Environment.NewLine;
                this.txt_show.SelectionStart = this.txt_show.TextLength;
                this.txt_show.ScrollToCaret();
            }
        }

        /// <summary>
        /// 查询所有
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_searchAll_Click(object sender, EventArgs e)
        {
            var mongoDBTest = new MongoDBImpl();
            var result = mongoDBTest.TestSearchAll();
            string resultStr = string.Empty;
            foreach (var item in result)
            {
                resultStr += "姓名：" + item.Name + "-年龄:" + item.Age + "-朋友：" + item.Friends + Environment.NewLine;
            }
            txt_show.Text = resultStr;
        }

        /// <summary>
        /// 排序查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_link_Click(object sender, EventArgs e)
        {
            string name = txt_name.Text;
            string age = txt_age.Text;
            if (!string.IsNullOrWhiteSpace(name) && !string.IsNullOrWhiteSpace(age))
            {
                UserInfo user = new UserInfo()
                {
                    Name = name,
                    Age = Convert.ToDouble(age)
                };
                var mongoDBTest = new MongoDBImpl();
                var result = mongoDBTest.TestSearchDataBase(user);
                string resultStr = string.Empty;
                foreach (var item in result)
                {
                    resultStr += "姓名：" + item.Name + "-年龄:" + item.Age + "-朋友：" + item.Friends + Environment.NewLine;
                }
                txt_show.Text = resultStr;
            }
            else
            {
                txt_show.Text = "无查询条件";
            }
        }

        /// <summary>
        /// 列表查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_searchlist_Click_1(object sender, EventArgs e)
        {
            string name = txt_name.Text;
            string age = txt_age.Text;
            if (!string.IsNullOrWhiteSpace(name) && !string.IsNullOrWhiteSpace(age))
            {
                UserInfo user = new UserInfo()
                {
                    Name = name,
                    Age = Convert.ToDouble(age)
                };
                var mongoDBTest = new MongoDBImpl();
                var result = mongoDBTest.TestSearchList(user);
                string resultStr = string.Empty;
                foreach (var item in result)
                {
                    resultStr += "姓名：" + item.Name + "-年龄:" + item.Age + "-朋友：" + item.Friends + Environment.NewLine;
                }
                txt_show.Text = resultStr;
            }
            else
            {
                txt_show.Text = "无查询条件";
            }
        }

        /// <summary>
        /// 单个查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_searchSingle_Click_1(object sender, EventArgs e)
        {
            string name = txt_name.Text;
            string age = txt_age.Text;
            string friends = textBox1.Text;
            if (!string.IsNullOrWhiteSpace(name) && !string.IsNullOrWhiteSpace(age) && !string.IsNullOrWhiteSpace(friends))
            {
                UserInfo user = new UserInfo()
                {
                    Name = name,
                    Age = Convert.ToDouble(age),
                    Friends = Convert.ToDouble(friends)
                };
                var mongoDBTest = new MongoDBImpl();
                var result = mongoDBTest.TestSearchSingle(user);
                string resultStr = string.Empty;
                if (result != null)
                {
                    resultStr = "姓名：" + result.Name + "-年龄:" + result.Age + "-朋友：" + result.Friends;
                }
                txt_show.Text = resultStr;
            }
            else
            {
                txt_show.Text = "无查询条件";
            }
        }

        /// <summary>
        /// 插入测试
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_insert_Click_1(object sender, EventArgs e)
        {
            string name = txt_name.Text;
            string age = txt_age.Text;
            string friends = textBox1.Text;
            if (!string.IsNullOrWhiteSpace(name) && !string.IsNullOrWhiteSpace(age) && !string.IsNullOrWhiteSpace(friends))
            {
                UserInfo user = new UserInfo()
                {
                    Name = name,
                    Age = Convert.ToDouble(age),
                    Friends = Convert.ToDouble(friends)
                };
                var mongoDBTest = new MongoDBImpl();
                var result = mongoDBTest.TestInsert(user);
                string resultStr = string.Empty;
                if (result != null)
                {
                    resultStr = "插入信息：姓名：" + result.Name + "-年龄:" + result.Age + "-朋友：" + result.Friends;
                }
                txt_show.Text = resultStr;
            }
            else
            {
                txt_show.Text = "请输入姓名-年龄-朋友个数";
            }
        }

        /// <summary>
        /// 批量插入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_manyInsert_Click_1(object sender, EventArgs e)
        {

            List<UserInfo> users = new List<UserInfo>();
            UserInfo user = null;
            Random r = new Random();
            for (int i = 0; i < 6; i++)
            {
                user = new UserInfo
                {
                    Name = "LW" + r.Next(0, 1233),
                    Age = Convert.ToDouble(r.Next(0, 99)),
                    Friends = Convert.ToDouble(r.Next(0, 1780))
                };
                users.Add(user);
            }
            var mongoDBTest = new MongoDBImpl();
            var result = mongoDBTest.TestInsertMany(users);
            string resultStr = "插入信息：" + Environment.NewLine;
            foreach (var item in result)
            {
                resultStr += "姓名：" + item.Name + "-年龄:" + item.Age + "-朋友：" + item.Friends + Environment.NewLine;
            }
            txt_show.Text = resultStr;
        }

        /// <summary>
        /// 条件更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_conditionUpdate_Click_1(object sender, EventArgs e)
        {
            string oldName = txt_condition.Text;
            string newName = txt_newCondition.Text;
            var mongoDBTest = new MongoDBImpl();
            var result = mongoDBTest.TestUpdateOne(oldName, newName);
            if (result > 0)
            {
                txt_show.Text = "更新成功！";
            }
        }


        /// <summary>
        /// 根据条件删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_remove_Click_1(object sender, EventArgs e)
        {
            string name = txt_name.Text;
            var mongoDBTest = new MongoDBImpl();
            var result = mongoDBTest.TestDeleteMany(name);
            if (result > 0)
            {
                txt_show.Text = "删除成功！";
            }
        }

        /// <summary>
        /// 插入限制
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsNumber(e.KeyChar) || e.KeyChar == '\b'))
            {
                e.Handled = true;
            }
        }

        private void txt_age_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsNumber(e.KeyChar) || e.KeyChar == '\b'))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// 创建自增表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            AutoIncrement autoIncrement = new AutoIncrement()
            {
                TableMark = "TableMark"
            };
            var mongoDBTest = new MongoDBImpl();
            var result = mongoDBTest.TestInsertIncrement(autoIncrement);

        }


        /// <summary>
        /// 实现ID自增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_modified_Click_1(object sender, EventArgs e)
        {

            string name = txt_name.Text;
            string age = txt_age.Text;
            string friends = textBox1.Text;
            if (!string.IsNullOrWhiteSpace(name) && !string.IsNullOrWhiteSpace(age) && !string.IsNullOrWhiteSpace(friends))
            {
                AutoIncrement autoIncrement = new AutoIncrement()
                {
                    TableMark = "tableMark"
                };
                var mongoDBTest = new MongoDBImpl();
                var result = mongoDBTest.TestInsertModify(autoIncrement);
               
                string resultStr = string.Empty;
                if (result != null)
                {
                    resultStr = result.seq.ToString();
                }
                txt_show.Text = resultStr;
            }
            else
            {
                txt_show.Text = "请输入姓名-年龄-朋友个数";
            }


          
           
        }
    }
}
