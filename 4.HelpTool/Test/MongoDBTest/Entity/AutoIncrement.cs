﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel;

namespace MongoDBTest.Entity
{
    [Description("AutoIncrement")]
    [BsonIgnoreExtraElements]
    public class AutoIncrement
    {

        public BsonObjectId _id;//这个对应了 MongoDB.Bson.ObjectId 

        [BsonDefaultValue(defaultValue: "")]
        [BsonElement(elementName: "tableMark")]
        public string TableMark { get; set; }

        [BsonDefaultValue(defaultValue: "0")]
        [BsonElement(elementName: "seq")]
        public int seq { set; get; }
    }
}
