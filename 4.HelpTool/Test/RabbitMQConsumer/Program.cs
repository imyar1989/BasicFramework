﻿using RabbitMQ.Library;
using System;

namespace RabbitMQConsumer
{
    class Program
    {
        static void Main(string[] args)
        {
            Listening();
            Console.ReadLine();
        }

        private static void Listening()
        {
            string listenQueueName = "log.opt.goods.queue";
            //RabbitClient.Instance.ActionEventMessage += ActionEventMessage;

            RabbitClient.Instance.ActionEventMessage += ((MessageResult result) =>
            {
                var message = RabbitClient.GetResultMessage<UserInfo>(result); ;
                result.Handled = true;
                Console.WriteLine(message.Name);
            });

            RabbitClient.Instance.OnListening(listenQueueName);
        }

        private static void ActionEventMessage(MessageResult result)
        {
            var message = RabbitClient.GetResultMessage<UserInfo>(result); ;
            result.Handled = true;
            Console.WriteLine(message.Name);
        }
    }
}
