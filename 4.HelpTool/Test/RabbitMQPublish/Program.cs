﻿using RabbitMQ.Library;
using System;

namespace RabbitMQPublish
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateExchange();
            SetETERouting();
            CreateQueue();
            //RemoveQueue();
            SetRouting();
            SendEventMessage();

            Console.ReadLine();
        }

        /// <summary>
        /// 创建交换机
        /// </summary>
        private static void CreateExchange()
        {
            var exchanges = RabbitClient.Config.Exchanges;
            Console.WriteLine("创建交换机开始...");
            foreach (var item in exchanges)
            {
                RabbitClient.Instance.CreateExchange(item);
            }
            Console.WriteLine("创建交换机完毕...");
        }


        /// <summary>
        /// 配置交换机TO交换机路由
        /// </summary>
        private static void SetETERouting()
        {

            var routingRule = RabbitClient.Config.RoutingRules
                .FindAll(p => p.Type == RabbitClient.RountingToExchage);
            Console.WriteLine("配置交换机TO交换机路由开始...");
            foreach (var item in routingRule)
            {
                RabbitClient.Instance.CreateExchangeToExchange(item);
            }
            Console.WriteLine("配置交换机TO交换机路由完毕...");
        }


        /// <summary>
        /// 创建队列
        /// </summary>
        private static void CreateQueue()
        {
            var queues = RabbitClient.Config.Queues;
            Console.WriteLine("创建队列开始...");
            foreach (var item in queues)
            {
                RabbitClient.Instance.CreateQueue(item);
            }
            Console.WriteLine("创建队列完毕...");
        }

        /// <summary>
        /// 删除队列
        /// </summary>
        private static void RemoveQueue()
        {
            var queues = RabbitClient.Config.Queues;
            Console.WriteLine("删除队列开始...");
            foreach (var item in queues)
            {
                RabbitClient.Instance.RemoveQueue(item.Queue);
            }
            Console.WriteLine("删除队列完毕...");
        }

        /// <summary>
        /// 配置路由
        /// </summary>
        private static void SetRouting()
        {

            var routingRule = RabbitClient.Config.RoutingRules
                .FindAll(p => p.Type == RabbitClient.RountingToQueue);
            Console.WriteLine("配置路由开始...");
            foreach (var item in routingRule)
            {
                RabbitClient.Instance.CreateExchangeToQueue(item);
            }
            Console.WriteLine("配置路由完毕...");
        }

        /// <summary>
        /// 发送信息
        /// </summary>
        private static void SendEventMessage()
        {
            for (var i = 1; i < 100; i++)
            {
                var originObject = new UserInfo()
                {
                    ID = "1",
                    Name = $"lw测试{i}",
                    Age = 22,
                    Created = DateTime.Now
                };

                var sendMessage = RabbitClient.GetPublishMessage<UserInfo>(originObject, RabbitClient.JsonData);
                //信息  交换机  路由Key
                //[log.opt.abc 精确匹配，log.opt.abc]
                //[log.opt.# 模糊匹配多个点，log.opt.xxx.xxx]
                //[log.opt.* 模糊匹配一个点，log.opt.xxx]
                RabbitClient.Instance.SendEventMessage(sendMessage, "log.opt.exchange", "log.opt.*");

                Console.WriteLine(i);
            }
        }
    }
}
