import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {
  ///////////////////////////////消息相关接口////////////////////////////
  //获取对应时间段类，自己的总的消息，未读消息数量
  loadMessageCount(par) {
    return httpRequest({
      url: "/api/Basic/Message/LoadMyselfMessageCount/{beginTime}/{endTime}",
      method: 'GET',
      modal: false,
      data: par
    });
  },
  //获取最新未读信息
  loadCurrentMessage(par) {
    return httpRequest({
      url: "/api/Basic/Message/LoadUnreadByTime/{beginTime}/{endTime}",
      method: 'GET',
      modal: false,
      data: par
    });
  },
  //回复消息
  replayMassage(par) {
    return httpRequest({
      url: "/api/Basic/Message/SendMessage",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //查看信息
  readMassage(par) {
    return httpRequest({
        url: "/api/Basic/Message/ReadMessage/{id}",
        method: 'GET',
        modal: false,
        data: par
    });
   },
   //删除消息
   removeMassage(par) {
    return httpRequest({
        url: "/api/Basic/Message/Remove",
        method: 'POST',
        modal: false,
        data: par
    });
   },
  ///////////////////////////////操作信息相关接口////////////////////////////
  //获取对应时间段内，操作日志数量
  loadOperationCount(par) {
    return httpRequest({
      url: "/api/Basic/OperationLog/LoadOperationCount/{beginTime}/{endTime}",
      method: 'GET',
      modal: false,
      data: par
    });
  },
  //获取最新操作日志列表
  loadOperationList(par) {
    return httpRequest({
      url: "/api/Basic/OperationLog/LoadLatestList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  ///////////////////////////////异常信息相关接口////////////////////////////
  //获取对应时间段内，异常信息数量
  loadExceptionCount(par) {
    return httpRequest({
      url: "/api/Basic/ExceptionLog/LoadExceptionCount/{beginTime}/{endTime}",
      method: 'GET',
      modal: false,
      data: par
    });
  },
  //获取最异常日志信息
  loadExceptionList(par) {
    return httpRequest({
      url: "/api/Basic/ExceptionLog/LoadLatestList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  ///////////////////////////////安全信息相关接口////////////////////////////
  //获取接口预警和黑名单个数信息
  loadApiMonitorCount(par) {
    return httpRequest({
      url: "/api/Safe/ApiMonitorLog/LoadApiMonitorCount",
      method: 'POST',
      modal: false,
      data: par
    });
  },
}
