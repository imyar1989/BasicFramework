/* 
 * @Author: LW  
 * @Date: 2021-01-11 14:22:37  
 * @function:订单管理相关接口
 * ---------------------------------------------------------- 
 */
import http from '@/utils/http';
import store from '@/store';

const {
  httpRequest
} = http;

export default {

  //获取订单分页列表
  loadPageList(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Order/OrderInfo/LoadPageList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //获取支付方式类型
  loadPayTypeList(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Order/OrderInfo/LoadPayTypeList",
      method: 'GET',
      modal: false,
      data: par
    });
  },
  //获取订单状态类型
  loadOrderStatusList(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Order/OrderInfo/LoadOrderStatusList",
      method: 'GET',
      modal: false,
      data: par
    });
  },
  //获取物流商类型
  loadLogisticsProviderList(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Order/OrderInfo/LoadLogisticsProviderList",
      method: 'GET',
      modal: false,
      data: par
    });
  },
  //////////////////////////////////////////////////////////发货
  //保存订单信息
  saveInfo(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Order/OrderInfo/ModifyInfo",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //订单发货
  deliverInfo(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Order/OrderInfo/DeliverInfo",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //删除订单信息
  removeInfo(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Order/OrderInfo/Remove",
      method: 'POST',
      modal: false,
      data: par
    });
  },
}
