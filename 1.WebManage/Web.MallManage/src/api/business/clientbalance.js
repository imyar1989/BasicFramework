/* 
 * @Author: LW  
 * @Date: 2021-01-11 14:22:37  
 * @function:客户消费记录相关接口
 * ---------------------------------------------------------- 
 */
import http from '@/utils/http';
import store from '@/store';

const {
  httpRequest
} = http;

export default {
  //获取客户消费记录信息列表
  loadPageList(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Client/ClientBalance/LoadPageList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
}
