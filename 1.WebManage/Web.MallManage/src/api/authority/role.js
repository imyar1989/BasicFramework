/* 
 * @Author: LW  
 * @Date: 2021-01-11 14:22:37  
 * @function:角色管理相关接口
 * ---------------------------------------------------------- 
 */ 
import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {
  //获取分页列表信息
  loadPageList(par) {
    return httpRequest({
      url: "/api/Authority/RoleInfo/LoadList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //启用或停用
  forbiddenInfo(par) {
    return httpRequest({
        url: "/api/Authority/RoleInfo/ForbidOrEnable/{id}",
        method: 'GET',
        modal: false,
        data: par
    });
  },
  //删除信息
  removeInfo(par) {
    return httpRequest({
        url: "/api/Authority/RoleInfo/Remove",
        method: 'POST',
        modal: false,
        data: par
    });
  },
   ///////////////////////编辑
   //增加数据
   addInfo(par) {
    return httpRequest({
        url: "/api/Authority/RoleInfo/AddInfo",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //编辑数据
   modifyInfo(par) {
    return httpRequest({
        url: "/api/Authority/RoleInfo/Modify",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   ////////////////////////菜单操作
   //根据角色ID获取数据所有菜单和可操作菜单
   loadMenusListByRoleId(par) {
    return httpRequest({
        url: "/api/Authority/FunctionCfg/LoadTreeListByRoleId",
        method: 'POST',
        modal: false,
        data: par
    });
   },

   //保存菜单授权数据
   saveMenusInfo(par) {
    return httpRequest({
        url: "/api/Authority/FunctionCfg/ModifyRoleMenuInfo",
        method: 'POST',
        modal: false,
        data: par
    });
   },
}
