/* 
 * @Author: LW  
 * @Date: 2021-01-13 13:10:13  
 * @function:岗位管理HTTP
 * ---------------------------------------------------------- 
 */ 
import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {
  //获取分页列表信息
  loadPageList(par) {
    return httpRequest({
      url: "/api/Authority/Post/LoadList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //启用或停用
  forbiddenInfo(par) {
    return httpRequest({
        url: "/api/Authority/Post/ForbidOrEnable/{postId}",
        method: 'GET',
        modal: false,
        data: par
    });
  },
  //删除信息
  removeInfo(par) {
    return httpRequest({
        url: "/api/Authority/Post/Remove",
        method: 'POST',
        modal: false,
        data: par
    });
  },
   ///////////////////////编辑
   //增加数据
   addInfo(par) {
    return httpRequest({
        url: "/api/Authority/Post/AddPost",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //编辑数据
   modifyInfo(par) {
    return httpRequest({
        url: "/api/Authority/Post/Modify",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   ////////////////////////用户操作
   //获取用户列表
   loadUserListByPostId(par) {
    return httpRequest({
        url: "/api/Authority/Post/LoadListInfoByPostId",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //保存包含的用户信息
   saveUserInfo(par) {
    return httpRequest({
        url: "/api/Authority/Post/ModifyPostUserInfo",
        method: 'POST',
        modal: false,
        data: par
    });
   },
}

