/* 
 * @Author: LW  
 * @Date: 2021-01-11 14:22:37  
 * @function:用户管理相关接口
 * ---------------------------------------------------------- 
 */ 
import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {
  //获取部门列表信息
  loadDepartmentTreeList(par) {
    return httpRequest({
      url: "/api/Authority/Department/LoadAllTreeList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //获取部门下拉列表信息
  loadDepartmentList(par) {
    return httpRequest({
      url: "/api/Authority/Department/LoadAllSelectList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //获取岗位树状列表
  loadPostTreeListInfo(par) {
    return httpRequest({
      url: "/api/Authority/Post/LoadAllSelectList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //获取角色树状列表
  loadRoleTreeListInfo(par) {
    return httpRequest({
      url: "/api/Authority/RoleInfo/LoadAllSelectList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //获取用户分页列表信息
  loadPageList(par) {
    return httpRequest({
      url: "/api/Authority/UserInfo/LoadList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  ///////////////////////编辑
   //增加数据
   addInfo(par) {
    return httpRequest({
        url: "/api/Authority/UserInfo/AddUser",
        method: 'POST',
        modal: false,
        data: par
    });
   },
   //编辑数据
   modifyInfo(par) {
    return httpRequest({
        url: "/api/Authority/UserInfo/Modify",
        method: 'POST',
        modal: false,
        data: par
    });
   },
    //重置密码
    pswReset(par) {
      return httpRequest({
          url: "/api/Authority/UserInfo/PswReset/{id}",
          method: 'GET',
          modal: false,
          data: par
      });
   },
   //删除
   removeInfo(par) {
    return httpRequest({
        url: "/api/Authority/UserInfo/Remove",
        method: 'POST',
        modal: false,
        data: par
    });
  },
}
