import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {

  //获取所有的异常日志
  loadList(par) {
    return httpRequest({
      url: "/api/Basic/ExceptionLog/LoadList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //获取异常日志类型
  loadLogTypeList(par) {
    return httpRequest({
      url: "/api/Basic/ExceptionLog/LoadLogTypeList",
      method: 'GET',
      modal: false,
      data: par
    });
  },
  //获取异常日志服务名称
  loadServiceNameList(par) {
    return httpRequest({
      url: "/api/Basic/ExceptionLog/LoadServiceNameList",
      method: 'GET',
      modal: false,
      data: par
    });
  },
   //删除异常日志
   removeInfo(par) {
    return httpRequest({
      url: "/api/Basic/ExceptionLog/Remove",
      method: 'POST',
      modal: false,
      data: par
    });
  },
}
