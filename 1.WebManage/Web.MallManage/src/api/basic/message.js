import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {

  //获取所有的消息
  loadPageList(par) {
    return httpRequest({
      url: "/api/Basic/Message/LoadPageList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //获取需要接收消息的用户
  loadListExceptMyself(par) {
    return httpRequest({
      url: "/api/Authority/UserInfo/LoadListExceptMyself",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //阅读消息
  readMessage(par) {
    return httpRequest({
      url: "/api/Basic/Message/ReadMessage/{id}",
      method: 'GET',
      modal: false,
      data: par
    });
  },
  //发送消息
  sendMessage(par) {
    return httpRequest({
      url: "/api/Basic/Message/SendMessage",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //修改消息
  modify(par) {
    return httpRequest({
      url: "/api/Basic/Message/Modify",
      method: 'POST',
      modal: false,
      data: par
    });
  },
   //删除消息
   remove(par) {
    return httpRequest({
      url: "/api/Basic/Message/Remove",
      method: 'POST',
      modal: false,
      data: par
    });
  },
}
