/* 
 * @Author: LW  
 * @Date: 2020-08-06 14:00:31  
 * @function: 动态菜单操作逻辑帮助类  
 * ---------------------------------------------------------- 
 */
const rootRoutes = [
  {
    path: '/home',
    name: 'home',
    component: () => import('@/views/home/index.vue'),
    meta: {
      requireLogin: false
    },
    children: []
  }
];

export default {
  /**
   * 合并远程路由到本地路由
   * @param: routeList [Array] 远程路由列表 
   * */
  mergeRoutes(routeList) {
    if (routeList) {
      for (let i = 0; i < routeList.length; i++) {
      
        if (routeList[i].link_url.length > 1) {
          let temp = {
            path: routeList[i].link_url,
            component: (resolve) => require([`@/views${routeList[i].link_url}/index.vue`], resolve),
            meta: {
              id: routeList[i].id,
              function_type: routeList[i].function_type,
              name: routeList[i].name,
              active: routeList[i].active
            },
            children: []
          };
          rootRoutes[0].children.push(temp);
        } 
        //开始获取二三级菜单
        if (routeList[i].children.length > 0) {

          let childrenItem = routeList[i].children;
          for (let childindex  = 0; childindex  < childrenItem.length; childindex ++) {
            if (childrenItem[childindex ].children.length > 0) {
              childrenItem[childindex ].children.forEach(element => {
                let elementTemp = {
                  path: element.link_url,
                  component: (resolve) => require([`@/views${element.link_url}/index.vue`],
                    resolve),
                  meta: {
                    id: element.id,
                    name: element.name
                  },
                  children: []
                };
                rootRoutes[0].children.push(elementTemp);
              });
            } else {
              if (childrenItem[childindex ].link_url != "") {
                let childrenTemp = {
                  path:childrenItem[childindex].link_url,
                  component: (resolve) => require([`@/views${childrenItem[childindex].link_url}/index`],
                    resolve),
                  meta: {
                    id: childrenItem[childindex].id,
                    name: childrenItem[childindex].name
                  },
                  children: []
                };
                rootRoutes[0].children.push(childrenTemp);
              }
            }
          }
        }
      }
    }
    return rootRoutes;
  },
};
