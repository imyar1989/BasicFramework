/* 
 * @Author: LW  
 * @Date: 2020-07-31 14:04:39  
 * @function: 统一分支store模块
 */
import Vue from 'vue';
import Vuex from 'vuex';
import getters from './getters';

Vue.use(Vuex);

//module其实只是解决了当state中很复杂臃肿的时候，module可以将store分割成模块，每个模块中拥有自己的state、mutation、action和getter。
const modulesFiles = require.context('./modules', true, /\.js$/);

//不需要import app from ./modules/app 它将自动要求所有vuex模块从模块文件
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  //set './app.js' => 'app'
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1');
  const value = modulesFiles(modulePath);
  modules[moduleName] = value.default;
  return modules;
}, {});

const store = new Vuex.Store({
  modules,
  getters
});

export default store;
